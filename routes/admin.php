<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest:admin', 'namespace' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/checklogin', 'LoginController@checklogin')->name('checklogin');

    Route::get('/forgot_password_step-1', 'LoginController@forgotPasswordstep_i_get');
    Route::post('/forgot_password_step-1', 'LoginController@forgotPasswordstep_i')->name('admin-forgot.step-1');
    Route::get('/forgot_password_step-2', 'LoginController@forgotPasswordstep_ii_get');
    Route::get('/forgot_password_step-2/resend_otp', 'LoginController@forgot_resend_otp');
    Route::post('/forgot_password_step-2', 'LoginController@forgotPasswordstep_ii')->name('admin-forgot.step-2');
    Route::get('/otp_verify', 'LoginController@otp_verify_get');
    Route::post('/otp_verify-2', 'LoginController@otp_verify_post')->name('otp-verify');
});

Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', 'DashboardController@index')->name('home');
    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::get('/page', 'PageController@index')->name('page.index');
    Route::get('/page/edit/{id}', 'PageController@edit')->name('page.edit');
    Route::post('/page/edit/{id}', 'PageController@update')->name('page.update');

    Route::get('/setting', 'SettingController@edit')->name('setting.edit');
    Route::post('/setting', 'SettingController@update')->name('setting.update');

    Route::get('/change-password', 'LoginController@change_password')->name('user.edit');
    Route::post('/change-password', 'LoginController@save_password')->name('user.update');

    Route::post('/inquery/{id}', 'DashboardController@destroy')->name('inquery_delete');

    Route::get('/plan/{plan}/rule', 'PlanRuleController@index')->name('planrule.index');
    Route::get('/plan/{plan}/ruleadd', 'PlanRuleController@create')->name('planrule.create');
    Route::post('/plan/{plan}/ruleadd', 'PlanRuleController@store')->name('planrule.store');
    Route::get('/plan/{plan}/ruleedit/{id}', 'PlanRuleController@edit')->name('planrule.edit');
    Route::post('/plan/{plan}/ruleedit/{id}', 'PlanRuleController@update')->name('planrule.update');
    Route::post('/plan/ruledelete/{id}', 'PlanRuleController@destroy')->name('planrule.destroy');

    Route::get('/staff/{role}', 'StaffController@index')->name('staff.index');
    Route::get('/staff/{role}/add', 'StaffController@create')->name('staff.create');
    Route::post('/staff/{role}/add', 'StaffController@store')->name('staff.store');
    Route::get('/staff/{role}/verify/{id}', 'StaffController@otp_verify_get')->name('staff.verify');
    Route::post('/staff/{role}/verify/{id}', 'StaffController@otp_verify_post')->name('staff.verify_post');
    Route::get('/staff/{role}/edit/{id}', 'StaffController@edit')->name('staff.edit');
    Route::post('/staff/{role}/edit/{id}', 'StaffController@update')->name('staff.update');
    Route::post('/staff/delete/{id}', 'StaffController@destroy')->name('staff.destroy');
    Route::get('/staff/status/{id}/{status}', 'StaffController@status')->name('staff.status');

    Route::get('/insurance', 'InsuranceController@index')->name('insurance.index');
    Route::get('/insurance/resend_otp', 'InsuranceController@resend_otp');
    Route::get('/insurance/add', 'InsuranceController@create')->name('insurance.create');
    Route::get('/insurance/filter_plan', 'InsuranceController@filter_plan')->name('insurance.filter_plan');    
    Route::get('/insurance/{id}', 'InsuranceController@insurance_detail');
    
    Route::post('/insurance/add', 'InsuranceController@store')->name('insurance.store');
    Route::resources([
        'faq' => 'FaqController',
        'slider' => 'SliderController',
        'brand' => 'BrandController',
        'plan' => 'PlanController',
        'smartphone' => 'MobileController',
        'howitwork' => 'HowitworkController',
        'service' => 'ServiceController',
        'testimonial' => 'TestimonialController',
    ]);
});
