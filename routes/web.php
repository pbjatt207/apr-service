<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('', 'HomeController@home');
Route::get('contact', 'HomeController@contact');

Route::get('faq', 'HomeController@faq');
Route::get('service', 'HomeController@service');
Route::any('plan', 'PlanController@index')->name('plan');
Route::post('inquery', 'InqueryController@store')->name('inquiry');

Route::get('login', 'AuthController@loginget');
Route::post('login', 'AuthController@loginpost')->name('login');
Route::get('logout', 'AuthController@logout');

Route::get('register', 'AuthController@registerget');
Route::post('register', 'AuthController@registerpost')->name('register');

Route::get('register/otp_verify', 'AuthController@verify_otp');
Route::post('register/otp_verify', 'AuthController@verify_otp_post')->name('otp-verify');
Route::get('register/resend_otp', 'AuthController@resend_otp');

Route::get('profile', 'ProfileController@profile');
Route::post('user-update', 'ProfileController@update')->name('user.update');
Route::post('changepassword', 'ProfileController@changepassword')->name('user.changepassword');
Route::post('service_plan', 'InsuranceController@insurance_re')->name('insurance_re');
Route::get('service_plan/{id}', 'InsuranceController@create')->name('insurance.add');
Route::post('service_plan/{id}', 'InsuranceController@store')->name('insurance.store');
Route::get('page/{slug:slug}', 'HomeController@about');

Route::get('/forgot_password_step-1', 'AuthController@forgotPasswordstep_i_get');
Route::post('/forgot_password_step-1', 'AuthController@forgotPasswordstep_i')->name('forgot.step-1');
Route::get('/forgot_password_step-2', 'AuthController@forgotPasswordstep_ii_get');
Route::post('/forgot_password_step-2', 'AuthController@forgotPasswordstep_ii')->name('forgot.step-2');
