<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'api'], function () {
    
    Route::group(['middleware' => 'auth:app'], function () {
        Route::get('/staff_list', 'StaffController@staff_list');
        Route::get('/staff_Detail', 'StaffController@staff_Detail');
        Route::post('/add_staff', 'StaffController@add_staff');
        Route::post('/edit_staff', 'StaffController@edit_staff');
        
        Route::get('/admin_insurance_list', 'InsuranceController@insurance_list');
	    Route::post('/admin_add_insurance', 'InsuranceController@add_insurance');
	    Route::post('/admin_update_insurance', 'InsuranceController@update_insurance');
	    Route::post('/admin_change_password', 'AuthController@change_password');
	    Route::get('/admin_profile', 'AuthController@Profile');
	    Route::post('/admin_edit_profile', 'AuthController@editProfile');
	    Route::get('/dashboard', 'DashboardController@dashboard');
	    
	    Route::post('/edit_setting','SettingController@edit_setting');
	    
    });
    
	Route::group(['middleware' => 'auth:api'], function () {
	   // Route::get('/staff_list', 'StaffController@staff_list');
	    Route::get('/user_profile', 'AuthController@Profile');
	    Route::post('/user_edit_profile', 'AuthController@editProfile');
        Route::post('/user_change_password', 'AuthController@change_password');
	    
	    

		Route::get('/user_insurance_list', 'InsuranceController@insurance_list');
	    Route::post('/user_add_insurance', 'InsuranceController@add_insurance');
	    Route::post('/user_update_insurance', 'InsuranceController@update_insurance');
		
    });
	// Route::group(['middleware' => 'auth:api'], function () {

	//     Route::get('/staff_list', 'StaffController@staff_list');
	//     Route::post('/add_staff', 'StaffController@add_staff');
		
    // });

    // sendOtp api
    Route::get('/get_setting','SettingController@get_setting');
	Route::post('/sendOtp', 'AuthController@sendOtp');
	
	//forgot password
	
	Route::post('/forgot_password', 'AuthController@forgot_password');
	
	// verifyOtp api
	Route::post('/verifyOtp', 'AuthController@verifyOtp');
	
	// Register api
	Route::post('/new_register', 'AuthController@register');

    // Login api
	Route::post('/login', 'AuthController@login');

    // plan api
	
    // slider api
	Route::get('/slider_list', 'SliderController@Slider');
	Route::get('/brand_list', 'InsuranceController@brand_list');
	
	Route::post('/plan_list', 'PlanController@Plan');
	// Logout api
	Route::get('/logout', 'AuthController@logout');

    // Auth Api's

    

});
