<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class SmartPhone extends Model
{
    protected $guarded = [];
    protected $with = ['brand'];

    public function brand()
    {
        return  $this->hasOne('App\Model\Brand', 'id', 'brand_id');
    }
}
