<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $guarded = [];

    public function rule() {
    	return $this->hasMany('App\Model\PlanCondition', 'plan_id', 'id');
    }
}
