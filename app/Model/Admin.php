<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $guarded = [];

    public function role()
    {
        return $this->hasOne('App\Model\Role', 'id', 'role_id');
    }
    public function admin()
    {
        return $this->hasOne('App\Model\Admin', 'id', 'admin_id');
    }
}
