<?php

namespace App\Http\Controllers;

use App\Model\Inquery;
use App\Model\Setting;
use App\Model\User;
use Illuminate\Http\Request;
use Mail;
use Hash;
use Validator;
use Auth;

class AuthController extends Controller
{
    public function loginget()
    {
        return view('frontend.inc.login');
    }
    public function loginpost(Request $request)
    {

        $rules = [
            "mobile"       => "required",
            "password"    => "required",
        ];
        $request->validate($rules);

        $user_data = array(
            'mobile'     => $request->email,
            'password'  => $request->password
        );

        $is_remembered = !empty($request->remember_me) ? true : false;

        if (Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])) {
            if(session()->get('sess_req')) {
                return redirect(route('plan'))->with('success', 'Success! Login');
            }
            return redirect(url('/'))->with('success', 'Success! Login');
        } else {
            return redirect()->back()->with('danger', 'Credentials not matched.');
        }
    }
    public function registerget()
    {
        return view('frontend.inc.register');
    }
    public function registerpost(Request $request)
    {
        $user = $request->user();
        $setting = Setting::find(1);
        $otp = rand(100000,999999);
        
        
        $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
        // dd($msg);
        $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$request->mobile,$msg], $setting->sms_api);

        // dd($apiUrl);
        $sms    = file_get_contents($apiUrl);
        $sess_register = session()->get('sess_register');
        
        $new_arr = empty($request->price) && $sess_register ? $sess_register : $request->all();
        $new_arr['otp_code'] = $otp;
        // dd($new_arr);
        session(['sess_register' => $new_arr]);
        return redirect(url('register/otp_verify'));
        
    }
    public function verify_otp()
    {   
        $sess_register = session()->get('sess_register');
        // dd($sess_register);
        $data = compact('sess_register');
        return view('frontend.inc.register_otp_verify',$data);
    }
    public function verify_otp_post(Request $request)
    {
        // dd($request);
        $rules = [
            'otp_code'   => 'required',            
        ];

        $request->validate($rules);
        
        $sess_register = session()->get('sess_register');
        // dd($sess_register);
        if($sess_register['otp_code'] == $request->otp_code){
           
            $input['password']          = Hash::make($request->password);
            $record                     = new User();
            $record->password           = Hash::make($sess_register['password']);
            $record->fname              = $sess_register['fname'];
            $record->mobile             = $sess_register['mobile'];
            $record->otp_code           = $sess_register['otp_code'];
            $record->is_otp_verified    = '1';


            //  
            // $record->fill($input);
            $record->save();
            session()->forget('sess_register');
        } else {
            return redirect()->back()->with('danger', 'Failed! OTP not matched.');
        }
        
        return redirect(url('/login'))->with('success', 'Success! New record has been added.');
       

        // $setting = Setting::find(1);

        // $subject = "New Enquiry at ".$setting->title;
        // $email_params = [
        //     'to'        => env('MAIL_USERNAME'),
        //     'reciever'  => $setting->title,
        //     'from'      => $request->record['email'],
        //     'sender'    => $request->record['name'],
        //     'subject'   => $subject,
        //     'form_subject' => $request->record['subject'],
        //     'mobile'    => $request->record['mobile'],
        //     'msg'       => $request->record['message'],
        // ];
        // dd($email_params);
        // Mail::send('frontend.inc.email', $email_params, function ($msgEmail) use ($email_params) {
        //     extract($email_params);

        //     $msgEmail->to($to, $reciever)
        //     ->subject($subject)
        //     ->from($from, $sender)
        //     ->replyTo($to, $reciever);
        // });

        // return redirect()->back()->with('success', 'Success! New record has been added.');
        
    }
    public function forgotPasswordstep_i_get()
    {
        return view('frontend.inc.forgot_mobile');
    }
    public function forgotPasswordstep_i(Request $request)
    {
        // dd($request);
        $request->validate([
            'mobile'    => 'required|numeric|regex:/^\d{10}/'
        ]);
        
        if(empty($request->otp_code))
        {
            $admin = User::where('mobile', $request->mobile)->first();
            // dd($admin);
            if(!empty($admin->id))
            {
                
                $otp = rand(100000, 999999);
                $admin->otp_code = $otp;
                $admin->save();
                $setting = Setting::find(1);       
        
        
                $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
                // dd($msg);
                $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$request->mobile,$msg], $setting->sms_api);

                // dd($apiUrl);
                $sms    = file_get_contents($apiUrl);

                return redirect('/forgot_password_step-2?mobile='.$request->mobile);
            }
        }
            
    }
    public function forgotPasswordstep_ii_get(Request $request)
    {
        $data = compact('request');
        return view('frontend.inc.forgot_mobile_step',$data);
    }

    public function forgotPasswordstep_ii(Request $request)
    {
        // dd($request);
        $request->validate([
            'mobile'    => 'required',
            'otp_code'  => 'required',
            'password'  => 'required'
        ]);
        $user = User::where('mobile',$request->mobile)->where('otp_code',$request->otp_code)->get();
        if(count($user) == 1)
        {
            $admin = User::where('mobile', $request->mobile)->where('otp_code',$request->otp_code)->first();
            // dd($admin);
            if(!empty($admin->id))
            {
               
                $admin->password = Hash::make($request->password);
                $admin->save();

                return redirect('/login')->with('success', 'Success! Your password successfully changed.');
            }
        }
            
    }
    public function destroy(Inquery $inquery)
    {
        $inquery->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function logout()
    {   
        Auth::logout();

        return redirect('/')->with('success', 'Success! Logout');
    }

    public function resend_otp(Request $request)
    {
        $sess_register = session()->get('sess_register');
       
        if($sess_register['mobile'])
            {
                
                $setting = Setting::find(1);
                $otp = rand(100000,999999);                
                
                $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
                // dd($msg);
                $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$sess_register['mobile'],$msg], $setting->sms_api);

                // dd($apiUrl);
                $sms    = file_get_contents($apiUrl);
                
                $sess_register['otp_code']=$otp;
                session(['sess_register' => $sess_register]);
                $sess_register = session()->get('sess_register');
                // dd($sess_register);
                
            } 
        return redirect()->back()->with('success', 'Success! OTP resend successful');
    }
}
