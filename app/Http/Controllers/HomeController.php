<?php

namespace App\Http\Controllers;

use App\Model\About;
use App\Model\Brand;
use App\Model\Faq;
use App\Model\HowItWork;
use Illuminate\Http\Request;
use App\Model\Page;
use App\Model\Post;
use App\Model\Service;
use App\Model\Setting;
use App\Model\Plan;
use App\Model\Slider;
use App\Model\SmartPhone;
use App\Model\Testimonial;
use App\Model\Insurance;
use NumberFormatter;

class HomeController extends Controller
{
    
    public function home()
    {
        
        $sliders = Slider::where('image', '!=', null)->get();
        // dd($sliders);
        $testimonials = Testimonial::get();
        // dd($testimonials);
        $faqs = Faq::latest()->paginate(4);
        // dd($faqs);
        $brands = Brand::get();
        // dd($brands);
        $smartphones = SmartPhone::latest()->paginate(6);
        // dd($smartphones);
        $howitwork = HowItWork::orderBy('id', 'ASC')->latest()->paginate(6);
        // dd($howitwork);
        $setting = Setting::find(1);
                

        $data = compact('sliders', 'testimonials', 'howitwork', 'faqs', 'brands', 'setting', 'smartphones');
        return view('frontend.inc.homepage', $data);
    }
    

    public function service()
    {
        $lists = Service::get();
        $data = compact('lists');
        return view('frontend.inc.service', $data);
    }

    public function about(Page $slug)
    {

        // $about = About::where('slug',$slug->slug)->first();
        $data = compact('slug');
        return view('frontend.inc.about', $data);
    }

    public function contact()
    {
        $setting = Setting::find(1);
        $data = compact('setting');
        return view('frontend.inc.contact', $data);
    }

    public function faq(Request $request)
    {
        // $lists = Faq::get();

        $query = Faq::latest();

        if (!empty($request->title)) {
            $query->where('title', 'LIKE', '%' . $request->title . '%');
        }
        $lists = $query->get();

        $data = compact('lists');
        return view('frontend.inc.faq', $data);
    }
    public function plan(Request $request)
    {
        $get_price = (int)$request->price;

        $plan_filter_arr = $request->all();
       
        $lists = Plan::with('rule')->where('minprice','<=',$get_price)->where('maxprice','>=',$get_price)->where('active',true)->get();
        
        $data = compact('lists','plan_filter_arr');
        return view('frontend.inc.plan', $data);
    }
}
