<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Insurance;
use App\Model\Plan;
use Illuminate\Http\Request;
use App\Model\PlanCondition;
use App\Model\Setting;
use App\Model\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Image;
use Mail;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Insurance::paginate(10);

        $data = compact('lists');
        return view('backend.inc.insurance.list', $data);
    }

    public function insurance_re(Request $request)
    {
        // dd($request);
        $plan_arr = $request->all();
        $plan = Plan::findOrFail($request->plan_id);
        $data = compact('plan_arr','plan');
        return view('frontend.inc.insurance', $data);
        // return redirect()->route('insurance.add',$request->plan_id);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create($plan)
    // {   
    //     $plan = Plan::findOrFail($plan);
    //     $data = compact('plan');
    //     return view('frontend.inc.insurance', $data);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$plan)
    {
        // dd($request);
        session()->forget('sess_req');
        if (@$request->record['id']) {
            $record           = Insurance::with('plan')->findOrFail($request->record['id']);
            // dd($record->plan);
            $input            = $request->record;

            if ($input['payment_mode'] == 'online') {
                $input['payment_status'] = 'paid';

                $input['status'] = true;
                $record->fill($input);
                if($record->update()){

                // $record = Insurance::findOrFail($record->id);
                    $tot_price = $record->payment * 1;
                    //display the result
                    $gst_price = $tot_price * 0.09;



                    $price = $tot_price - ($gst_price + $gst_price);

                    $inword_price = Insurance::convert_number_to_words($tot_price);
                    
                    $setting = Setting::find(1);

                    $subject = "Your Service ID : ".$record->invoice_pre.''.$record->invoice_no;
                    $email_params = [
                        // 'to'        => env('MAIL_USERNAME'),
                        'to'        => $setting->email,
                        'reciever'  => $setting->title,
                        'from'      => $record->email,
                        'sender'    => $record->name,
                        'subject'   => $subject,
                        'insurance' => $record,
                        'plan'      => $record['plan'],
                        'setting'   => $setting,
                        'price'     => $price,
                        'gst_price' => $gst_price,
                        'inword_price' => $inword_price

                    ];
                    
                    // dd($email_params);
                    Mail::send('email.invoice', $email_params, function ($msgEmail) use ($email_params) {
                        extract($email_params);

                        // $msgEmail->to($to, $reciever)
                        // ->subject($subject)
                        // ->from($from, $sender)
                        // ->replyTo($to, $reciever);
                        $msgEmail->to($from, $sender)
                            ->subject($subject)
                            ->from($to, $reciever)
                            ->replyTo($from, $sender);
                    });
                }
            } else {
                $input['txn_id'] = null;
                $record->delete();
            }
            

            return response()->json($record);
        } else {
            $record           = new Insurance;
            $input            = $request->record;

            if ($request->hasFile('photo')) {
                
                $file = $request->photo;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/photo/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/photo/' . $name;
                $input['photo'] = $url;
            }
            if ($request->hasFile('aadharcard1')) {
                $file = $request->aadharcard1;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/aadharcardfront/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/aadharcardfront/' . $name;
                $input['aadharcard1'] = $url;
            }
            if ($request->hasFile('aadharcard2')) {
                $file = $request->aadharcard2;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/aadharcardback/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/aadharcardback/' . $name;
                $input['aadharcard2'] = $url;
            }
            if ($request->hasFile('mobile1')) {
                $file = $request->mobile1;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/mobilefirst/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/mobilefirst/' . $name;
                $input['mobile1'] = $url;
            }
            if ($request->hasFile('mobile2')) {
                $file = $request->mobile2;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/mobilesecond/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/mobilesecond/' . $name;
                $input['mobile2'] = $url;
            }
            if ($request->hasFile('invoice')) {
                $file = $request->invoice;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/invoice/';
                if (!file_exists($optimizePath)) {
                    mkdir($optimizePath, 0755, true);
                }
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/invoice/' . $name;
                $input['invoice'] = $url;
            }

            $plan = Plan::findOrFail($plan);
            $input['customer_id'] = auth()->user()->id;
            $input['payment'] = $plan->price;
            $maxInsurance = Insurance::latest()->first();
            $input['invoice_no']    =  sprintf('%06d', $maxInsurance->invoice_no+1);
            $record->fill($input);
            $record->save();

            $data = compact('record','plan');
            return view('frontend.inc.insurance', $data);
            // return redirect(route('admin.insurance.create', $id))->with('success', 'Success! New record has been added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\planplanplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(PlanCondition $plan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\planplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role, $staff)
    {
        $staff = Admin::find($staff);
        $editData =  ['record' => $staff->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('staff', 'role');
        return view('backend.inc.staff.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role, $staff)
    {
        $staff = PlanCondition::find($staff);
        $record     = $staff;
        $input      = $request->record;
        if ($input['password']) {
            $input['password']    = Hash::make($input['password']);
        }
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.staff.index', $role))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy($staff)
    {
        $staff = Admin::findOrFail($staff);
        $staff->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted.');
    }

    public function status($admin, $status)
    {
        $admin = Admin::findOrFail($admin);
        $admin->active = $status;
        $admin->save();
        // dd($admin);
        return redirect()->back()->with('success', 'Success! Status has been updated.');
    }
}
