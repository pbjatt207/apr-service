<?php

namespace App\Http\Controllers;

use App\Model\Inquery;
use App\Model\Setting;
use App\Model\Insurance;
use App\Model\User;
use App\Model\City;
use Illuminate\Http\Request;
use Mail;
use Hash;
use Auth;

class ProfileController extends Controller
{
    public function profile()
    {
        $user_id = auth()->user()->id;

        // $cities = City::get();
        // $city_arr = [];
        // foreach($cities as $key => $c){
        //     $city_arr[$c->id] = $c->name;
        // }

        // $states = State::get();
        // $state_arr = [];
        // foreach($states as $key => $s){
        //     $state_arr[$s->id] = $s->name;
        // }
        
        $repair_services = Insurance::where('customer_id',$user_id)->where('status',true)->get();
        $data = compact('repair_services');
        return view('frontend.inc.profile',$data);
    }
    public function update(Request $request)
    {
        $rules = [
            'fname'   => 'required',
            'email'   => 'required|email',
        ];

        $messages = [
            'fname.required'  => 'Please Enter Name.',
            'email.required'  => 'Please Enter Email.',

        ];

        $request->validate($rules, $messages);
        $input            = $request->all();
        // dd($input);
        
        $record           = Auth::user();
        $record->fill($input);
        $record->address = $input['address'];
        $record->save();

        return redirect()->back()->with('success', 'Profile has been updated successfully.');
    }

    public function changepassword(Request $request)
    {
        $rules = [
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ];
        $request->validate($rules);

        $new_password = Hash::make($request->new_password);
        Auth()->user()->update(['password' => $new_password]);
        return redirect()->back()->with('success', 'Your password has been changed successfully.');
    }
}
