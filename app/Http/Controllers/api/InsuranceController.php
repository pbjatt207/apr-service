<?php
namespace App\Http\Controllers\api;

use Validator;
use Mail;
use Auth;
use DB;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Insurance;
use App\Model\Admin;
use App\Model\Role;
use App\Model\Setting;
use App\Model\Plan;
use App\Model\Brand;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class InsuranceController extends Controller
{
    public function insurance_list(Request $request)
    {
        $user = Auth::user();
        // dd($user);
        $lists = Insurance::where('user_id', $user->id)->where('status',true)->with('admin', 'user', 'plan')->latest()->paginate(50);

        if($user->role_id == '1'){                
            $lists = Insurance::with('admin', 'user','plan')->where('status',true)->latest()->paginate(50);                
        }
        if($user->role_id == '2'){
               
                

                $staff = Admin::where('admin_id',$user->id)->whereIn('role_id',[3,5])->get();
                $staff_arr = [];
                foreach($staff as $s){
                    $staff_arr[] = $s->id;
                    // $lists = Insurance::whereIn('user_id',[$s->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
                }
                
                foreach($staff as $s){
                    $shop = Admin::where('admin_id',$s->id)->where('role_id',3)->get();
                    $shop_arr = [];
                    foreach($shop as $shop){
                        $shop_arr[] = $shop->id;
                        // $lists = Insurance::whereIn('user_id',[$s->id,$shop->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
                    }
                }
                $total_array = array_merge($staff_arr, $shop_arr);
                $total_array[] = $user->id;
                

                $lists = Insurance::whereIn('user_id',$total_array)->where('status',true)->with('admin', 'user','plan')->latest()->paginate(10);
                
            }
            
            if($user->role_id == '3'){
                $lists = Insurance::where('user_id',$user->id)->where('status',true)->with('admin', 'user', 'plan')->latest()->paginate(10);
            }
            if($user->role_id == '5'){
                $find_shop = Insurance::whereHas('admin', function($q) use($user){
                    $q->where('admin_id', '=', $user->id);
                })->get();
                if(count($find_shop) != '0')
                foreach($find_shop as $fs){
                    $lists = Insurance::whereIn('user_id',[$user->id,$fs->user_id])->where('status',true)->with('admin', 'user', 'plan')->latest()->paginate(10);
                } else {
                    $lists = Insurance::where('user_id',$user->id)->where('status',true)->with('admin', 'user', 'plan')->latest()->paginate(10);
                }
            }
            if($user->role_id == '4'){
                $lists = Insurance::where('customer_id',$user->id)->where('status',true)->with('admin', 'user', 'plan')->latest()->paginate(10);
            }
        
        $re = [
            'status'    => true,
            'message'   => 'total records ' .count($lists),
            'data'      => $lists,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }



    public function add_insurance(Request $request)
    {
        // $role = 'Shop';
        $user = auth()->user();
        
         
        
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|min:10',
            "email"      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            
            // if ($request->id) {

            //     $record           = Insurance::findOrFail($request->id);
            //     $rules = [
            //         'record'        => 'required|array',
            //         'record.otp_code'   => 'required'
            //     ];
        
            //     $request->validate($rules);
            //     $input            = $request->record;
            //     // dd($record->otp_code == $input['otp_code']);
            //     if($record->otp_code == $input['otp_code']){
            //         if ($input['payment_mode'] == 'online') {
            //             $input['payment_status'] = 'paid';
            //         } else {
            //             $input['txn_id'] = null;
            //         }
            //         $input['status'] = true;
            //         $input['is_otp_verified'] = '1';
            //         $record->fill($input);
            //         // $record->update();
    
            //         if($record->update()){
            //             $insurance = Insurance::findOrFail($record->id);
                        
            //             $setting = Setting::find(1);
        
            //             $subject = "Your Service ID : ".$insurance->invoice_pre.''.$insurance->invoice_no;
            //             $email_params = [
            //                 // 'to'        => env('MAIL_USERNAME'),
            //                 'to'        => $setting->email,
            //                 'reciever'  => $setting->title,
            //                 'from'      => $record->email,
            //                 'sender'    => $record->name,
            //                 'subject'   => $subject,
            //                 'insurance' => $insurance,
            //                 'setting'   => $setting,
            //             ];
            //             // dd($email_params);
            //             Mail::send('email.insurance', $email_params, function ($msgEmail) use ($email_params) {
            //                 extract($email_params);
        
            //                 $msgEmail->to($from, $sender)
            //                 ->subject($subject)
            //                 ->from($to, $reciever)
            //                 ->replyTo($from, $sender);
    
                            
            //             });
            //         }
        
            //         return response()->json($record);
            //     } 
                
            // } else {
               
                $input            = $request->all();
                
                
    
                if ($request->hasFile('photo')) {
                    $file = $request->photo;
                    $optimizeImage = Image::make($file);
                    // $optimizeImage->resize(1903, 523);
                    $optimizePath = public_path() . '/images/insurance/photo/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/photo/' . $name;
                    $input['photo'] = $url;
                }
                if ($request->hasFile('aadharcard1')) {
                    $file = $request->aadharcard1;
                    $optimizeImage = Image::make($file);
                    // $optimizeImage->resize(1903, 523);
                    $optimizePath = public_path() . '/images/insurance/aadharcardfront/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/aadharcardfront/' . $name;
                    $input['aadharcard1'] = $url;
                }
                if ($request->hasFile('aadharcard2')) {
                    $file = $request->aadharcard2;
                    $optimizeImage = Image::make($file);
                    // $optimizeImage->resize(1903, 523);
                    $optimizePath = public_path() . '/images/insurance/aadharcardback/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/aadharcardback/' . $name;
                    $input['aadharcard2'] = $url;
                }
                if ($request->hasFile('mobile1')) {
                    $file = $request->mobile1;
                    $optimizeImage = Image::make($file);
                    // $optimizeImage->resize(1903, 523);
                    $optimizePath = public_path() . '/images/insurance/mobilefirst/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/mobilefirst/' . $name;
                    $input['mobile1'] = $url;
                }
                if ($request->hasFile('mobile2')) {
                    $file = $request->mobile2;
                    $optimizeImage = Image::make($file);
                    $optimizePath = public_path() . '/images/insurance/mobilesecond/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/mobilesecond/' . $name;
                    $input['mobile2'] = $url;
                }
                if ($request->hasFile('invoice')) {
                    $file = $request->invoice;
                    $optimizeImage = Image::make($file);
                    $optimizePath = public_path() . '/images/insurance/invoice/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/insurance/invoice/' . $name;
                    $input['invoice'] = $url;
                }
                
                
                if($user->role_id == '4'){
                    $otp = '';
                    $input['otp_code'] = '';
                    $input['is_otp_verified']= '1';
                    $input['customer_id'] = auth()->user()->id;
                } else {
                    $setting = Setting::find(1);
                    $otp = rand(100000,999999);
                    $input['otp_code'] = $otp;
                    $input['is_otp_verified']= '0';
                    
                    $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
                    // $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$input['mobile'],$msg], $setting->sms_api);
                    // $sms    = file_get_contents($apiUrl);
                    $input['user_id'] = auth()->user()->id;
                }
                // unset($input['role']);
                $maxInsurance = Insurance::latest()->first();
                $input['invoice_no']    =  sprintf('%06d', $maxInsurance->invoice_no+1);
                // $input['payment_mode'] = '';
                $input['payment_status'] = 'DUE';
                $record = new Insurance($input);
                
                
    
                $plan = Plan::where('active', true)->get();
                $planArr  = ['' => 'Select plan'];
                if (!$plan->isEmpty()) {
                    foreach ($plan as $b) {
                        $planArr[$b->price] = $b->name . ' (' . $b->price . ' ₹)';
                    }
                }
                if($record->save()){
                    $record = Insurance::find($record->id);
                    $re = [
                        'status'    => true,
                        'message'   => 'Insurance added successfully',
                        'otp'       => $otp,
                        'data'    => $record,

                    ];
                    $code = 200;
                } else {
                    $re = [
                        'status'    => false,
                        'message'   => 'error not added',

                    ];
                    $code = 401;
                }
                
                // return redirect(route('admin.insurance.create', $id))->with('success', 'Success! New record has been added.');
            // }
        }

        return response()->json($re, $code);
        
    }
    public function update_insurance(Request $request)
    {
        
        $record           = Insurance::with('plan')->findOrFail($request->id);
        // dd($record);
            // $rules = [
            //     'record'        => 'required|array',
            //     'record.otp_code'   => 'required'
            // ];
    
            // $request->validate($rules);
            
            $input            = $request->all();
            
            // dd($record->otp_code == $input['otp_code']);
            // if($record->otp_code == $input['otp_code']){
            
                // if ($input['payment_mode'] == 'online') {
                    $input['payment_status'] = 'paid';
                // } else {
                //     $input['txn_id'] = null;
                // }
                $input['status'] = true;
                $input['is_otp_verified'] = '1';
                $record->fill($input);
                // $record->update();

                if($record->update()){
                    $tot_price = $record->payment * 1;
                //display the result
                $gst_price = $tot_price * 0.09;

                $price = $tot_price - ($gst_price + $gst_price);

                $inword_price = Insurance::convert_number_to_words($tot_price);
                
                $setting = Setting::find(1);

                $subject = "Your Service ID : ".$record->invoice_pre.''.$record->invoice_no;
                $email_params = [
                    // 'to'        => env('MAIL_USERNAME'),
                    'to'        => $setting->email,
                    'reciever'  => $setting->title,
                    'from'      => $record->email,
                    'sender'    => $record->name,
                    'subject'   => $subject,
                    'insurance' => $record,
                    'plan'      => $record['plan'],
                    'setting'   => $setting,
                    'price'     => $price,
                    'gst_price' => $gst_price,
                    'inword_price' => $inword_price

                ];
                
                // dd($email_params);
                Mail::send('email.invoice', $email_params, function ($msgEmail) use ($email_params) {
                    extract($email_params);

                    // $msgEmail->to($to, $reciever)
                    // ->subject($subject)
                    // ->from($from, $sender)
                    // ->replyTo($to, $reciever);
                    $msgEmail->to($from, $sender)
                        ->subject($subject)
                        ->from($to, $reciever)
                        ->replyTo($from, $sender);
                });
                }
    
                return response()->json($record);
            
    }

    public function brand_list(Request $request)
    {
        $lists  = Brand::pluck('name')->toArray();
        
        $re = [
            'status'    => true,
            'message'   => 'total records ' .count($lists),
            'data'      => $lists,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }
}