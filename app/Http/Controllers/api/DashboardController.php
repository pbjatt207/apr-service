<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Insurance;
use App\Model\Plan;
use App\Model\User;
use Illuminate\Http\Request;
use App\Model\PlanCondition;
use App\Model\Role;
use App\Model\Brand;
use App\Model\Setting;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Image;

use PDF;
use Validator;
use Mail;
use DateTime;
use Auth;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {   
    $user = Auth::user();
    
    if($user->role_id == '1'){
        $franchise = Admin::where('role_id','2')->get();
        $shop = Admin::where('role_id','3')->get();
        $staff = Admin::where('role_id','5')->get();
        $insurance = Insurance::where('status',true)->get();
        $re = [
                'status'        => true,
                'franchise'     => count($franchise),
                'shop'          => count($shop),
                'insurance'     => count($insurance),
                'staff'         => count($staff)
                ];
    }
    if($user->role_id == '2'){
        // $franchise = Admin::where('role_id','2')->count();
        
        
        $staff = Admin::where('admin_id',$user->id)->whereIn('role_id',[3,5])->get();
        $staff_arr = [];
        foreach($staff as $s){
            $staff_arr[] = $s->id;
            // $lists = Insurance::whereIn('user_id',[$s->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
        }
        
        foreach($staff as $s){
            $shop = Admin::where('admin_id',$s->id)->where('role_id',3)->get();
            $shop_arr = [];
            foreach($shop as $shop){
                $shop_arr[] = $shop->id;
                // $lists = Insurance::whereIn('user_id',[$s->id,$shop->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
            }
        }
        $total_array = array_merge($staff_arr, $shop_arr);
        $total_array[] = $user->id;
        

        $insurance = Insurance::whereIn('user_id',$total_array)->where('status',true)->with('admin', 'user','plan')->get();
        
        $shop = Admin::where('role_id','3')->where('admin_id',$user->id)->get();
        
        $staff = Admin::where('role_id','5')->where('admin_id',$user->id)->get();
        // dd($staff);
        
        $re = [
                'status'        => true,
                'shop'          => count($shop),
                'insurance'     => count($insurance),
                'staff'         => count($staff)
                ];
    }
    if($user->role_id == '3'){
        $insurance = Insurance::where('user_id',$user->id)->where('status',true)->get();
        $re = [
                'status'        => true,
                
                'insurance'     => count($insurance)
                ];
    }
    if($user->role_id == '5'){
        $insurance = Insurance::where('user_id',$user->id)->where('status',true)->get();
        $re = [
                'status'        => true,
                
                'insurance'     => count($insurance)
                ];
    }
    
    
            $code = 200;
    return response()->json($re, $code);
        
    }
}