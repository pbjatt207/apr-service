<?php
namespace App\Http\Controllers\api;

use Validator;
use Mail;
use Auth;
use DB;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Slider;
use App\Model\Admin;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class StaffController extends Controller
{
    public function staff_list(Request $request)
    {
        // dd(auth()->user());
        // $role = 'Franchi';
        $role = $request->role;
        // dd($role);
        $query = Admin::whereHas('role', function ($q) use ($role) {
            $q->where('name', $role);
        });
        if (auth()->user()->role_id == 2) {
            $query->where('admin_id', auth()->user()->id);
        }
        $lists = $query->get();
        
        $re = [
            'status'    => true,
            'message'   => 'total records ' .count($lists),
            'data'      => $lists,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }
    
     public function staff_Detail(Request $request )
    {
        $Data = DB::table('admins')->find($request->id);
        
        foreach($Data as $index => $arr){
                
                // foreach($arr as $key => $value){
                    
                    if($arr == null){
                        
                     $Data->{$index}= ''; 
                    }
                // }
            }
            
        $re = [
            'status'    => true,
            'data'      => $Data,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }

    public function add_staff(Request $request)
    {
        // $role = 'Franchise';
       
        $validator = Validator::make($request->all(), [
            'fname'      => 'required',
            'mobile'     => 'required|unique:admins|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10',
            "password"   => "required",
            "email"      => 'required|email|unique:admins'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            
            $roleid = Role::where('name', $request->role)->first();
            // dd($roleid->id);
            $input            = $request->all();
            if ($request->hasFile('aadhar_front')) {
                $file = $request->aadhar_front;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/aadhar/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['aadhar_front'] = $name;
            }
            if ($request->hasFile('aadhar_back')) {
                $file = $request->aadhar_back;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/aadhar/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['aadhar_back'] = $name;
            }
            if ($request->hasFile('pan_img')) {
                $file = $request->pan_img;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/pan_card/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['pan_img'] = $name;
            }
            if ($request->hasFile('bank_statement')) {
                $file = $request->bank_statement;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/bank_statement/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['bank_statement'] = $name;
            }


        
        unset($input['role']);
        
        $input['password']    = bcrypt($input['password']);
        $input['role_id']    = $roleid->id;
        $input['admin_id'] = auth()->user()->id;
            
        
        
		$input['is_otp_verified']= '0';
		
		
        $record           = new Admin($input);
        if ($record->save()) {
            $re = [
                'status'    => true,
                'message'   => 'Success, New record add successful',
            ];
            $code = 200;
        }

        }

        return response()->json($re, $code);
        
    }
   

    public function edit_staff(Request $request)
    {
        // $role = 'Franchise';
        // dd($request);
       
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:10|unique:admins,id,'.$request->id,
            "email"      => 'required|email|unique:admins,id,'.$request->id
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            
            
            // dd($roleid->id);
            $input            = $request->all();
            
            if ($request->hasFile('aadhar_front')) {
                $file = $request->aadhar_front;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/aadhar/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['aadhar_front'] = $name;
            }
            if ($request->hasFile('aadhar_back')) {
                $file = $request->aadhar_back;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/aadhar/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['aadhar_back'] = $name;
            }
            if ($request->hasFile('pan_img')) {
                $file = $request->pan_img;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/pan_card/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['pan_img'] = $name;
            }
            if ($request->hasFile('bank_statement')) {
                $file = $request->bank_statement;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/admin/bank_statement/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $input['bank_statement'] = $name;
            }


        
        unset($input['role']);
        if($request->password){
        $input['password']    =  bcrypt($input['password']);
        }else{
            unset($input['password']);
        }
            
// 		dd($input);
		
        $record           = Admin::find($input['id']);
        // dd($record);
        $record->fill($input);
        if ($record->save()) {
            $re = [
                'status'    => true,
                'message'   => 'Success, record update successful',
            ];
            $code = 200;
        }

        }

        return response()->json($re, $code);
        
    }
}