<?php
namespace App\Http\Controllers\api;

use Validator;
use Mail;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Plan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PlanController extends Controller
{
    public function Plan(Request $request)
    {
        $get_price          = (int) $request['price'];
        $lists  = Plan::with('rule')->where('minprice','<=',$get_price)->where('maxprice','>=',$get_price)->where('active',true)->get();
        
        $re = [
            'status'    => true,
            'message'   => 'total records ' .count($lists),
            'data'      => $lists,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }
}