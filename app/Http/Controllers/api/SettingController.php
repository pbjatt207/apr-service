<?php
namespace App\Http\Controllers\api;

use Validator;
use Mail;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SettingController extends Controller
{
    public function get_setting(Request $request)
    {
        $Data  = DB::table('settings')->find(1);
        
        foreach($Data as $index => $arr){
                
                // foreach($arr as $key => $value){
                    
                    if($arr == null){
                        
                     $Data->{$index}= ''; 
                    }
                // }
            }
        
        $re = [
            'status'    => true,
            'message'   => 'Success',
            'data'      => $Data,
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }
    
    public function edit_setting(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'title'     => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $record     = Setting::find(1);
            $input      = $request->all();
            if ($request->hasFile('logo')) {
                $file = $request->logo;
                $optimizeImage = Image::make($file);
                $optimizePath = public_path() . '/images/setting/logo/';
                $name = 'logo.png';
                $optimizeImage->save($optimizePath . $name, 72);
                $input['logo'] = $name;
            }
            if ($request->hasFile('favicon')) {
                $file = $request->favicon;
                $optimizeImage = Image::make($file);
                $optimizeImage->resize(70, 70);
                $optimizePath = public_path() . '/images/setting/favicon/';
                $name = 'favicon.png';
                $optimizeImage->save($optimizePath . $name, 72);
                $input['favicon'] = $name;
            }
    
            $record->fill($input);
            if ($record->save()) {
                $re = [
                    'status'    => true,
                    'message'   => 'Success.'
                ];
                $code = 200;
            }
        }
        return response()->json($re, $code);
    }
}