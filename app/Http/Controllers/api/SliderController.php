<?php
namespace App\Http\Controllers\api;

use Validator;
use Mail;
use Auth;
use DB;
use Intervention\Image\ImageManagerStatic as Image;
use App\Model\Slider;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class SliderController extends Controller
{
    public function Slider(Request $request)
    {
        $Data  = DB::table('sliders')->get();
        
         foreach($Data as $index => $arr){
                // dd($arr);
                foreach($arr as $key => $value){
                    
                    if($value == null){
                        
                     $Data[$index]->{$key}= ''; 
                    }
                }
            }
            
        $re = [
            'status'    => true,
            'message'   => 'total records ' .count($Data),
            'data'      => $Data,
            // 'isExists'  => $user->count()
        ];
        $code = 200;
            
        return response()->json($re, $code);
    }
}