<?php
namespace App\Http\Controllers\api;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;
use Mail;
use DateTime;
use Auth;
use DB;
// use Hash;
use Illuminate\Support\Facades\Hash;

use Intervention\Image\ImageManagerStatic as Image;
// use Image;
// use Intervention\Image\Image;
use App\Model\User;
use App\Model\Admin;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon;
use App\Models\AppModels\Customer;

class AuthController extends Controller
{
    public function Profile(Request $request)
    {
        $user = Auth::user();
        $re = [
            'status'    => true,
            'data'      => $user
            ];
        $code = 200;
        return response()->json($re, $code);
    }
    
    public function editProfile(Request $request)
    {
        $user = Auth::user();
        
        $input = $request->all();
        if ($request->hasFile('image')) {
                    $file = $request->image;
                    $optimizeImage = Image::make($file);
                    // $optimizeImage->resize(1903, 523);
                    $optimizePath = public_path() . '/images/user/';
                    $name = time() . $file->getClientOriginalName();
                    $optimizeImage->save($optimizePath . $name, 72);
                    $url = 'public/images/user/' . $name;
                    $input['image'] = $url;
                }
        if($user->update($input)){
            $re = [
                'status'        => true,
                'message'       => 'Update Successful',
                'user_details'  => $user
                ];
            $code = 200;
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Error'
                ];
            $code = 401;
            
        }
        return response()->json($re, $code);
    }
    
    public function sendOtp(Request $request)
    {
        
        $setting = Setting::find(1);
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|numeric|regex:/\d{10}/',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
           

            $message = urlencode("Your One Time otp_to_password (OTP) is {$otp} for {$setting->app_name}.");
            // print_r($setting);
            $sms_url = str_replace(["[MOBILE]", "[MESSAGE]"], [request('mobile'), $message], $setting->sms_api);
            // die;

            $sms = file_get_contents($sms_url);

            $user = $request->type == 'Admin' ? Admin::where('mobile', request('mobile')) : User::where('mobile', request('mobile'));

            if ($user->count()==1) {
                $otp = rand(100000, 999999);
                $dataArr = [
                    'otp_code' => $otp,
                    'otp_to_password'  => bcrypt($otp),
                ];
                
                $request->type == 'Admin' ? Admin::updateOrCreate(['mobile' => request('mobile')], $dataArr) : User::updateOrCreate(['mobile' => request('mobile')], $dataArr);
                $re = [
                    'status'    => true,
                    'message'   => 'OTP has been sent to '.request('mobile'),
                    'otp_code'  => $otp,
                    // 'isExists'  => $user->count()
                ];
                $code = 200;
            }
             else {
                $re = [
                    'status'    => false,
                    'message'   => 'Mobile No. '.request('mobile').' Not Registered ',
                    // 'isExists'  => $user->count()
                ];
                $code = 200;
                // $dataArr = [
                    
                //     'mobile' => request('mobile'),
                //     'otp_code' => $otp,
                //     'otp_to_password'  => bcrypt($otp),
                    
                // ];
                // $new_user = new User($dataArr);
                // $new_user->save();
                
                
            }

        }
        return response()->json($re, $code);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|numeric|regex:/\d{10}/',
            "password"    => "required",
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            if($request->type == 'Admin'){
                $user_check = Admin::where('mobile',request('mobile'));
                
            } else {
                $user_check = User::where('mobile',request('mobile'));
            }
            
            $is_remembered = !empty($request->remember_me) ? true : false;
            // dd($request->only('mobile','password'));


            if($user_check->count() == 1){
                $auth = $request->type == 'Admin' ? Auth::guard('admin')->attempt(['mobile' => request('mobile'), 'password' => request('password')]) : Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')]);
            if ($auth) {
                
                $user = $request->type == 'Admin' ? Auth::guard('admin')->user() : Auth::user();
                // dd($user);
                $token  = $user->createToken('APRServices')->accessToken;
                
                $re = [
                    'status'    => true,
                    'message'   => 'Success!! Login successful.',
                    'token' => $token,
                    'user_details' => $user,
                ];
                $code = 200;
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Mobile no or password not match',
                ];
                $code = 401;
            }
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Mobile no not registered',
            ];
            $code = 401;
        }
            

        }   

        return response()->json($re);
    }
    
    public function verifyOtp(Request $request)
    {
        
        $OTP = request('otp_code');
        $mobile = request('mobile');
        
        $userCount = $request->type == 'Admin' ? Admin::where('otp_code',$OTP)->where('mobile',$mobile)->count() : User::where('otp_code',$OTP)->where('mobile',$mobile)->count();
        
        
        
        if($userCount == 1){
            $users = $request->type == 'Admin' ? Admin::where('otp_code',$OTP)->where('mobile',$mobile)->first() : User::where('otp_code',$OTP)->where('mobile',$mobile)->first();
            
            $users->is_otp_verified = 1;
            $users->update();
                    
            $re = [
                'status'    => true,
                'message'   => 'Success!! Verify successful.',
                'otp_code' => $OTP,
            ];
        } else {
            $re = [
                'status'    => false,
                'message'       => 'Otp not matched'
            ];
        }
        
    
        
        return response()->json($re);
    }

	public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fname'             => 'required|string|regex:/[A-Za-z ]+/',
            'mobile'            => 'required|unique:users|min:10',
            // 'email'             => 'required|email|unique:users',
            'password'          => 'required'
            // 'is_termscondition' => 'required'
        ]);
        
                           
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {

            // $UserData =  User::where('mobile', $request->mobile)->whereHas('role', function ($q) {
            //     $q->where('name', 'User');
            // })->first();
            

            $check  =  User::where('mobile', $request->mobile)->count();
           

            // $fname = strtoupper(substr($request->fname, 0, 3));
            
            if ($check == 0) {
                $otp = rand(100000, 999999);
                $dataArr = [
                    'fname'             => request('fname'),
                    // 'email'             => request('email'),
                    'mobile'            => request('mobile'),
                    'otp_code'          => $otp,
                    'is_otp_verified'   => '0',
                    'password'          => Hash::make(request('password')),
                    // 'device_id' => request('device_id'),
                    // 'device_type' => request('device_type'),
                    // 'fcm_id'    => request('fcm_id'),
                    // 'google_id'    => request('google_id'),
                    // 'fb_id'    => request('fb_id'),
                    // 'social_type'    => request('social_type'),
                    // 'is_termscondition'=> request('is_termscondition'),
                    // 'role_id'     => $role->id,
                    // 'city_id'     => request('city_id'),
                    // 'accept_code'  => $request->accept_code,
                ];
                
                $newUser = new User($dataArr);
                
                $newUser->save();
                $re = [
                    'status'    => true,
                    'message'   => 'Successful registered',
                ];
                // if($newUser->data_fill == 1){
                    
                //     Auth::attempt(['mobile' => request('mobile'), 'otp_to_password' => $newUser->otp]);
                //     $user   = Auth::user();
                    
            
                //     $token  = $user->createToken('fishApp')->accessToken;

                //     $re = [
                //         'status'    => true,
                //         'message'   => 'Success!! Registration successful.',
                //         'token' => $token,
                //         'user_details' => $user,
                //     ];
                    
                // }

            }
        }
        return response()->json($re);
    }
    public function change_password(Request $request){
        $user = Auth::user();
        
        $validator = Validator::make($request->all(), [
            'current_password'            => 'required',
            'new_password'             => 'required',
            'confirm_password'          => 'required|same:new_password',
        ]);
        
                           
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 403;
        } else {
            $user = Auth::user();
            if(Hash::check($request->current_password, $user->password)){
                $new_password = Hash::make($request->new_password);
                $user->update(['password'=>$new_password]);
                
                $re = [
                    'status' => true,
                    'message' => 'Password change successfully',
                    ];
                    $code = 200;
            } else {
                $re = [
                    'status' => false,
                    'message' => 'Please enter correct current password',
                    
                    ];
                     $code = 403;
            }
        }
        return response()->json($re);
    }
    public function forgot_password(Request $request){
        
        
        $validator = Validator::make($request->all(), [
            
            'new_password'             => 'required',
            'confirm_password'          => 'required|same:new_password',
        ]);
        
                           
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 403;
        } else {
            
            if($request->new_password){
                $user = $request->type ? Admin::where('mobile',$request->mobile)->where('otp_code',$request->otp_code) : User::where('mobile',$request->mobile)->where('otp_code',$request->otp_code);
                
                $new_password = Hash::make($request->new_password);
                $user->update(['password'=>$new_password]);
                
                $re = [
                    'status' => true,
                    'message' => 'Password change successfully',
                    ];
                    $code = 200;
            } else {
                $re = [
                    'status' => false,
                    'message' => 'Please enter correct old password',
                    
                    ];
                     $code = 403;
            }
        }
        return response()->json($re);
    }
    public function logout()
    {   
        Auth::logout();

        $re = [
            'status'    => true,
            'message'   => 'Success!! Logout successful.',
        ];
        return response()->json($re);
    }
}