<?php

namespace App\Http\Controllers;

use App\Model\Plan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = $request->user();
        $sess_req = session()->get('sess_req');
        $plan_filter_arr = empty($request->price) && $sess_req ? $sess_req : $request->all();
        
        if($user) {
            $get_price          = (int) $plan_filter_arr['price'];
            $lists  = Plan::with('rule')->where('minprice','<=',$get_price)->where('maxprice','>=',$get_price)->where('active',true)->get();        
            $data   = compact('lists','plan_filter_arr');
            return view('frontend.inc.plan', $data);
        } else {
            session(['sess_req' => $plan_filter_arr]);
            return view('frontend.inc.login');
        }
    }
}
