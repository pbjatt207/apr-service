<?php

namespace App\Http\Controllers;

use App\Model\Inquery;
use App\Model\Setting;
use Illuminate\Http\Request;
use Mail;

class InqueryController extends Controller
{
    public function index()
    {
        $setting = Setting::find(1);
        $data = compact('setting');
        return view('frontend.inc.contact', $data);
    }

    public function store(Request $request)
    {
        
        

        $record           = new Inquery;
        $record->name            = $request->name;
        $record->email            = $request->email;
        $record->mobile            = $request->mobile;
        $record->subject            = $request->subject;
        $record->message            = $request->message;

        $record->save();

        $setting = Setting::find(1);

        $subject = "New Enquiry at ".$setting->title;
        $email_params = [
            // 'to'        => env('MAIL_USERNAME'),
            'to'        => $setting->email,
            'reciever'  => $setting->title,
            'from'      => $request->email,
            'sender'    => $request->name,
            'subject'   => $subject,
            'form_subject' => $request->subject,
            'mobile'    => $request->mobile,
            'msg'       => $request->message,
        ];
        // dd($email_params);
        Mail::send('frontend.inc.email', $email_params, function ($msgEmail) use ($email_params) {
            extract($email_params);

            $msgEmail->to($to, $reciever)
            ->subject($subject)
            ->from($from, $sender)
            ->replyTo($to, $reciever);
        });

        return redirect()->back()->with('success', 'Success! New record has been added.');
        // return redirect( url('/') )->with('success', 'Success! New record has been added.');
    }

    public function destroy(Inquery $inquery)
    {
        $inquery->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
