<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use Illuminate\Support\Str;
use Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Category::latest()->paginate(10);

        $data = compact('lists'); 
        return view('backend.inc.category.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent = Category::get();
        $parentArr  = ['' => 'Select Parent category'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('parentArr'); 
        return view('backend.inc.category.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Category;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/category/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {  
            return redirect( route('admin.category.index') )->with('success', 'Success! New record has been added.');
        } else {
            return redirect( route('admin.category.index') )->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Category $category)
    {
        $editData =  ['record'=>$category->toArray()];
        $request->replace($editData);
        $request->flash();

        $parent = Category::get();
        $parentArr  = ['' => 'Select Parent category'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('category','parentArr'); 
        return view('backend.inc.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $record     = $category;
        $input      = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/category/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }    
        
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect( route('admin.category.index') )->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
