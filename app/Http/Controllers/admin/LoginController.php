<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Hash;
use App\Model\User;
use App\Model\Setting;

class LoginController extends Controller
{
    public function index()
    {
        return view('backend.inc.login');
    }

    public function checklogin(Request $request)
    {
        $rules = [
            "login"       => "required",
            "password"    => "required",
        ];
        $request->validate($rules);

        $user_data = array(
            'mobile'     => $request->login,
            'password'  => $request->password
        );

        $is_remembered = !empty($request->remember_me) ? true : false;
        $check = Admin::where('mobile', $request->login)->first();
        $check_count = Admin::where('mobile', $request->login)->count();
        // dd($check);
        if($check_count == 0){
            return redirect()->back()->with('error', 'Mobile Number Not Registred');
        }

        if ($check->active == true) {
            
            if($check->is_otp_verified == 1){
                if (Auth::guard('admin')->attempt($user_data, $is_remembered)) {
                    return redirect(route('admin.home'));
                } else {
                    return redirect()->back()->with('error', 'Credentials not matched.');
                }
            } else {
                
                $setting = Setting::find(1);
                $otp = rand(100000,999999);
                
                $check->update(['otp_code' => $otp]);
                
                $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
                // dd($msg);
                $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$request->login,$msg], $setting->sms_api);

                // dd($apiUrl);
                $sms    = file_get_contents($apiUrl);
                return redirect('/admin-access/otp_verify?mobile='.$request->login);
            }
        } else {
            return redirect()->back()->with('error', 'Account is not active. please contact admin.');
        }
    }
    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect(route('admin.login'));
    }

    public function change_password()
    {
        return view('backend.inc.profile.changepassword');
    }

    public function save_password(Request $request)
    {
        $rules = [
            // 'current_password' => 'required|string',
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ];
        $request->validate($rules);

        $new_password = Hash::make($request->new_password);
        // if ($request->current_password != $request->new_password) {
        Auth()->user()->update(['password' => $new_password]);
        return redirect()->back()->with('success', 'Your password has been changed successfully.');
        // } else {
        //     return redirect()->back()->with('danger', 'Error!! Current and new password are same.');
        // }
    }

    public function forgotPasswordstep_i_get()
    {
        return view('backend.inc.forgot_pw_step_i');
    }
    public function forgotPasswordstep_i(Request $request)
    {
        // dd($request);
        $request->validate([
            'mobile'    => 'required|numeric|regex:/^\d{10}/'
        ]);
        
        if(empty($request->otp_code))
        {
            $admin = Admin::where('mobile', $request->mobile)->first();
            
            
            if(!empty($admin->id))
            {
                
                $otp = rand(100000, 999999);
                $admin->otp_code = $otp;
                $admin->save();
                $setting = Setting::find(1);       
        
        
                $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
                // dd($msg);
                $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$request->mobile,$msg], $setting->sms_api);

                // dd($apiUrl);
                $sms    = file_get_contents($apiUrl);

                return redirect('/admin-access/forgot_password_step-2?mobile='.$request->mobile);
            } else {
                return redirect()->back()->with('error', 'Your mobile No. not registered.');
            }
        } 
            
    }
    public function forgotPasswordstep_ii_get(Request $request)
    {
        $data = compact('request');
        return view('backend.inc.forgot_pw_step_ii',$data);
    }
    public function forgot_resend_otp(Request $request)
    {
        $admin = Admin::where('mobile', $request->mobile)->first();
        $otp = rand(100000, 999999);
        $admin->otp_code = $otp;
        $admin->save();
        $setting = Setting::find(1);       


        $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
        // dd($msg);
        $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$request->mobile,$msg], $setting->sms_api);

        // dd($apiUrl);
        $sms    = file_get_contents($apiUrl);
        return redirect()->back()->with('success', 'Success! OTP resend successful');

    }
    public function forgotPasswordstep_ii(Request $request)
    {
        // dd($request);
        $request->validate([
            'mobile'    => 'required',
            'otp_code'  => 'required',
            'password'  => 'required'
        ]);
        $user = Admin::where('mobile',$request->mobile)->where('otp_code',$request->otp_code)->get();
        
        if(count($user) == 1)
        {
            $admin = Admin::where('mobile', $request->mobile)->where('otp_code',$request->otp_code)->first();
            // dd($admin);
            if(!empty($admin->id))
            {
               
                $admin->password = Hash::make($request->password);;
                $admin->save();

                return redirect('/admin-access/login')->with('success', 'Success! Your password successfully changed.');
            }
        }
            
    }
    public function otp_verify_get(Request $request)
    {
        $data = compact('request');
        return view('backend.inc.otp_verify',$data);
    }
    public function otp_verify_post(Request $request)
    {
        $request->validate([
            'mobile'    => 'required',
            'otp_code'  => 'required',
        ]);
        $user = Admin::where('mobile',$request->mobile)->where('otp_code',$request->otp_code)->get();
        if(count($user) == 1)
        {
            $admin = Admin::where('mobile', $request->mobile)->where('otp_code',$request->otp_code)->first();
            // dd($admin);
            if(!empty($admin->id))
            {
               
                $admin->is_otp_verified = 1;
                $admin->save();

                return redirect('/admin-access/login')->with('success', 'Success! Your password successfully changed.');
            }
        } else {
                return redirect()->back()->with('error', 'Failed, OTP not matched.');
        }
    }
}
