<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Insurance;
use App\Model\Plan;
use App\Model\User;
use Illuminate\Http\Request;
use App\Model\PlanCondition;
use App\Model\Role;
use App\Model\Brand;
use App\Model\Setting;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Image;
use Mail;
use DB;
use PDF;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $role = Role::get();
        $user = auth()->user();
        // dd($user->id);
        // $lists = [];
        

            if($user->role_id == '1'){                
                $lists = Insurance::with('admin', 'user')->where('status',true)->latest()->paginate(10);                
            }
            if($user->role_id == '2'){
               
                // $lists = Insurance::where('user_id',$user->id)->with('admin', 'user')->latest()->paginate(10);
                // dd($lists);

                // $find_shop = Admin::where('admin_id',$user->id)->where('role_id',3)->get();
                // $f_shop = [];
                // foreach($find_shop as $fs){
                //     $f_shop[] = $fs->id;
                // }

                $staff = Admin::where('admin_id',$user->id)->whereIn('role_id',[3,5])->get();
                $staff_arr = [];
                foreach($staff as $s){
                    $staff_arr[] = $s->id;
                    // $lists = Insurance::whereIn('user_id',[$s->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
                }
                
                foreach($staff as $s){
                    $shop = Admin::where('admin_id',$s->id)->where('role_id',3)->get();
                    $shop_arr = [];
                    foreach($shop as $shop){
                        $shop_arr[] = $shop->id;
                        // $lists = Insurance::whereIn('user_id',[$s->id,$shop->id, $user->id])->with('admin', 'user')->latest()->paginate(10);
                    }
                }
                $total_array = array_merge($staff_arr, $shop_arr);
                $total_array[] = $user->id;
                

                $lists = Insurance::whereIn('user_id',$total_array)->with('admin', 'user')->where('status',true)->latest()->paginate(10);
                

                
                
                    
                
            }
            if($user->role_id == '3'){
                $lists = Insurance::where('user_id',$user->id)->with('admin', 'user')->where('status',true)->latest()->paginate(10);
            }
            if($user->role_id == '5'){
                $find_shop = Insurance::whereHas('admin', function($q) use($user){
                    $q->where('admin_id', '=', $user->id);
                })->get();
                if(count($find_shop) != '0')
                foreach($find_shop as $fs){
                    $lists = Insurance::whereIn('user_id',[$user->id,$fs->user_id])->with('admin', 'user')->where('status',true)->latest()->paginate(10);
                } else {
                    $lists = Insurance::where('user_id',$user->id)->with('admin', 'user')->where('status',true)->latest()->paginate(10);
                }
            }
        
        // dd($lists);
        $data = compact('lists', 'user');
        return view('backend.inc.insurance.list', $data);
    }

    public function insurance_detail(Request $request, $id){
        
        $list = Insurance::where('id',$id)->with('admin', 'user','plan')->first();

        $tot_price = $list->payment * 1;
        //display the result
        $gst_price = $tot_price * 0.09;
        // $ab = $gst_price

        $price = $tot_price - ($gst_price + $gst_price);

        $inword_price = Insurance::convert_number_to_words($tot_price);

        $setting = Setting::find(1);
        $logo_url = 'public/images/setting/logo/'. $setting->logo;
        if ($request->has('export')) {
            if ($request->get('export') == 'pdf') {
                $pdf = PDF::loadView('backend.inc.insurance.insuranec', compact('list','setting','logo_url','price','gst_price','inword_price'));
                return $pdf->download($list->name.'-invoice.pdf');
            }
        }
        
        $data = compact('list');
        return view('backend.inc.insurance.insurance_detail', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $plan = Plan::where('active', true)->get();
        $planArr  = ['' => 'Select plan'];
        if (!$plan->isEmpty()) {
            foreach ($plan as $b) {
                $planArr[$b->price] = $b->name . ' (' . $b->price . ' ₹)';
            }
        }

        $brand = Brand::get();
        $brandArr  = ['' => 'Select brand'];
        if (!$brand->isEmpty()) {
            foreach ($brand as $b) {
                $brandArr[$b->name] = $b->name;
            }
        }
        $data = compact('planArr','brandArr');
        return view('backend.inc.insurance.add', $data);
        
    }
    public function filter_plan(Request $request)
    {
        $get_price = (int)$request->plan_price;

       
        $plan = Plan::with('rule')->where('minprice','<=',$get_price)->where('maxprice','>=',$get_price)->where('active',true)->get();
       
        $planArr  = ['' => 'Select plan'];
        if (!$plan->isEmpty()) {
            foreach ($plan as $b) {
                $planArr[$b->id] = $b->name . ' (' . $b->price . ' ₹)';
            }
        }
        // dd($planArr);
        $plan_array = view('backend.inc.insurance.plan_array', compact('planArr'))->render();
        $re = [
            'status'    => true,
            'plan_array' => $plan_array
        ];
        return response()->json($re, 200);   
    }

    public function resend_otp(Request $request)
    {
        $record = Insurance::find($request->id);
        
        $otp = rand(100000, 999999);
        $record->otp_code = $otp;
        $record->save();
        $setting = Setting::find(1);       


        $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
        // dd($msg);
        $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$record->mobile,$msg], $setting->sms_api);

        // dd($apiUrl);
        $sms    = file_get_contents($apiUrl);
        $data = compact('record');
        return view('backend.inc.insurance.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        
        
        if ($request->id) {
            
            $record           = Insurance::with('plan')->findOrFail($request->id);
            $rules = [
                'record'        => 'required|array',
                'record.otp_code'   => 'required'
            ];
    
            $request->validate($rules);
            $input            = $request->record;
            // dd($record->otp_code == $input['otp_code']);
            if($record->otp_code == $input['otp_code']){
                if ($input['payment_mode'] == 'online') {
                    $input['payment_status'] = 'paid';

                    $input['status'] = true;
                    $input['is_otp_verified'] = '1';
                    $record->fill($input);
                    // $record->update();

                    if($record->update()){
                        $tot_price = $record->payment * 1;
                    //display the result
                    $gst_price = $tot_price * 0.09;

                    $price = $tot_price - ($gst_price + $gst_price);

                    $inword_price = Insurance::convert_number_to_words($tot_price);
                    
                    $setting = Setting::find(1);

                    $subject = "Your Service ID : ".$record->invoice_pre.''.$record->invoice_no;
                    $email_params = [
                        // 'to'        => env('MAIL_USERNAME'),
                        'to'        => $setting->email,
                        'reciever'  => $setting->title,
                        'from'      => $record->email,
                        'sender'    => $record->name,
                        'subject'   => $subject,
                        'insurance' => $record,
                        'plan'      => $record['plan'],
                        'setting'   => $setting,
                        'price'     => $price,
                        'gst_price' => $gst_price,
                        'inword_price' => $inword_price

                    ];
                    
                    // dd($email_params);
                    Mail::send('email.invoice', $email_params, function ($msgEmail) use ($email_params) {
                        extract($email_params);

                        // $msgEmail->to($to, $reciever)
                        // ->subject($subject)
                        // ->from($from, $sender)
                        // ->replyTo($to, $reciever);
                        $msgEmail->to($from, $sender)
                            ->subject($subject)
                            ->from($to, $reciever)
                            ->replyTo($from, $sender);
                    });
                    }
                } else {
                    $input['txn_id'] = null;
                    $record->delete();

                }
                
    
                return response()->json($record);
            } 
            
        } else {
            $record           = new Insurance;
            $input            = $request->record;
            
            $payment = Plan::find($input['plan_id']);
            
            $input['payment'] = $payment->price;

            if ($request->hasFile('photo')) {
                $file = $request->photo;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/photo/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/photo/' . $name;
                $input['photo'] = $url;
            }
            if ($request->hasFile('aadharcard1')) {
                $file = $request->aadharcard1;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/aadharcardfront/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/aadharcardfront/' . $name;
                $input['aadharcard1'] = $url;
            }
            if ($request->hasFile('aadharcard2')) {
                $file = $request->aadharcard2;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/aadharcardback/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/aadharcardback/' . $name;
                $input['aadharcard2'] = $url;
            }
            if ($request->hasFile('mobile1')) {
                $file = $request->mobile1;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/mobilefirst/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/mobilefirst/' . $name;
                $input['mobile1'] = $url;
            }
            if ($request->hasFile('mobile2')) {
                $file = $request->mobile2;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/mobilesecond/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/mobilesecond/' . $name;
                $input['mobile2'] = $url;
            }
            if ($request->hasFile('invoice')) {
                $file = $request->invoice;
                $optimizeImage = Image::make($file);
                // $optimizeImage->resize(1903, 523);
                $optimizePath = public_path() . '/images/insurance/invoice/';
                $name = time() . $file->getClientOriginalName();
                $optimizeImage->save($optimizePath . $name, 72);
                $url = 'public/images/insurance/invoice/' . $name;
                $input['invoice'] = $url;
            }
            
            $setting = Setting::find(1);
            $otp = rand(100000,999999);
            $input['otp_code'] = $otp;
            $input['is_otp_verified']= '0';
            
            $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
            $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$input['mobile'],$msg], $setting->sms_api);
            $sms    = file_get_contents($apiUrl);

            $input['user_id'] = auth()->user()->id;
            $maxInsurance = Insurance::latest()->first();
            $input['invoice_no']    =  sprintf('%06d', $maxInsurance->invoice_no+1);
            // dd($input);
            $record->fill($input);
            
            $record->save();

            $plan = Plan::where('active', true)->get();
            $planArr  = ['' => 'Select plan'];
            if (!$plan->isEmpty()) {
                foreach ($plan as $b) {
                    $planArr[$b->price] = $b->name . ' (' . $b->price . ' ₹)';
                }
            }

            $data = compact('record', 'planArr');
            return view('backend.inc.insurance.add', $data);
            // return redirect(route('admin.insurance.create', $id))->with('success', 'Success! New record has been added.');
        }
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\planplanplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(PlanCondition $plan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\planplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role, $staff)
    {
        $staff = Admin::find($staff);
        $editData =  ['record' => $staff->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('staff', 'role');
        return view('backend.inc.staff.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role, $staff)
    {
        $staff = PlanCondition::find($staff);
        $record     = $staff;
        $input      = $request->record;
        if ($input['password']) {
            $input['password']    = Hash::make($input['password']);
        }
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.staff.index', $role))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy($staff)
    {
        $staff = Admin::findOrFail($staff);
        $staff->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted.');
    }

    public function status($admin, $status)
    {
        $admin = Admin::findOrFail($admin);
        $admin->active = $status;
        $admin->save();
        // dd($admin);
        return redirect()->back()->with('success', 'Success! Status has been updated.');
    }

    
}
