<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Http\Request;
use App\Model\PlanCondition;
use App\Model\Role;
use App\Model\Setting;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Image;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($role)
    {
        $check = Role::where('name', $role)->count();
        
        if ($check != 0) {

            $query = Admin::whereHas('role', function ($q) use ($role) {
                $q->where('name', $role)->with('admin','role');
            });
            if (auth()->user()->role_id == 2 ) {
                if($role == 'Shop'){
                    $staff = Admin::where('admin_id',auth()->user()->id)->where('role_id',5)->get();
                    // dd($staff);
                    $staff_arr = [];
                    foreach($staff as $s){
                        $staff_arr[] = $s->id;                        
                    }
                    // $staff_arr = 29,35;
                    // dd($staff_arr);                    
                    $staff_arr[] = auth()->user()->id;                   
                    
                    $query->whereIn('admin_id', $staff_arr);
                    
                    
                }
                if($role == 'Staff'){
                    $query->where('admin_id', auth()->user()->id);
                }

            }

            if (auth()->user()->role_id == 5 ) {
                $query->where('admin_id', auth()->user()->id);
            }
            $lists = $query->paginate(10);

            $data = compact('lists', 'role');
            return view('backend.inc.staff.list', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($role)
    {
        $data = compact('role');
        return view('backend.inc.staff.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $role)
    {
        
        $rules = [
            'record'        => 'required|array',
            'record.fname'   => 'required|string',
            'record.mobile'  => 'required|unique:admins,mobile'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];
        

        $roleid = Role::where('name', $role)->first();

        $request->validate($rules, $messages);

        $record           = new Admin;
        $input            = $request->record;
        $input['password']    = Hash::make($input['password']);
        $input['role_id']    = $roleid->id;
        $input['admin_id'] = auth()->user()->id;

        if ($request->hasFile('aadhar_front')) {
            $file = $request->aadhar_front;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/aadhar/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['aadhar_front'] = $name;
        }
        if ($request->hasFile('aadhar_back')) {
            $file = $request->aadhar_back;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/aadhar/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['aadhar_back'] = $name;
        }
        if ($request->hasFile('pan_img')) {
            $file = $request->pan_img;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/pan_card/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['pan_img'] = $name;
        }
        if ($request->hasFile('bank_statement')) {
            $file = $request->bank_statement;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/bank_statement/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['bank_statement'] = $name;
        }
        // $setting = Setting::find(1);
        // $otp = rand(100000,999999);
        // $input['otp_code']=$otp;
		$input['is_otp_verified']= '0';
		
		// $msg = urlencode("Dear ".$setting->title." user, ".$otp." is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE. Regards APR SERVICES");
        // // dd($msg);
        // $apiUrl = str_replace(["[MOBILE]", "[MESSAGE]"], [$input['mobile'],$msg], $setting->sms_api);

        // // dd($apiUrl);
        // $sms    = file_get_contents($apiUrl);
        // $record->fill($input);
        // if ($record->save()) {
            
        //     return redirect(route('admin.staff.verify',[$role, $record->id]))->with('success', 'Success! New record has been added.');
        // } else {
        //     return redirect(route('admin.staff.index', $role))->with('danger', 'Error! Something going wrong.');
        // }
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.staff.index', $role))->with('success', 'Success! New record has been added.');
        }
    }
    public function otp_verify_get($role,$id)
    {
        
        $user = Admin::find($id);

        $data = compact('user','role','id');
        return view('backend.inc.staff.otp_verify', $data);
        
    }
    public function otp_verify_post(Request $request, $role, $id)
    {
        // dd($id);
        // dd($role);
        $rules = [
            'record'        => 'required|array',
            'record.otp_code'   => 'required'
        ];

        $messages = [
            'otp_code'  => 'Please Enter OTP.',
        ];

        $request->validate($rules, $messages);
        $input  = $request->record;
        // dd($input['otp_code']);
        $user = Admin::where('id',$id)->where('otp_code',$input['otp_code'])->get();
        if(count($user) == 1){
            $user = Admin::where('id',$id)->where('otp_code',$input['otp_code'])->update(['is_otp_verified' => '1']);
        } else {
            return redirect()->back()->with('danger', 'Failed! Otp not matched.');
        }


        return redirect(route('admin.staff.index', $role))->with('success', 'Success! New record has been added.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\planplanplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(PlanCondition $plan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\planplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role, $staff)
    {
        $staff = Admin::find($staff);
        $editData =  ['record' => $staff->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('staff', 'role');
        // dd($data);
        return view('backend.inc.staff.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role, $staff)
    {
        $rules = [
            'record'        => 'required|array',
            'record.fname'   => 'required|string',
            'record.mobile'  => 'required|unique:admins,mobile,' . $staff,
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];
        $request->validate($rules, $messages);

        $staff = Admin::find($staff);
        
        $record     = $staff;
        $input      = $request->record;
        if ($input['password']) {
            $input['password']    = Hash::make($input['password']);
        }
        if ($request->hasFile('aadhar_front')) {
            $file = $request->aadhar_front;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/aadhar/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['aadhar_front'] = $name;
        }
        if ($request->hasFile('aadhar_back')) {
            $file = $request->aadhar_back;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/aadhar/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['aadhar_back'] = $name;
        }
        if ($request->hasFile('pan_img')) {
            $file = $request->pan_img;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/pan_card/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['pan_img'] = $name;
        }
        if ($request->hasFile('bank_statement')) {
            $file = $request->bank_statement;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/admin/bank_statement/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['bank_statement'] = $name;
        }
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.staff.index', $role))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy($staff)
    {
        $staff = Admin::findOrFail($staff);
        $staff->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted.');
    }

    public function status($admin, $status)
    {
        $admin = Admin::findOrFail($admin);
        $admin->active = $status;
        $admin->save();
        // dd($admin);
        return redirect()->back()->with('success', 'Success! Status has been updated.');
    }
}
