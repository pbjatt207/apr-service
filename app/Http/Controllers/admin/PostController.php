<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Page;
use App\Model\Post;
use App\Model\Blogcategory;
use App\Model\Tag;
use Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Post::latest()->with('blogcategory')->paginate(10);
        
        if(!$lists->isEmpty()) {
            foreach($lists as $k => $l) {
                $tagArr = [];
                if(!empty($l->tags)) {
                    $tags = Tag::whereIn('id', explode(",", $l->tags))->get();
                    foreach($tags as $t) {
                        $tagArr[] = $t->name;
                    }
                }
                $lists[$k]->tags = implode(", ", $tagArr);
                $categoryArr = [];
                if(!empty($l->category_id)) {
                    $category = Blogcategory::whereIn('id', explode(",", $l->category_id))->get();
                    foreach($category as $t) {
                        $categoryArr[] = $t->name;
                    }
                }
                $lists[$k]->category_id = implode(", ", $categoryArr);
            }
        }

        $data = compact('lists');
        return view('backend.inc.post.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Blogcategory::get();
        $categoryArr  = ['' => 'Select blog category'];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $tag = Tag::get();
        $tagArr  = [];
        if (!$tag->isEmpty()) {
            foreach ($tag as $pcat) {
                $tagArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('categoryArr', 'tagArr'); 
        return view('backend.inc.post.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.title'  => 'required|string',
            'image'         => 'required'
        ];
        
        $messages = [
            'record.title'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Post;
        $input            = $request->record;

        $btag = $request->input('tags');
        $btag = implode(',', $btag);
        $input['tags'] = $btag;

        $bcategory = $request->input('category');
        $bcategory = implode(',', $bcategory);
        $input['category_id'] = $bcategory;
        // dd($input);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/blog/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['title'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {  
            return redirect( route('admin.post.index') )->with('success', 'Success! New record has been added.');
        } else {
            return redirect( route('admin.post.index') )->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Post $post)
    {
        $editData =  ['record'=>$post->toArray()];
        $editData['tags'] = explode(",", $editData['record']['tags']);
        $editData['category'] = explode(",", $editData['record']['category_id']);
        $request->replace($editData);
        $request->flash();

        $category = Blogcategory::get();
        $categoryArr  = [];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $tag = Tag::get();
        $tagArr  = [];
        if (!$tag->isEmpty()) {
            foreach ($tag as $pcat) {
                $tagArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('post','categoryArr','tagArr'); 
        return view('backend.inc.post.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $record     = $post;
        $input      = $request->record;
        
        $btag = $request->input('tags');
        $btag = implode(',', $btag);
        $input['tags'] = $btag;

        $bcategory = $request->input('category');
        $bcategory = implode(',', $bcategory);
        $input['category_id'] = $bcategory;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/blog/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }    
        
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['title'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect( route('admin.post.index') )->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
