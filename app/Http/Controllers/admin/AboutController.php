<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\About;
use Illuminate\Support\Str;
use Image;

class AboutController extends Controller
{
    public function edit(Request $request, About $about)
    {
        $about = About::find(1);
        $editData =  ['record'=>$about->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('about'); 
        return view('backend.inc.about.edit', $data);
    }

    public function update(Request $request, About $about)
    {
        $record     = About::find(1);
        $input      = $request->record;
        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/about/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }    
        
        $record->fill($input);
        if ($record->save()) {
            return redirect( route('admin.about.edit', $about->id) )->with('success', 'Success! Record has been edided');
        }
    }
}
