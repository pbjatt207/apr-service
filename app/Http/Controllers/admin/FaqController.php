<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Faq;
use App\Model\Category;
use Illuminate\Support\Str;
use Image;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Faq::latest()->paginate(10);

        $data = compact('lists');
        return view('backend.inc.faq.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.inc.faq.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.title'   => 'required|string'
        ];

        $messages = [
            'record.title'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Faq;
        $input            = $request->record;

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['title'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.faq.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.faq.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Faq $faq)
    {
        $editData =  ['record' => $faq->toArray()];
        $request->replace($editData);
        $request->flash();


        $data = compact('faq');
        return view('backend.inc.faq.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $record     = $faq;
        $input      = $request->record;

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['title'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.faq.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
