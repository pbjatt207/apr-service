<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting;
use Illuminate\Support\Str;
use Image;

class SettingController extends Controller
{
    public function edit(Request $request, Setting $setting)
    {
        $setting = Setting::find(1);
        $editData =  ['record' => $setting->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('setting');
        return view('backend.inc.setting.edit', $data);
    }


    public function update(Request $request, Setting $setting)
    {
        $record     = Setting::find(1);
        $input      = $request->record;
        if ($request->hasFile('logo')) {
            $file = $request->logo;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/images/setting/logo/';
            $name = 'logo.png';
            $optimizeImage->save($optimizePath . $name, 72);
            $input['logo'] = $name;
        }
        if ($request->hasFile('favicon')) {
            $file = $request->favicon;
            $optimizeImage = Image::make($file);
            $optimizeImage->resize(70, 70);
            $optimizePath = public_path() . '/images/setting/favicon/';
            $name = 'favicon.png';
            $optimizeImage->save($optimizePath . $name, 72);
            $input['favicon'] = $name;
        }

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.setting.edit', $setting->id))->with('success', 'Success! Record has been edided');
        }
    }
}
