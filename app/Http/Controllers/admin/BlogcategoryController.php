<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Blogcategory;
use Illuminate\Support\Str;
use Image;

class BlogcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Blogcategory::latest()->paginate(10);

        $data = compact('lists');
        return view('backend.inc.blogcategory.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent = Blogcategory::get();
        $parentArr  = ['' => 'Select Parent category'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('parentArr');
        return view('backend.inc.blogcategory.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Blogcategory;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/images/blogcategory/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.blogcategory.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.blogcategory.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Blogcategory $blogcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Blogcategory $blogcategory)
    {
        $editData =  ['record' => $blogcategory->toArray()];
        $request->replace($editData);
        $request->flash();

        $parent = Blogcategory::get();
        $parentArr  = ['' => 'Select Parent category'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }

        $data = compact('blogcategory', 'parentArr');
        return view('backend.inc.blogcategory.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blogcategory $blogcategory)
    {
        $record     = $blogcategory;
        $input      = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/images/blogcategory/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.category.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blogcategory $blogcategory)
    {
        $blogcategory->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
