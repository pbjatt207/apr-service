<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Brand;
use Illuminate\Http\Request;
use App\Model\PlanCondition;
use Illuminate\Support\Str;
use Image;

class PlanRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($plan)
    {
        $lists = PlanCondition::latest()->where('plan_id', $plan)->paginate(10);

        $data = compact('lists', 'plan');
        return view('backend.inc.planrule.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($plan)
    {
        $data = compact('plan');
        return view('backend.inc.planrule.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $plan)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new PlanCondition;
        $input            = $request->record;

        $input['plan_id']    = $plan;
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.planrule.index', $plan))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.planrule.index', $plan))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\planplanplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(PlanCondition $plan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\planplan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $plan, $planrule)
    {
        $planrule = PlanCondition::find($planrule);
        $editData =  ['record' => $planrule->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('planrule', 'plan');
        return view('backend.inc.planrule.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $plan, $planrule)
    {
        $planrule = PlanCondition::find($planrule);
        $record     = $planrule;
        $input      = $request->record;

        $input['plan_id']    = $plan;
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.planrule.index', $plan))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlanCondition $plan)
    {
        $plan->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
