<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Brand;
use Illuminate\Http\Request;
use App\Model\SmartPhone;
use Illuminate\Support\Str;
use Image;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = SmartPhone::latest()->paginate(10);

        $data = compact('lists');
        return view('backend.inc.smartphone.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::get();
        $brandArr  = ['' => 'Select brand'];
        if (!$brand->isEmpty()) {
            foreach ($brand as $b) {
                $brandArr[$b->id] = $b->name;
            }
        }

        $data = compact('brandArr');
        return view('backend.inc.smartphone.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new SmartPhone;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/smartphone/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }


        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.smartphone.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.smartphone.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\smartphonesmartphonesmartphone  $smartphone
     * @return \Illuminate\Http\Response
     */
    public function show(SmartPhone $smartphone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\smartphonesmartphone  $smartphone
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SmartPhone $smartphone)
    {
        $editData =  ['record' => $smartphone->toArray()];
        $request->replace($editData);
        $request->flash();

        $brand = Brand::get();
        $brandArr  = ['' => 'Select brand'];
        if (!$brand->isEmpty()) {
            foreach ($brand as $b) {
                $brandArr[$b->id] = $b->name;
            }
        }

        $data = compact('smartphone', 'brandArr');
        return view('backend.inc.smartphone.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\smartphone  $smartphone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmartPhone $smartphone)
    {
        $record     = $smartphone;
        $input      = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/smartphone/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.smartphone.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\smartphone  $smartphone
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmartPhone $smartphone)
    {
        $smartphone->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
