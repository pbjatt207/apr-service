<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\HowItWork;
use Illuminate\Support\Str;
use Image;

class HowitworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = HowItWork::latest()->paginate(10);

        $data = compact('lists');
        return view('backend.inc.howitwork.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.inc.howitwork.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new HowItWork;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/howitwork/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }


        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.howitwork.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.howitwork.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\howitworkhowitworkhowitwork  $howitwork
     * @return \Illuminate\Http\Response
     */
    public function show(HowItWork $howitwork)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\howitworkhowitwork  $howitwork
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, HowItWork $howitwork)
    {
        $editData =  ['record' => $howitwork->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('howitwork');
        return view('backend.inc.howitwork.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\howitwork  $howitwork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HowItWork $howitwork)
    {
        $record     = $howitwork;
        $input      = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            // $optimizeImage->resize(1903, 523);
            $optimizePath = public_path() . '/images/howitwork/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        // $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.howitwork.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\howitwork  $howitwork
     * @return \Illuminate\Http\Response
     */
    public function destroy(HowItWork $howitwork)
    {
        $howitwork->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
