<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Page;
use Illuminate\Support\Str;
use Image;

class PageController extends Controller
{
    public function index()
    {
        $lists = Page::latest()->paginate(10);

        $data = compact('lists');
        return view('backend.inc.page.list', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'  => 'required|string',
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Page;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/images/page/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.page.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.page.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    public function edit(Request $request, $page)
    {
        $page = Page::findOrFail($page);
        $editData =  ['record' => $page->toArray()];
        $request->replace($editData);
        $request->flash();

        $data = compact('page');
        return view('backend.inc.page.edit', $data);
    }

    public function update(Request $request, $page)
    {
        $page = Page::findOrFail($page);
        $record     = $page;
        $input      = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path() . '/images/page/';
            $name = time() . $file->getClientOriginalName();
            $optimizeImage->save($optimizePath . $name, 72);
            $input['image'] = $name;
        }

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.page.index'))->with('success', 'Success! Record has been edided');
        }
    }
}
