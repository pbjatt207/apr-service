<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>APR SERvice</title>
        <style>
            .invoice_detail>table{
                width: 100%;
                border-collapse: collapse;
            }
            .invoice_detail>table, .invoice_detail table tr{
                /* border: 1px solid #000; */
                padding: 5px;
            }
            
            .invoice_detail table tr td {
                border: 1px solid #000;
                padding: 5px;
            }
            .invoice_detail table tr td:first-child{
                border-left: 0;
                padding: 5px;
            }
            .invoice_detail table tr td:last-child{
                border-right: 0;
                padding: 5px;
            }
            td .service_detail tr .service_detail_td {
                border-left: 1px solid #000;
                border-left: 1px solid #000;
                border-bottom: none;
                border-top: none;
            }
            td table {
                margin: 0;
            }
            td .service_detail{
                border-collapse: collapse;
                border: none;
            }
            td .service_detail tr{
                border: none;
            }
            .service_detail tr th{
                border: 1px solid #000;
                border-top: 0;
            }
            .service_detail tr th:first-child{
                border-left: 0;
            }
            .service_detail tr th:last-child{
                border-right: 0;
            }
            .font-w {
                font-weight: bold;
            }
            .text-center{
                text-align: center;
            }
            
        </style>
    </head>
    
    <body>
        <div style="max-width: 800px; margin: auto; border: 1px solid #000;">
            <!-- <div style="float: left;background: #ebebeb; text-align: center; font-size: 48px; padding: 30px 15px;">
                
            </div> -->
            <div style="border-bottom: 1px solid #000;position: relative;">
                <div style="display: flex;padding: 0 5px;">                
                    <div style="font-size: 30px;font-weight: bold; font-family:Verdana, Geneva, Tahoma, sans-serif;width: 80%;color: #f58634;text-transform: uppercase;">{{ $reciever }}</div>
                </div>
                <div style="display: flex;background: #00afef;padding: 5px 5px;">
                    <div style="width: 80%;font-size: 20px;color: #fff;font-family: sans-serif;">Mobile Protection Plan & Mobile Service Center </div>
                </div>
                <div style="display: flex;padding: 5px;font-family: sans-serif;">
                    <div style="width: 40%;">
                        <div style="padding-right: 25px;">
                        {{$setting['address']}}
                        </div>
                    </div>
                    <div style="width: 40%;">
                        <ul style="list-style: none;text-align: right;padding: 0;margin: 0;">
                            <li>
                                Tel : {{$setting['mobile']}}
                            </li>
                            <li>
                                Web : aprservices.in
                            </li>
                            <li>
                                Email : {{$setting['email']}}
                            </li>
                        </ul>
                    </div>
                    <!-- <div style="width: 35px;"></div> -->
                </div>
                <div style="float: right; margin-top: -115px; margin-right: 5px;background: #fff;display: flex;padding: 5px;">
                    <div style="justify-content: center;align-items: center;display: flex;">

                        <img style="vertical-align: middle;" src="{{ url('images/setting/logo/'.$setting->logo) }}" alt="" width="100px" >
                    </div>
                </div>
                
            </div>
            <div style="border-bottom: 1px solid #000;position: relative;font-family: sans-serif;">
                <div style="display: flex;padding: 5px">
                    <div style="width: 33.33%;">
                        <span class="font-w">GSTIN : </span>
                        <span style="text-transform: uppercase;">{{$setting['GST']}}</span>                        
                    </div>
                    <div style="width: 33.33%;text-align: center;">
                        <div style="text-transform: uppercase;font-size: 20px;font-weight: bold;">tax invoice</div>
                    </div>
                    <div style="width: 33.33%;text-align: right;font-weight: bold;">
                        <div>
                            Original for Recipient
                        </div>
                    </div>
                </div>
                <div class="invoice_detail" style="font-size: 13px;">
                    
                    <table >
                        <tr >
                            <td class="invoice_detail_td" style="width: 40%;">
                                <div style="text-align: center;font-weight: bold;">
                                    Customer Detail
                                </div>
                            </td>
                            <td class="invoice_detail_td"  style="width: 60%; vertical-align: baseline;padding: 0;" rowspan="2" >
                                <div>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="border:none;width:25%">Invoice No</td>
                                            <td style="border:none;width:25%;text-transform: uppercase;font-weight: bold;">{{ $insurance['invoice_pre'] }}{{ $insurance['invoice_no'] }}</td>
                                            <td style="border:none;width:25%">Invoice Date</td>
                                            <td style="border:none;width:25%">{{ date("d-m-Y", strtotime($insurance['created_at'])) }}</td>
                                        </tr>
                                        
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="invoice_detail_td" >
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 20%;font-weight: bold;">Name :</div>
                                    <div style="width: 80%;">{{ $insurance['name'] }}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 20%;font-weight: bold;">Address :</div>
                                    <div style="width: 80%;">{{ $insurance['address'] }}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 20%;font-weight: bold;">Phone :  </div>
                                    <div style="width: 80%;">{{ $insurance['mobile'] }}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 20%;font-weight: bold;">Brand</div>
                                    <div style="width: 80%;">{{ $insurance['plan_brand'] }}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 20%;font-weight: bold;">Mobile IMEI</div>
                                    <div style="width: 80%;">{{ $insurance['imei_number1'] }}<br>{{ $insurance['imei_number2'] }}</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="invoice_detail_td" colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="invoice_detail_td" colspan="2" style="padding: 0;border: none;">
                                <table style="width: 100%;border-left-color: red;" class="service_detail" >
                                    <tr>
                                        <th rowspan="2">Sr. No.</th>
                                        <th rowspan="2">Name of Service</th>
                                        <th rowspan="2">HSN/SAC</th>
                                        <th rowspan="2">Rate</th>
                                        <th rowspan="2">Taxable Value</th>
                                        <th colspan="2">SGST</th>
                                        <th colspan="2">CGST</th>
                                        <th rowspan="2">Total</th>
                                    </tr>
                                    <tr>
                                        
                                        <th>%</th>
                                        <th>Amount</th>
                                        <th>%</th>
                                        <th>Amount</th>
                                    </tr>
                                    <tr>
                                        <td class="service_detail_td">1.</td>
                                        <td class="service_detail_td">{{ $insurance->plan['name'] }}</td>
                                        <td class="service_detail_td">995411</td>
                                        <td class="service_detail_td">{{$price}}</td>
                                        <td class="service_detail_td">{{$price}}</td>
                                        <td class="service_detail_td">9</td>
                                        <td class="service_detail_td">{{$gst_price}}</td>
                                        <td class="service_detail_td">{{ $insurance['payment'] }}</td>
                                    </tr>
                                    <tr style="height: 130px;">
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                        <td class="service_detail_td"></td>
                                    </tr>
                                    <tr class="font-w">                                        
                                        <td  style="border-bottom: 0;" colspan="3">Total</td>
                                        <td  style="border-bottom: 0;" class=""></td>
                                        <td  style="border-bottom: 0;" class="">{{$price}}</td>
                                        <td  style="border-bottom: 0;" class=""></td>
                                        <td  style="border-bottom: 0;" class="">{{$gst_price}}</td>
                                        <td  style="border-bottom: 0;" class="">{{ $insurance['payment'] }}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="font-w text-center" style="">Total in words</td>
                            <td class="font-w" style="">
                                <div style="float: left;">Taxable Amount</div>
                                <div style="float: right;">{{$price}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3"  >
                                <div style="text-transform: uppercase;" id='inWord'>{{ $inword_price }} RUPEES</div>   
                            </td>
                            <td class="font-w" style="">
                                <div style="float: left;">@SGST/9</div>
                                <div style="float: right;">{{ $gst_price }}</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w" style="">
                                <div style="float: left;">@CGST/9</div>
                                <div style="float: right;">{{$gst_price}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w" style="">
                                <div style="float: left;">Total Tax</div>
                                <div style="float: right;">{{$gst_price + $gst_price}}</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w text-center">
                                Bank Detail
                            </td>
                            <td class="font-w" style="">
                                <div style="float: left;">Total Amount After Tax</div>
                                <div style="float: right;">Rs {{ $insurance['payment'] }}</div>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2">
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 40%;font-weight: bold;">Bank Name</div>
                                    <div style="width: 60%;font-weight: bold;">{{$setting['bank_name']}}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 40%;font-weight: bold;">Branch Name</div>
                                    <div style="width: 60%;font-weight: bold;text-transform: uppercase;">{{$setting['branch_name']}}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 40%;font-weight: bold;">A/C</div>
                                    <div style="width: 60%;font-weight: bold;">{{$setting['bank_ac']}}</div>
                                </div>
                                <div style="display:flex;margin-bottom: 5px;">
                                    <div style="width: 40%;font-weight: bold;">Bank Branch IFSC</div>
                                    <div style="width: 60%;font-weight: bold;text-transform: uppercase;">{{$setting['bank_ifsc']}}</div>
                                </div>
                            </td>
                            <td class="" style="">
                                
                                <div style="float: right;">(E & O.E.)</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w" style="">
                                <div style="float: left;">GST Payable on Reverse Charge</div>
                                <div style="float: right;">N.A.</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w text-center">Terms & Conditions</td>
                            <td class="text-center">
                                <div style="font-size: 10px;margin-bottom: 5px;">Certified that the particulars given above are true and correct</div>
                                <div style="font-size: 17px;" class="font-w">For APR Services</div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 0;" rowspan="2">
                                <div>{!! $setting['invoice_terms'] !!}</div>
                            </td>
                            <td style="min-height: 35px;"></td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 0;text-align: center;height: 20px;" class="font-w">
                                Authorised Signature
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        
    </body>
</html>
