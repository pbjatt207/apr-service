<div class="overly-main"></div>
<div class="sidebar mobileview">
  <!-- sidebar -->
  <div class="l-navbar sho" id="nav-bar">
    <nav class="navor mynave-listt">
      <div class="accordion">
        <ul>
          <li>
            <div class="main-deshbord-heding"> <span class="bx bxs-dashboard nav-icon" data-bs-toggle="tooltip" data-bs-placement="right"></span>
              <a href="{{ route('admin.home') }}" class="nav-text">Dashboard</a>
            </div>
          </li>
          @if(auth()->user()->role_id == 1)
          <li class="has-dropdown accordion-box ">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right"></span>
              <span class="nav-text">Master</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Slider</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.slider.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.slider.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Brand</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.brand.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.brand.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Faqs</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.faq.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.faq.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">How It Work</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.howitwork.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.howitwork.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Service</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.service.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.service.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Testimonial</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.testimonial.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.testimonial.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
                <li class="has-dropdown accordion-box">
                  <div class="drop-submenu-r"> <span class="nav-text" style="margin-left: 25px;">Smart Phone</span>
                    <a href=""></a>
                  </div>
                  <div class="accordion-content">
                    <ul style="margin-left: 25px;">
                      <li> <a href="{{ route('admin.smartphone.create') }}">Add</a></li>
                      <li> <a href="{{ route('admin.smartphone.index') }}">View</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
          <li class="has-dropdown accordion-box">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
              </span>
              <span class="nav-text">Plan</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li> <a href="{{ route('admin.plan.create') }}">Add</a>
                <li> <a href="{{ route('admin.plan.index') }}">View</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="has-dropdown accordion-box">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
              </span>
              <span class="nav-text">Franchise</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li> <a href="{{ route('admin.staff.create','Franchise') }}">Add</a>
                <li> <a href="{{ route('admin.staff.index','Franchise') }}">View</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="accordion-box">
            <a href="{{ route('admin.staff.index','Shop') }}">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">Shop</span>
              </div>
            </a>
          </li>
          @endif
          @if(auth()->user()->role_id == 2 )
          <li class="has-dropdown accordion-box">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
              </span>
              <span class="nav-text">Staff</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li> <a href="{{ route('admin.staff.create','Staff') }}">Add</a>
                <li> <a href="{{ route('admin.staff.index','Staff') }}">View</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="has-dropdown accordion-box">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
              </span>
              <span class="nav-text">Shop</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li> <a href="{{ route('admin.staff.create','Shop') }}">Add</a>
                <li> <a href="{{ route('admin.staff.index','Shop') }}">View</a>
                </li>
              </ul>
            </div>
          </li>
          @endif
          @if(auth()->user()->role_id == 5)
          <li class="has-dropdown accordion-box">
            <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
              </span>
              <span class="nav-text">Shop</span>
              <a href=""></a>
            </div>
            <div class="accordion-content">
              <ul>
                <li> <a href="{{ route('admin.staff.create','Shop') }}">Add</a>
                <li> <a href="{{ route('admin.staff.index','Shop') }}">View</a>
                </li>
              </ul>
            </div>
          </li>
          @endif
          @if(auth()->user()->role_id == 1)
          <li class="accordion-box">
            <a href="{{ route('admin.page.index') }}">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">Pages</span>
              </div>
            </a>
          </li>
          @endif
          @if(auth()->user()->role_id == 1)
          <li class="accordion-box">
            <a href="{{ route('admin.insurance.index') }}">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">Mobile Repair Services</span>
              </div>
            </a>
          </li>
          @else
            <li class="has-dropdown accordion-box">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text">Mobile Repair Service</span>
                <a href=""></a>
              </div>
              <div class="accordion-content">
                <ul>
                  <li> <a href="{{ route('admin.insurance.create') }}">Add</a>
                  <li> <a href="{{ route('admin.insurance.index') }}">View</a>
                  </li>
                </ul>
              </div>
            </li>
          @endif
          @if(auth()->user()->role_id == 1)
          <li class="accordion-box">
            <a href="{{ route('admin.setting.edit') }}">
              <div class="drop-submenu-r"> <span class="bx bx-cog nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">Setting</span>
              </div>
            </a>
          </li>
          @endif
          <li class="accordion-box">
            <a href="{{ route('admin.user.edit') }}">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">Change Password</span>
              </div>
            </a>
          </li>
          <li class="accordion-box">
            <a href="{{ route('admin.logout') }}">
              <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" style="width: 20px;" data-bs-toggle="tooltip" data-bs-placement="right">
                </span>
                <span class="nav-text" style="font-size: 16px;">LogOut</span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
  <!-- sidbar -->
</div>