<header class="header " id="header">
	<div class="heder-dark">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-2 col-5">
					<div class="title-logo">APR<span> Service</span>
					</div>
				</div>
				<div class="col-lg-2 col-2">
					<div class="sidebar-btn">
						<div class=" mob-s-y change" id="button-enlarge"></i>
							<div class="bars"></div>
							<div class="bars"></div>
							<div class="bars"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-5">
					<div class="navbar-icon-heder">
						<ul>
							<li class="nav-item dropdown hover-colo ">

								<a class="dropdown-toggle nav-my-co " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <i class='bx bxs-user' data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile"></i>
								</a>

								<ul class="dropdown-menu drop-hover-i" aria-labelledby="navbarDropdown">

									@if(auth()->user()->role_id == 1)
									<li><a class="dropdown-item" href="{{ route('admin.setting.edit') }}">Settings</a>
									</li>
									<li>
										<hr class="dropdown-divider">
									</li>
									@endif
									<li><a class="dropdown-item" href="{{ route('admin.user.edit') }}">Change Password</a>
									</li>
									<li>
										<hr class="dropdown-divider">
									</li>
									<li><a class="dropdown-item" href="{{ route('admin.logout') }}">Logout</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<!-- <div class="navbar-icon-heder2">
						<ul>
							<li class="nav-item dropdown hover-colo">
								<a class=" nav-my-co" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <i class='bx bxs-bell' data-bs-toggle="tooltip" data-bs-placement="bottom" title="notification"></i>
								</a>
								<ul class="dropdown-menu  drop-hover-i" aria-labelledby="navbarDropdown">
									<li><a class="dropdown-item" href="#">Action</a>
									</li>
									<li><a class="dropdown-item" href="#">Another action</a>
									</li>
									<li>
										<hr class="dropdown-divider">
									</li>
									<li><a class="dropdown-item" href="#">Something else here</a>
									</li>
								</ul>
							</li>
						</ul>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</header>