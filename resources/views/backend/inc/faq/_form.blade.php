@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('title', 'Enter title'), ['class' => 'active']}}
      {{Form::text('record[title]', '', ['class' => 'form-control', 'placeholder'=>'Enter title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('slug', 'Enter slug'), ['class' => 'active'] }}
      {{ Form::text('record[slug]','', ['class'=>'form-control', 'placeholder'=>'Enter slug']) }}
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group">
      {{ Form::label('description', 'Enter description'), ['class' => 'active'] }}
      {{ Form::textarea('record[description]','', ['class'=>'form-control editor', 'placeholder'=>'Enter description', 'rows' => '5', 'cols' => '10']) }}
    </div>
  </div>
</div>