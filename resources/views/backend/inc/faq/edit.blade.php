@extends('backend.layout.master')
@section('title','Faq Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit Faq</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.faq.index') }}">Faq</a></li>
                <li class="breadcrumb-item active">Edit Faq</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.faq.update',$faq->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.faq._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_faq" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection