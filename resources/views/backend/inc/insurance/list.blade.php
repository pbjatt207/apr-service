@extends('backend.layout.master')
@section('title', 'Mobile Repair')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View Mobile Repair</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Mobile Repair</li>
                    </ol>
                </div>
                @if(auth()->user()->role_id != 1)
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{ route('admin.insurance.create') }}" class="btn btn-primary" type="button"><i class='bx bx-plus'>Add</i></a>
                    </div>
                </div>
                @endif
                
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <!-- <div class="search-top-table">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                </div>
                                <h4>View staff</h4>
                            </div>
                            <div class="col-lg-6">
                                <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="table-responsiveima pt-4">
                        <input class="form-control mb-3" style="width:25%" id="myInput" type="text" placeholder="Search..">
                        <table>
                            <thead class="the-colo-table">
                                <tr>
                                    <!-- <th><input type="checkbox" name="sr.no">Sr.No</th> -->
                                    <th>S. No.</th>
                                    @if($user->role_id == '3')
                                    <th>Shop</th>
                                    @endif
                                    @if($user->role_id == '2')
                                    <th>Franchise/Shop</th>
                                    @endif
                                    @if($user->role_id == '5')
                                    <th>Shop</th>
                                    @endif
                                    @if($user->role_id == '1')
                                    <th>User</th>
                                    @endif
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Brand</th>
                                    <th>Brand Price</th>
                                    <!-- <th>Imei Number</th> -->
                                    <!-- <th>Aadhar Card</th> -->
                                    <th>Txn_id</th>
                                    <th>Payment Mode</th>
                                    <th>Price</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                    <!-- <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $sn = 1;
                                @endphp
                                @foreach($lists as $list)
                                <tr>
                                    <td>{{ $sn++ }}</td>
                                    <td>@if($list->customer_id) {{ $list->customer_id ? $list->user->fname.' '.$list->user->lname.'(Customer)' : '' }} @endif @if($list->user_id) {{ $list->user_id ? $list->admin->fname.' '.$list->admin->lname . ' ('.$list->admin->role->name.')' : ''}} @endif</td>
                                    <td>
                                        {{ $list->name }} <br>
                                        <small>({{ $list->father }})</small>
                                    </td>
                                    <td>{{ $list->email }} </td>
                                    <td>{{ $list->mobile }} </td>
                                    <td>{{ $list->plan_brand }} </td>
                                    <td>₹{{ $list->plan_price }} </td>
                                    <!-- <td>
                                        1. {{ $list->imei_number1 ? $list->imei_number1 : '-' }} <br>
                                        2. {{ $list->imei_number2 ? $list->imei_number2 : '-' }} 
                                    </td>
                                    <td>{{ $list->aadharcard  ? $list->aadharcard : 'N/A' }}</td> -->
                                    <td>{{ $list->txn_id }}</td>
                                    @if($list->payment_status == 'due')
                                    <td class="text-danger">{{ $list->payment_mode }}</td>
                                    @else
                                    <td class="text-success">{{ $list->payment_mode }}</td>
                                    @endif
                                    <td>₹{{ $list->payment }} </td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>
                                        <a class="btn btn-secondary" href="{{ url('admin-access/insurance/'.$list->id) }}">View Detail</a>
                                    </td>
                                    <!-- @php
                                    $statusArr = [
                                    'true' => 'Active',
                                    'false' => 'Inactive',
                                    ]
                                    @endphp
                                    <td>
                                        {{ Form::select('verified', $statusArr,$list->active, ['class'=>'form-control','id'=>url('/admin-access/staff/status/'.$list->id), 'onchange'=>'myFunction(this)']) }}
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <div class="p-3 text-right">{{ $lists->links() }}</div>
                </div>
            </section>
        </div>
    </main>
</div>
<script>
    function myFunction(abc) {
        window.location = abc.id + '/' + abc.value;
    }
</script>
@endsection