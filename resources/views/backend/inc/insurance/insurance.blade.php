<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Service Plan</title>
    </head>
    <body>
        <div style="max-width: 800px; margin: auto; border: 1px solid #ccc;">
            <div style="background: #ebebeb; text-align: center; font-size: 48px; padding: 30px 15px;">
                <img style="float: left;" src="{{url('images/setting/logo/'.$setting->logo)}}" alt="" width="100px">
                <div style="float: left; padding-left: 20px;">{{ $setting->title }}</div> 
                <div style="float: right; font-size: 15px;text-align: right;">
                    <img src="{{ url('images/email.png') }}" alt="" style="width:20px;margin-right: 5px;margin-top: 8px;"> <a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a><br>
                    <img src="{{ url('images/telephone.png') }}" alt="" style="width:20px;margin-right: 5px;margin-top: 8px;"> <a href="tel:{{ $setting->mobile }}">{{ $setting->mobile }}</a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="padding: 15px">
                Dear {{ucwords($list->name)}},<br>
                
                <h3>Your Service ID : {{ $list->invoice_pre }}{{ $list->invoice_no }}</h3>
                <p>Find details as below:</p>
                <table style="border-collapse: collapse; width: 100%;">
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px" colspan="2">
                            <span style="font-weight: bold;">Service ID : </span> {{ $list->invoice_pre }}{{ $list->invoice_no }}
                        </td>
                    </tr>
                    <tr>
                        <!-- <th style="border: 1px solid #ccc; text-align: left;">Txn_id</th>
                        <td style="border: 1px solid #ccc;">#8789786</td> -->
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Txn_id : </span> #{{ $list->txn_id }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Date : </span> {{ $list->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Name : </span> {{ ucwords($list->name) }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Email : </span> {{ $list->email }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Mobile No. : </span> {{ $list->mobile }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Aadhar No. : </span> {{ $list->aadharcard }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Plan Brand : </span> {{ ucwords($list->plan_brand) }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold; ">Plan Price : </span><span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $list->plan_price }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">IMEI number : </span> {{ $list->imei_number1 }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">IMEI number : </span> {{ $list->imei_number2 }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Payment : </span><span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $list->payment }}
                        </td>
                        <td style="border: 1px solid #ccc; padding:5px">
                            <span style="font-weight: bold;">Payment Mode : </span> {{ ucwords($list->payment_mode) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; padding:5px" colspan="2">
                            <span style="font-weight: bold;">Payment Status : </span> {{ ucwords($list->payment_status) }}
                        </td>
                    </tr>
                    
                </table>
                <p>&nbsp;</p>
                <p>
                    Thanks &amp; Regards<br>
                    {{ $setting->title }}
                </p>
            </div>
            <div style="background: #ebebeb; text-align: center; padding: 10px 0; font-size:17px">
                <a href="{{ url('/page/term-condition') }}">Terms & conditions</a>
            </div>
        </div>
    </body>
</html>
