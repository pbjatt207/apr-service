@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('name', 'Enter name'), ['class' => 'active']}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile number'), ['class' => 'active']}}
      {{Form::text('record[mobile]', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile number'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('email', 'Enter email'), ['class' => 'active']}}
      {{Form::text('record[email]', '', ['class' => 'form-control', 'placeholder'=>'Enter email'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('address', 'Enter address'), ['class' => 'active'] }}
      {{ Form::textarea('record[address]','', ['class'=>'form-control', 'placeholder'=>'Enter address', 'rows' => '4', 'cols' => '10']) }}
      
    </div>
  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group">
          {{Form::label('aadharcard', 'Enter aadhar card'), ['class' => 'active']}}
          {{Form::text('record[aadharcard]', '', ['class' => 'form-control', 'placeholder'=>'Enter aadhar card'])}}
        </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
          {{Form::label('image', 'Choose mobile box image'), ['class' => 'active']}}
          {{Form::file('image',['class'=>'form-control'])}}
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('plan_id', 'Select plan'), ['class' => 'active'] }}
      {{ Form::select('record[plan_id]', $planArr,'0', ['class'=>'form-control']) }}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('image2', 'Choose  mobile box sticker image'), ['class' => 'active']}}
      {{Form::file('image2',['class'=>'form-control'])}}
    </div>
  </div>
  <div class="col-lg-6 mt-3">
    <div class="row">
      <div class="col-lg-3">
        <div class="form-group">
          Payment Mode :
        </div>
      </div>
      <div class="col-lg-4">
        <div class="form-group">
          {{ Form::radio('record[payment_mode]', 'cash' , true,['class' => 'paymentMethod']) }}
          {{Form::label('cash', 'Cash'), ['class' => 'active']}}
          {{ Form::radio('record[payment_mode]', 'online' , false,['class' => 'paymentMethod']) }}
          {{Form::label('online', 'RazorPay'), ['class' => 'active']}}
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row mt-5">
  <div class="col-lg-3">
    <div class="row">
      <div class="col-lg-6">
        Net Price :
      </div>
      <div class="col-lg-6">
        200 ₹
      </div>
    </div>
  </div>
  <div class="col-lg-9">
  </div>
  <div class="col-lg-3">
    <div class="row">
      <div class="col-lg-6">
        Other Charges :
      </div>
      <div class="col-lg-6">
        0 ₹
      </div>
    </div>
  </div>
  <div class="col-lg-9">
  </div>
</div>
<div class="row">
  <div class="col-lg-3">
    <hr class="" style="width: 100%;">
    <div class="row">
      <div class="col-lg-6">
        Total Amount :
      </div>
      @php
      $totalAmt = '200';
      $user = auth()->user();
      @endphp
      <div class="col-lg-6">
        200 ₹
      </div>
      {{Form::text('record[payment]', $totalAmt, ['class' => 'form-control', 'hidden'])}}
      
    </div>
  </div>
  <div class="col-lg-9">
  </div>
</div>
<div class="row mt-5">
  <div class="col-lg-12">
    <div class="form-group">
      <!-- {{Form::checkbox('record[termandcondition]', '', ['class' => 'form-control', 'placeholder'=>'Enter aadhar card'])}}
      {{Form::label('termandcondition', 'agree term & condition'), ['class' => 'active']}} -->
    </div>
  </div>
</div>