@extends('backend.layout.master')
@section('title', 'Mobile Repair')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View Mobile Repair</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.insurance.index') }}">Mobile Repair</a></li>
                        <li class="breadcrumb-item active"> {{ $list->name }} </li>
                    </ol>
                </div>
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a class="btn btn-primary">{{ $list->created_at->format('d M Y') }}</a>
                        <a href="{{ url('admin-access/insurance/'.$list->id.'?export=pdf') }}" class="btn btn-danger">Export PDF</a>
                    </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <table>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Franchise :</span>
                                    <span>@if($list->customer_id) {{ $list->customer_id ? $list->user->fname.' '.$list->user->lname.'(Customer)' : '' }} @endif @if($list->user_id) {{ $list->user_id ? $list->admin->fname.' '.$list->admin->lname . ' ('.$list->admin->role->name.')' : ''}} @endif</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Name :</span>
                                    <span >{{ $list->name }}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Father's Name :</span>
                                    <span>{{ $list->father }}</span>
                                </div>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Email :</span>
                                    <span>{{$list->email}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Mobile :</span>
                                    <span>{{ $list->mobile }}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Address :</span>
                                    <span>{{$list->address}}</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Brand :</span>
                                    <span>{{$list->plan_brand}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Brand Price :</span>
                                    <span>₹{{ $list->plan_price }}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Brand Date :</span>
                                    <span>{{ $list->plan_date }}</span>
                                </div>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Plan Name :</span>
                                    <span>{{$list->plan ? $list->plan->name : 'N/A'}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Plan Price :</span>
                                    <span>₹{{ $list->payment }}</span>
                                </div>
                            </td>
                            <td>
                                
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Txn id :</span>
                                    <span>{{$list->txn_id ? $list->txn_id : 'N/A'}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Payment Mode :</span>
                                    <span>{{$list->payment_mode ? $list->payment_mode : 'N/A'}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Payment Status :</span>
                                    <span>{{$list->payment_status ? $list->payment_status : 'N/A'}}</span>
                                </div>
                            </td>
                            
                            
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Aadhar No :</span>
                                    <span>{{$list->aadharcard ? $list->aadharcard : 'N/A'}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Imei No. (i) :</span>
                                    <span>{{$list->imei_number1 ? $list->imei_number1 :' N/A'}}</span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Imei No (ii) :</span>
                                    <span>{{$list->imei_number2 ? $list->imei_number2 : 'N/A'}}</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Photo :</span>
                                    <span>
                                        @if($list->photo != '')
                                            <a href="{{ url($list->photo) }}" target="_blank">
                                                <img src="{{ url($list->photo) }}" alt="" width="150px">
                                            </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Aadhar card (i) :</span>
                                    <span>
                                        @if($list->aadharcard1 != '')
                                        <a href="{{ url($list->aadharcard1) }}" target="_blank">
                                            <img src="{{ url($list->aadharcard1) }}" alt="" width="150px">
                                        </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Aadhar card (ii) :</span>
                                    <span>
                                        @if($list->aadharcard2 != '')
                                        <a href="{{ url($list->aadharcard2) }}" target="_blank">
                                            <img src="{{ url($list->aadharcard2) }}" alt="" width="150px">
                                        </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <span class="service_history_title">Mobile :</span>
                                    <span>
                                        @if($list->mobile1 != '')
                                            <a href="{{ url($list->mobile1) }}" target="_blank">
                                                <img src="{{ url($list->mobile1) }}" alt="" width="150px">
                                            </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Mobile :</span>
                                    <span>
                                        @if($list->mobile2 != '')
                                        <a href="{{ url($list->mobile2) }}" target="_blank">
                                            <img src="{{ url($list->mobile2) }}" alt="" width="150px">
                                        </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span class="service_history_title">Invoice :</span>
                                    <span>
                                        @if($list->invoice != '')
                                        <a href="{{ url($list->invoice) }}" target="_blank">
                                            <img src="{{ url($list->invoice) }}" alt="" width="150px">
                                        </a>
                                            @else
                                            N/A
                                            @endif
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </section>
        </div>
    </main>
</div>
<script>
    function myFunction(abc) {
        window.location = abc.id + '/' + abc.value;
    }
</script>
@endsection