@extends('backend.layout.master')
@section('title', 'Mobile Repair')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Mobile Repair</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.insurance.index') }}">Mobile Repair</a></li>
                <li class="breadcrumb-item active">Add Mobile Repair</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    @if(!@$record->id)
                    <div id="iamgeUpload">
                        {{ Form::open(['url'=>route('admin.insurance.create') ,'id'=> 'uploadImage', 'files' => true, 'class' => 'user']) }}

                        <!-- {{ Form::open(['url' => route('admin.insurance.store'), 'method'=>'POST','id'=> 'checkoutForm', 'files' => true, 'class' => 'user']) }} -->
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block mt-3" id="successMessage">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($message = Session::get('danger'))
                        <div class="alert alert-danger alert-block mt-3" id="successMessage">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if(count($errors->all()))
                        <div class="alert alert-danger mt-3" id="successMessage">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li style="list-style: none;">{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('name', 'Enter name'), ['class' => 'active']}}
                                    {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('father', 'Enter father name'), ['class' => 'active']}}
                                    {{Form::text('record[father]', '', ['class' => 'form-control', 'placeholder'=>'Enter father name','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('mobile', 'Enter mobile number'), ['class' => 'active']}}
                                    {{Form::tel('record[mobile]', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile number','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{Form::label('email', 'Enter email'), ['class' => 'active']}}
                                    {{Form::email('record[email]', '', ['class' => 'form-control', 'placeholder'=>'Enter email','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{Form::label('aadharcard', 'Enter aadhar card'), ['class' => 'active']}}
                                    {{Form::number('record[aadharcard]', '', ['class' => 'form-control', 'placeholder'=>'Enter aadhar card','required'=>'required'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('address', 'Enter address'), ['class' => 'active'] }}
                                    {{ Form::textarea('record[address]','', ['class'=>'form-control', 'placeholder'=>'Enter address', 'rows' => '4', 'cols' => '10','required'=>'required']) }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('plan_brand', 'Select Brand'), ['class' => 'active'] }}
                                    {{ Form::select('record[plan_brand]', $brandArr,'0', ['class'=>'form-control']) }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {{Form::label('plan_price', 'Enter Brand Price'), ['class' => 'active']}}
                                    <!-- {{Form::text('record[plan_price]', '', ['class' => 'form-control', 'placeholder'=>'Enter brand price','required'=>'required'])}} -->
                                    <input type="text" name='record[plan_price]' class="form-control" placeholder="Enter brand price" id="plan_price" data-url="{{route('admin.insurance.filter_plan')}}" required>
                                    
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div id="plan_array"></div>
                            </div>

                            <!-- <div class="col-lg-6">
                                <div class="form-group">
                                    {{ Form::label('payment', 'Select Plan'), ['class' => 'active'] }}
                                    {{ Form::select('record[payment]', $planArr,'0', ['class'=>'form-control']) }}
                                </div>
                            </div> -->

                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('imei_number1', 'Enter IMEI number (first)'), ['class' => 'active']}}
                                    {{Form::text('record[imei_number1]', '', ['class' => 'form-control', 'placeholder'=>'Enter IMEI number','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('imei_number2', 'Enter IMEI number (second)'), ['class' => 'active']}}
                                    {{Form::text('record[imei_number2]', '', ['class' => 'form-control', 'placeholder'=>'Enter IMEI number'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('photo', 'Choose photo'), ['class' => 'active']}}
                                    {{Form::file('photo',['class'=>'form-control', 'id' => 'userPhoto','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('aadharcard1', 'Choose aadhar card (front)'), ['class' => 'active']}}
                                    {{Form::file('aadharcard1',['class'=>'form-control','id' => 'aadharphoto1','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('aadharcard2', 'Choose aadhar card (back)'), ['class' => 'active']}}
                                    {{Form::file('aadharcard2',['class'=>'form-control','id' => 'aadharphoto2','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('mobile1', 'Choose mobile image (first)'), ['class' => 'active']}}
                                    {{Form::file('mobile1',['class'=>'form-control','id' => 'mobile1','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('mobile2', 'Choose mobile image (second)'), ['class' => 'active']}}
                                    {{Form::file('mobile2',['class'=>'form-control','id' => 'mobile2','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    {{Form::label('invoice', 'Choose invoice image'), ['class' => 'active']}}
                                    {{Form::file('invoice',['class'=>'form-control','id' => 'invoice','required'=>'required'])}}
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <input type="submit" class="btn btn-primary" name="save" value="Next" />
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>

                    @endif
                    
                    @if(@$record->id)
                   
                    <div>
                        {{ Form::open(['id'=> 'checkoutForm', 'files' => true, 'class' => 'user']) }}
                        <h5>Payment Confirmation</h5>
                        <div class="col-lg-4">
                        <input type="hidden" value="{{$record->payment}}" id="pay_amount">
                            <div class="form-group">
                                {{Form::label('otp_code', 'Enter OTP code'), ['class' => 'active']}}
                                {{Form::text('record[otp_code]', '', ['class' => 'form-control', 'placeholder'=>'Enter Otp'])}}
                            </div>
                            <a href="{{ url('admin-access/insurance/resend_otp?id='.$record->id) }}">Resend Otp?</a>
                        </div>
                        <div class="mt-3">
                            Net Payment : {{ number_format($record->payment) }} ₹
                        </div>
                        <!-- <div class="mt-3">
                            
                            Payment Mode :
                            {{ Form::radio('record[payment_mode]', 'cash' , false,['class' => 'paymentMethod']) }}
                            {{Form::label('cash', 'Cash'), ['class' => 'active']}}
                            {{ Form::radio('record[payment_mode]', 'online' , true,['class' => 'paymentMethod']) }}
                            {{Form::label('online', 'RazorPay'), ['class' => 'active']}}

                        </div> -->
                        <input type="hidden" name="record[payment_mode]" value="online">
                        <div class="row mt-5">
                            <div class="col-lg-12">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                        {{Form::text('record[txn_id]', '', ['id' => 'paymentID', 'hidden'])}}
                        {{Form::text('id', $record->id, ['hidden'])}}
                        <div class="text-right mt-3">
                            <input type="submit" class="btn btn-primary" name="save" value="Processed to pay" />
                        </div>
                        {{ Form::close() }}
                    </div>
                    @endif
                   
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#rzp-footer-form').submit(function(e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function(r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })

    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#checkoutForm');
        // alert()

        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        if (transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        $.ajax({
            method: 'post',
            url: "{!! route('admin.insurance.store') !!}",
            data: form.serialize(),
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(r) {
                window.location = "{{ route('admin.insurance.index') }}";
            }
        })
    }
    var payamount = $('#pay_amount').val();
    
    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: payamount * 100,
        name: 'APR Services',
        description: '',
        image: '{{ url("imgs/logo.png") }}',
        prefill: {
            "email": "{{ Auth::user()->email }}",
            "contact": "{{ Auth::user()->mobile }}"
        },
        // amount: '200',
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);
    $(document).on('submit', '#checkoutForm', function(e) {
        e.preventDefault();
        if (is_valid) {
            // let payMethod = $('.paymentMethod:checked').val();
            // alert(payMethod);
            // if (payMethod == "online") {
                r.open();
            // } else {
            //     demoSuccessHandler(false);
            // }
        }
    });
</script>
<script>
$(document).ready(function(){
  $("#plan_price").keydown(function(){
    // $("input").css("background-color", "yellow");
  });
  $("#plan_price").keyup(function(){
   
    // e.preventDefault();
    
    var url =  $(this).data('url');    
    var plan_price = $('#plan_price').val();
    // alert(name);
    
   $.ajax({
          url:url,
          type:"GET",
          headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:{
            plan_price:plan_price,            
          },
          success:function(response){
              
              if(response.status){
                   $('#plan_array').html(response.plan_array);
              }
            else{
                
            }
            
          },
    });
      
    }).keyup();
});
</script>
@endsection