@extends('backend.layout.master')
@section('title','Slider Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Add Slider</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.slider.update',$slider->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.slider._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_slider" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection