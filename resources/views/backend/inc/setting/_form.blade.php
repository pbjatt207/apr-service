@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('title', 'Enter name'), ['class' => 'active']}}
      {{Form::text('record[title]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          {{Form::label('logo', 'Choose logo'), ['class' => 'active']}}
          {{Form::file('logo',['class'=>'form-control'])}}
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          @if($setting->logo)
          <img src="{{ url('/images/setting/logo/'.$setting->logo) }}" width="50%">
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('tagline', 'Enter tagline'), ['class' => 'active']}}
      {{Form::text('record[tagline]', '', ['class' => 'form-control', 'placeholder'=>'Enter tagline'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          {{Form::label('favicon', 'Choose favicon'), ['class' => 'active']}}
          {{Form::file('favicon',['class'=>'form-control'])}}
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          @if($setting->favicon)
          <img src="{{ url('/images/setting/favicon/'.$setting->favicon) }}" width="30%">
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile'), ['class' => 'active']}}
      {{Form::text('record[mobile]', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Enter email'), ['class' => 'active']}}
      {{Form::text('record[email]', '', ['class' => 'form-control', 'placeholder'=>'Enter email','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('address', 'Enter address'), ['class' => 'active'] }}
      {{ Form::textarea('record[address]','', ['class'=>'form-control', 'placeholder'=>'Enter address', 'rows' => '5', 'cols' => '10']) }}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('facebook', 'Enter facebook url'), ['class' => 'active']}}
      {{Form::text('record[facebook]', '', ['class' => 'form-control', 'placeholder'=>'Enter facebook url'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('instagram', 'Enter instagram url'), ['class' => 'active']}}
      {{Form::text('record[instagram]', '', ['class' => 'form-control', 'placeholder'=>'Enter instagram url'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('twitter', 'Enter twitter url'), ['class' => 'active']}}
      {{Form::text('record[twitter]', '', ['class' => 'form-control', 'placeholder'=>'Enter twitter url'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('pintertest', 'Enter pintertest url'), ['class' => 'active']}}
      {{Form::text('record[pintertest]', '', ['class' => 'form-control', 'placeholder'=>'Enter pintertest url'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('youtube', 'Enter youtube url'), ['class' => 'active']}}
      {{Form::text('record[youtube]', '', ['class' => 'form-control', 'placeholder'=>'Enter youtube url'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('bank_name', 'Enter bank name'), ['class' => 'active']}}
      {{Form::text('record[bank_name]', '', ['class' => 'form-control', 'placeholder'=>'Enter bank name'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('branch_name', 'Enter branch name'), ['class' => 'active']}}
      {{Form::text('record[branch_name]', '', ['class' => 'form-control', 'placeholder'=>'Enter branch name'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('bank_ac', 'Enter bank A/C'), ['class' => 'active']}}
      {{Form::text('record[bank_ac]', '', ['class' => 'form-control', 'placeholder'=>'Enter bank A/C'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('bank_ifsc', 'Enter bank ifsc'), ['class' => 'active']}}
      {{Form::text('record[bank_ifsc]', '', ['class' => 'form-control', 'placeholder'=>'Enter bank ifsc'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('GST', 'Enter GST'), ['class' => 'active']}}
      {{Form::text('record[GST]', '', ['class' => 'form-control', 'placeholder'=>'Enter GST'])}}
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group">
      {{ Form::label('invoice_terms', 'Enter invoice terms'), ['class' => 'active'] }}
      {{ Form::textarea('record[invoice_terms]','', ['class'=>'form-control editor', 'id'=> '', 'placeholder'=>'Enter invoice terms']) }}
    </div>
  </div>

</div>