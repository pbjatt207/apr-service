@extends('backend.layout.master')
@section('title',$role .' Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit {{ $role }}</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.staff.index','Franchise') }}">{{ $role }}</a></li>
                <li class="breadcrumb-item active">Edit {{ $role }}</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.staff.update',[$role, $staff->id]), 'method'=>'POST','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.staff._form')
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="{{ url('/images/admin/aadhar/'.$staff->aadhar_front) }}" alt="" width="100px">
                            <div class="form-group">
                            {{Form::label('aadhar_front', 'Choose aadhar front image'), ['class' => 'active']}}
                            {{Form::file('aadhar_front',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="{{ url('/images/admin/aadhar/'.$staff->aadhar_back) }}" alt="" width="100px">
                            <div class="form-group">
                            {{Form::label('aadhar_back', 'Choose aadhar back image'), ['class' => 'active']}}
                            {{Form::file('aadhar_back',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="{{ url('/images/admin/pan_card/'.$staff->pan_img) }}" alt="" width="100px">
                            <div class="form-group">
                            {{Form::label('pan_img', 'Choose pan image'), ['class' => 'active']}}
                            {{Form::file('pan_img',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="{{ url('/images/admin/bank_statement/'.$staff->bank_statement) }}" alt="" width="100px">
                            <div class="form-group">
                            {{Form::label('bank_statement', 'Choose Cancel Cheque OR Passbook Image'), ['class' => 'active']}}
                            {{Form::file('bank_statement',['class'=>'form-control'])}}
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_staff" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection