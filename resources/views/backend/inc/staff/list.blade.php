@extends('backend.layout.master')
@section('title', $role)
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View {{ $role }}</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">{{ $role }}</li>
                    </ol>
                </div>
                @if(auth()->user()->role_id != 1)
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{ route('admin.staff.create',$role) }}" class="btn btn-primary" type="button"><i class='bx bx-plus'>Add</i></a>
                    </div>
                </div>
                @endif
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <!-- <div class="search-top-table">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                </div>
                                <h4>View staff</h4>
                            </div>
                            <div class="col-lg-6">
                                <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="table-responsiveima pt-4">
                    <input class="form-control mb-3" style="width:25%" id="myInput" type="text" placeholder="Search..">
                        <table>
                            <thead class="the-colo-table">
                                <tr>
                                    <!-- <th><input type="checkbox" name="sr.no">Sr.No</th> -->
                                    <th>S. No.</th>
                                    <th>Added By</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Pan Card</th>
                                    <th>Aadhar Card</th>
                                    <th>Bank Statement</th>
                                    <th>Bank Details</th>
                                    <th>Address</th>
                                    @if($role == 'Shop')
                                    <th>Shop Name</th>
                                    @endif
                                    @if(auth()->user()->id == 1)
                                    <th>Action</th>
                                    @endif
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $sn = 1;
                                @endphp
                                @foreach($lists as $list)
                                <tr>
                                    <td>{{ $sn++ }}</td>
                                    <td>
                                        {{ $list->admin ? $list->admin->fname : 'N/A' }} <br>
                                        <small> ({{ $list->admin ? $list->admin->role->name : 'N/A' }}) </small>
                                    </td>
                                    <td>
                                    @if(auth()->user()->role_id != 1 )
                                    <a href="{{ route('admin.staff.edit',[$role,$list->id]) }}"><i class=" bx bxs-edit"></i> {{ $list->fname }} {{ $list->lname }}</a>
                                    @else 
                                    @if($list->role->name == 'Franchise')
                                    <a href="{{ route('admin.staff.edit',[$role,$list->id]) }}"><i class=" bx bxs-edit"></i> {{ $list->fname }} {{ $list->lname }}</a>
                                    @else
                                    {{ $list->fname }} {{ $list->lname }}
                                    @endif
                                    @endif
                                    </td>
                                    <td>{{ $list->email }} </td>
                                    <td>
                                        @if($list->alternative_mobile_no) <span style="font-weight:550">Mobile No. -</span>  {{$list->alternative_mobile_no}} @endif <br>
                                        @if($list->mobile) <span style="font-weight:550">Mobile No. -</span>  {{$list->mobile}} @endif <br>
                                        @if($list->alternative_whatsapp_no) <span style="font-weight:550">Whatsapp No. -</span>  {{$list->alternative_whatsapp_no}} @endif 
                                    </td>
                                    <td>
                                    <span style="font-weight:bold">{{ $list->pancard ?  $list->pancard : 'N/A' }}</span> <br>
                                    @if($list->pan_img)
                                        <a href="{{ url('/images/admin/pan_card/'.$list->pan_img) }}" target="_blank">
                                            <img src="{{ url('/images/admin/pan_card/'.$list->pan_img) }}" alt="" width="100px" class="table_img">
                                        </a>
                                    @endif  
                                    </td>
                                    <td>
                                        <!-- {{ $list->aadharcard  ? $list->aadharcard : 'N/A' }} -->
                                        <span style="font-weight:bold">{{ $list->aadharcard  ?  $list->aadharcard : 'N/A' }} </span><br>
                                        @if($list->aadhar_front)
                                        <a href="{{ url('/images/admin/aadhar/'.$list->aadhar_front) }}" target="_blank" >
                                            <img src="{{ url('/images/admin/aadhar/'.$list->aadhar_front) }}" alt="" width="60px" class="table_img">
                                        </a>
                                        @endif
                                        @if($list->aadhar_back)
                                        <a href="{{ url('/images/admin/aadhar/'.$list->aadhar_back) }}" target="_blank">
                                            <img src="{{ url('/images/admin/aadhar/'.$list->aadhar_back) }}" alt="" width="60px" class="table_img">
                                        </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($list->bank_statement)
                                        <a href="{{ url('/images/admin/bank_statement/'.$list->bank_statement) }}" target="_blank">
                                            <img src="{{ url('/images/admin/bank_statement/'.$list->bank_statement) }}" alt="" width="100px" class="table_img">
                                        </a>
                                        @endif
                                    </td>
                                    <td> 
                                       <span style="font-weight:550"> {{ $list->bank_name }} {{ $list->branch_name }}</span> <br>
                                       @if($list->ifsc_code) <span style="font-weight:550">IFSC code -</span>  {{$list->ifsc_code}} @endif <br>
                                       @if($list->acc_holder) <span style="font-weight:550">Acc holder -</span>  {{$list->acc_holder}} @endif <br>
                                       @if($list->acc_no) <span style="font-weight:550">Acc No. -</span>  {{$list->acc_no}} @endif
                                    </td>
                                    <td>{{ $list->address }} @if($list->address),@endif {{ $list->city }}@if($list->city),@endif {{ $list->state }}@if($list->state),@endif {{ $list->pincode }}</td>
                                    @if($role == 'Shop')
                                    <td>{{ $list->shop_name }}</td>
                                    @endif
                                    @if(auth()->user()->id == 1)
                                    <td>
                                        <!-- <button class="btn tblActnBtn p-0">
                                            <a href="{{ route('admin.staff.edit',[$role,$list->id]) }}" style="color: black;"><i class="bx bxs-edit"></i></a>
                                        </button> -->
                                        {{ Form::open(array('url' => route('admin.staff.destroy',$list->id), 'class' => 'btn tblActnBtn','style'=> 'padding:0px;')) }}
                                        {{ Form::hidden('_method', 'POST') }}
                                        <button class="btn tblActnBtn">
                                            <a style="color: black;"><i class="bx bxs-trash"></i></a>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                    @endif
                                    @php
                                    $statusArr = [
                                    'true' => 'Active',
                                    'false' => 'Inactive',
                                    ]
                                    @endphp
                                    <td>
                                        {{ Form::select('verified', $statusArr,$list->active, ['class'=>'form-control','id'=>url('/admin-access/staff/status/'.$list->id), 'onchange'=>'myFunction(this)']) }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="p-3 text-right">{{ $lists->links() }}</div>
                </div>
            </section>
        </div>
    </main>
</div>
<script>
    function myFunction(abc) {
        window.location = abc.id + '/' + abc.value;
    }
</script>
@endsection