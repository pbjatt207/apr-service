@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
@if($role == 'Shop')
<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('shop_name', 'Enter shop name'), ['class' => 'active']}}
      {{Form::text('record[shop_name]', '', ['class' => 'form-control', 'placeholder'=>'Enter shop name'])}}
    </div>
  </div>
</div>
@endif
<div class="row">
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('fname', 'Enter first name'), ['class' => 'active']}}
      {{Form::text('record[fname]', '', ['class' => 'form-control', 'placeholder'=>'Enter first name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('lname', 'Enter last name'), ['class' => 'active']}}
      {{Form::text('record[lname]', '', ['class' => 'form-control', 'placeholder'=>'Enter last name'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('password', 'Enter password'), ['class' => 'active']}}
      @if(@$edit->id)
      {{Form::password('record[password]', '', ['class' => 'form-control', 'placeholder'=>'password'])}}
      @else
      {{Form::text('record[password]', '', ['class' => 'form-control', 'placeholder'=>'password','required'=>'required'])}}
      @endif
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Enter email'), ['class' => 'active']}}
      {{Form::text('record[email]', '', ['class' => 'form-control', 'placeholder'=>'Enter email'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile number'), ['class' => 'active']}}
      {{Form::text('record[mobile]', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile number'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('alternative_mobile_no', 'Enter alt mobile number'), ['class' => 'active']}}
      {{Form::text('record[alternative_mobile_no]', '', ['class' => 'form-control', 'placeholder'=>'Enter alt mobile number'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('alternative_whatsapp_no', 'Enter whatsapp number'), ['class' => 'active']}}
      {{Form::text('record[alternative_whatsapp_no]', '', ['class' => 'form-control', 'placeholder'=>'Enter whatsapp number'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('pancard', 'Enter pan no.'), ['class' => 'active']}}
      {{Form::text('record[pancard]', '', ['class' => 'form-control', 'placeholder'=>'Enter pan no.'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('aadharcard', 'Enter aadhar no.'), ['class' => 'active']}}
      {{Form::text('record[aadharcard]', '', ['class' => 'form-control', 'placeholder'=>'Enter aadhar no.'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('acc_no', 'Enter account no.'), ['class' => 'active']}}
      {{Form::text('record[acc_no]', '', ['class' => 'form-control', 'placeholder'=>'Enter account no.'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('acc_holder', 'Enter account holder name.'), ['class' => 'active']}}
      {{Form::text('record[acc_holder]', '', ['class' => 'form-control', 'placeholder'=>'Enter account holder name'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('ifsc_code', 'Enter IFSC code'), ['class' => 'active']}}
      {{Form::text('record[ifsc_code]', '', ['class' => 'form-control', 'placeholder'=>'Enter IFSC code'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('bank_name', 'Enter bank name'), ['class' => 'active']}}
      {{Form::text('record[bank_name]', '', ['class' => 'form-control', 'placeholder'=>'Enter bank name'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('branch_name', 'Enter branch name'), ['class' => 'active']}}
      {{Form::text('record[branch_name]', '', ['class' => 'form-control', 'placeholder'=>'Enter branch name'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('address', 'Enter address'), ['class' => 'active']}}
      {{Form::text('record[address]', '', ['class' => 'form-control', 'placeholder'=>'Enter address'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('city', 'Enter city'), ['class' => 'active']}}
      {{Form::text('record[city]', '', ['class' => 'form-control', 'placeholder'=>'Enter city'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('state', 'Enter state'), ['class' => 'active']}}
      {{Form::text('record[state]', '', ['class' => 'form-control', 'placeholder'=>'Enter state'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('pincode', 'Enter pincode'), ['class' => 'active']}}
      {{Form::text('record[pincode]', '', ['class' => 'form-control', 'placeholder'=>'Enter pincode'])}}
    </div>
  </div>
</div>

