@extends('backend.layout.master')
@section('title', $role)
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Verify {{ $role }}</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.staff.index',$role) }}">{{ $role }}</a></li>
                <li class="breadcrumb-item active">Verify {{ $role }}</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.staff.verify_post',[$role, $id]), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block mt-3" id="successMessage">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($message = Session::get('danger'))
                        <div class="alert alert-danger alert-block mt-3" id="successMessage">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if(count($errors->all()))
                        <div class="alert alert-danger mt-3" id="successMessage">
                            <ul>
                                @foreach($errors->all() as $error)
                                <li style="list-style: none;">{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                {{Form::label('otp_code', 'Enter otp'), ['class' => 'active']}}
                                {{Form::text('record[otp_code]', '', ['class' => 'form-control', 'placeholder'=>'Enter otp'])}}
                                </div>
                            </div>
                        </div>
                        <div class="text-right mt-4">
                            <input type="submit" class="btn btn-primary" name="save" value="Verify" />
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection