@extends('backend.layout.master')
@section('title', $role)
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Add {{ $role }}</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.staff.index',$role) }}">{{ $role }}</a></li>
                <li class="breadcrumb-item active">Add {{ $role }}</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.staff.store',$role), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                    @include('backend.inc.staff._form')
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                            {{Form::label('aadhar_front', 'Choose aadhar front image'), ['class' => 'active']}}
                            {{Form::file('aadhar_front',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                            {{Form::label('aadhar_back', 'Choose aadhar back image'), ['class' => 'active']}}
                            {{Form::file('aadhar_back',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                            {{Form::label('pan_img', 'Choose pan image'), ['class' => 'active']}}
                            {{Form::file('pan_img',['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                            {{Form::label('bank_statement', 'Choose Cancel Cheque OR Passbook Image'), ['class' => 'active']}}
                            {{Form::file('bank_statement',['class'=>'form-control'])}}
                            </div>
                        </div>
                    </div>
                    <div class="text-right mt-4">
                        <input type="submit" class="btn btn-primary" name="save" value="Add" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection