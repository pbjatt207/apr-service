@extends('backend.layout.master')
@section('title','How It Work Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit How It Work</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.howitwork.index') }}">How It Work</a></li>
                <li class="breadcrumb-item active">Edit How It Work</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.howitwork.update',$howitwork->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.howitwork._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_howitwork" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection