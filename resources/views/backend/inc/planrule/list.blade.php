@extends('backend.layout.master')
@section('title','Plan Rule')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View Plan Rule</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Plan Rule</li>
                    </ol>
                </div>
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{ route('admin.planrule.create',$plan) }}" class="btn btn-primary" type="button"><i class='bx bx-plus'>Add</i></a>
                    </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <!-- <div class="search-top-table">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                </div>
                                <h4>View planrule</h4>
                            </div>
                            <div class="col-lg-6">
                                <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="table-responsiveima pt-4">
                        <table>
                            <thead class="the-colo-table">
                                <tr>
                                    <!-- <th><input type="checkbox" name="sr.no">Sr.No</th> -->
                                    <th>S. No.</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $sn = 1;
                                @endphp
                                @foreach($lists as $list)
                                <tr>
                                    <td>{{ $sn++ }}</td>
                                    <td>{{ $list->name }} </td>
                                    <td>
                                        <button class="btn tblActnBtn p-0">
                                            <a href="{{ route('admin.planrule.edit',[$plan,$list->id]) }}" style="color: black;"><i class="bx bxs-edit"></i></a>
                                        </button>
                                        {{ Form::open(array('url' => route('admin.planrule.destroy',[$plan,$list->id]), 'class' => 'btn tblActnBtn','style'=> 'padding:0px;')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button class="btn tblActnBtn">
                                            <a style="color: black;"><i class="bx bxs-trash"></i></a>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </main>
</div>
@endsection