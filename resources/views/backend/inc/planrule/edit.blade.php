@extends('backend.layout.master')
@section('title','Plan Rule Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit Plan Rule</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.planrule.index',$plan) }}">Plan Rule</a></li>
                <li class="breadcrumb-item active">Edit Plan Rule</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.planrule.update',[$plan,$planrule->id]), 'method'=>'POST','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.planrule._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_planrule" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection