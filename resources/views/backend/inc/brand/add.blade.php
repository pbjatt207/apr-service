@extends('backend.layout.master')
@section('title','Brand')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Add Brand</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.brand.index') }}">Brand</a></li>
                <li class="breadcrumb-item active">Add Brand</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.brand.store'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                    @include('backend.inc.brand._form')
                    <div class="text-right mt-4">
                        <input type="submit" class="btn btn-primary" name="save" value="Add" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection