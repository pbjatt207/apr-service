@extends('backend.layout.master')
@section('title','Service Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit Service</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.service.index') }}">Service</a></li>
                <li class="breadcrumb-item active">Edit Service</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.service.update',$service->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.service._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_service" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection