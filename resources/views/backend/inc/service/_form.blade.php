@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('name', 'Enter name'), ['class' => 'active']}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('slug', 'Enter slug'), ['class' => 'active']}}
      {{Form::text('record[slug]', '', ['class' => 'form-control', 'placeholder'=>'Enter slug'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('image', 'Choose image'), ['class' => 'active']}}
      {{Form::file('image',['class'=>'form-control'])}}
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="form-group">
    {{ Form::label('excerpt', 'Enter short description'), ['class' => 'active'] }}
    {{ Form::textarea('record[excerpt]','', ['class'=>'form-control editor', 'placeholder'=>'Enter short description', 'rows' => '3', 'cols' => '10']) }}
  </div>
</div>
<div class="col-lg-12">
  <div class="form-group">
    {{ Form::label('description', 'Enter description'), ['class' => 'active'] }}
    {{ Form::textarea('record[description]','', ['class'=>'form-control editor', 'placeholder'=>'Enter description', 'rows' => '7', 'cols' => '10']) }}
  </div>
</div>
<!-- <hr>
<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('seo_title', 'Enter seo title'), ['class' => 'active']}}
      {{Form::text('record[seo_title]', '', ['class' => 'form-control', 'placeholder'=>'Enter seo title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('seo_keywords', 'Enter seo keyword'), ['class' => 'active'] }}
      {{ Form::textarea('record[seo_keywords]','', ['class'=>'form-control', 'placeholder'=>'Enter seo keyword', 'rows' => '4', 'cols' => '10']) }}
    </div>
  </div>

  <div class="col-lg-6">
    <div class="form-group">
      {{ Form::label('seo_description', 'Enter seo description'), ['class' => 'active'] }}
      {{ Form::textarea('record[seo_description]','', ['class'=>'form-control', 'placeholder'=>'Enter seo description', 'rows' => '4', 'cols' => '10']) }}
    </div>
  </div>
</div> -->