@extends('backend.layout.master')
@section('title','Page')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View Pages</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Pages</li>
                    </ol>
                </div>
                <div class="col-lg-6">
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <!-- <div class="search-top-table">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                </div>
                                <h4>View service</h4>
                            </div>
                            <div class="col-lg-6">
                                <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="table-responsiveima pt-4">
                    <input class="form-control mb-3" style="width:25%" id="myInput" type="text" placeholder="Search.."> 
                        <table>
                            <thead class="the-colo-table">
                                <tr>
                                    <!-- <th><input type="checkbox" name="sr.no">Sr.No</th> -->
                                    <th>S. No.</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <!-- <th>Excerpt</th> -->
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $sn = 1;
                                @endphp
                                @foreach($lists as $list)
                                <tr>
                                    <td>{{ $sn++ }}</td>
                                    <td>
                                        <a href="{{ route('admin.page.edit',$list->id) }}"><i class=" bx bxs-edit"></i> {{ $list->name }}</a>
                                    </td>
                                    <td class="table-img">
                                        @if($list->image)
                                        <img src="{{ url('/').'/images/page/'.$list->image }}" alt="" width="100px">
                                        @else
                                        N/A
                                        @endif
                                    </td>
                                    <!-- <td>@if($list->description)
                                        {!! $list->description !!}
                                        @else
                                        N/A
                                        @endif
                                    </td> -->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                       
                    </div>
                    <div class="p-3 text-right">{{ $lists->links() }}</div>
                </div>
            </section>
        </div>
    </main>
</div>
@endsection