@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter name'), ['class' => 'active']}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          {{Form::label('image', 'Choose image'), ['class' => 'active']}}
          {{Form::file('image',['class'=>'form-control'])}}
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          @if($page->image)
          <img src="{{ url('/images/page/'.$page->image) }}" width="50%">
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group">
      {{ Form::label('description', 'Enter description'), ['class' => 'active'] }}
      {{ Form::textarea('record[description]','', ['class'=>'form-control editor', 'id'=> '', 'placeholder'=>'Enter description']) }}
    </div>
  </div>
</div>