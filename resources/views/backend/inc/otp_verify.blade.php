@extends('backend.layout.loginmaster')
@section('title','OTP Verify')
@section('content')
<div class="main-form-slide fadeInDown">
    <div id="formmy-con">
        <div class="card-header-f">
            <h2> OTP Verify</h2>
        </div>
        {{ Form::open(['url' => route('admin.otp-verify'), 'method'=>'POST', 'files' => true, 'class' => 'user formp-body']) }}
            {{csrf_field()}}

            @if($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{$message}}
            </div>

            @endif
            @if(count($errors->all()))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)

                    <li>{{$error}}</li>

                    @endforeach
                </ul>
            </div>
            @endif
            <label><small>OTP</small></label>
            <input type="text" class="form-controllrt" name="otp_code" placeholder="Enter OTP">
            <input class="form-input" style="width:100%;" name="mobile" type="hidden" value="{{$request->mobile}}" placeholder="Enter Otp" required>
            <a style="float:right" href="{{ url('admin-access/forgot_password_step-2/resend_otp?mobile='.$request->mobile) }}">Resend Otp?</a>
            <div class="clearfix"></div>
            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                <!-- <a>Forgot Password?</a> -->
                <button class="btn btn-primary">Verify</button>
            </div>
        {{ Form::close() }}
        <div id="form-mt-footer">
            <!-- <div class="small"><a href="#">Need an account? Sign up!</a></div> -->
        </div>
    </div>
</div>
@endsection