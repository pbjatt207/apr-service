@extends('backend.layout.master')
@section('title','Dashboard')
@section('content')

<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h1 class="mt-4 titl-fo">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item  active">Dashboard</li>
            </ol>
            @php
            $user = auth()->user();
            @endphp
            <div class="card mb-3 p-3">
                <div class="body">
                    Name : {{ $user->fname }} {{ $user->lname }}<br>
                    Email : {{ $user->email }}<br>
                    Aadhar Card : {{ $user->aadharcard }}<br>
                    Pan Card : {{ $user->pancard }}<br>
                    @if($user->role_id == 3)
                    Shop Name : {{ $user->shop_name }}<br>
                    @endif
                </div>
            </div>
            @if($user->role_id == 1)
            <div class="row">
                <div class="col-xl-6">
                    <div class="card mb-4">
                        <div class="card-header"> <i class="fas fa-chart-area mr-1"></i> Area Chart Example</div>
                        <div class="card-body">
                            <canvas id="myChart" class="canvas-chart-i"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="card mb-4">
                        <div class="card-header"> <i class="fas fa-chart-bar mr-1"></i> Bar Chart Example</div>
                        <div class="card-body">
                            <canvas id="myChart2" class="canvas-chart-i"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </main>
</div>

@endsection