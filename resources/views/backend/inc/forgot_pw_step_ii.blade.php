@extends('backend.layout.loginmaster')
@section('title','Forgot Password')
@section('content')
<div class="main-form-slide fadeInDown">
    <div id="formmy-con">
        <div class="card-header-f">
            <h2> Forgot Password</h2>
        </div>
        {{ Form::open(['url' => route('admin.admin-forgot.step-2'), 'method'=>'POST', 'files' => true, 'class' => 'user formp-body']) }}
            {{csrf_field()}}

            @if($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{$message}}
            </div>

            @endif
            @if(count($errors->all()))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)

                    <li>{{$error}}</li>

                    @endforeach
                </ul>
            </div>
            @endif
            <label><small>OTP</small></label>
            <input type="text" class="form-controllrt" name="otp_code" placeholder="Enter OTP" style="margin-bottom:5px">
            <a href="{{ url('admin-access/forgot_password_step-2/resend_otp?mobile='.$request->mobile) }}" style="margin-bottom:24px;float:right">Resend Otp?</a><br>
            <label><small>New Password</small></label>
            <input type="password" class="form-controllrt" name="password" placeholder="New Password">
            <input class="form-input" style="width:100%;" name="mobile" type="hidden" value="{{$request->mobile}}" placeholder="Enter Otp" required>
            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                <!-- <a>Forgot Password?</a> -->
                <button class="btn btn-primary">Save Password</button>
            </div>
        {{ Form::close() }}
        <div id="form-mt-footer">
            <!-- <div class="small"><a href="#">Need an account? Sign up!</a></div> -->
        </div>
    </div>
</div>
@endsection