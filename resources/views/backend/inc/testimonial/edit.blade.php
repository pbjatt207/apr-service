@extends('backend.layout.master')
@section('title','Testimonial Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit Testimonial</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.testimonial.index') }}">Testimonial</a></li>
                <li class="breadcrumb-item active">Edit Testimonial</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.testimonial.update',$testimonial->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.testimonial._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_testimonial" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection