@extends('backend.layout.loginmaster')
@section('title','Admin Login')
@section('content')
<div class="main-form-slide fadeInDown">
    <div id="formmy-con">
        <div class="card-header-f">
            <h2> Login</h2>
        </div>
        <form method="post" action="{{ route('admin.checklogin') }}" class="formp-body">
            {{csrf_field()}}

            @if($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{$message}}
            </div>

            @endif
            @if(count($errors->all()))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)

                    <li>{{$error}}</li>

                    @endforeach
                </ul>
            </div>
            @endif
            <label><small>Mobile</small></label>
            <input type="text" class="form-controllrt" name="login" placeholder="Mobile">
            <label><small>Password</small></label>
            <input type="password" class="form-controllrt" name="password" placeholder="Password">

            <!-- <div class="customcontrolche">
                <input type="checkbox">
                <label>Remember password</label>
            </div> -->
            <a style="float:left" href="{{ url('/admin-access/forgot_password_step-1') }}">Forgot Password ?</a>
            <div class="clearfix"></div>

            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                <!-- <a>Forgot Password?</a> -->
                <button class="btn btn-primary">Login</button>
            </div>
        </form>
        <div id="form-mt-footer">
            <!-- <div class="small"><a href="#">Need an account? Sign up!</a></div> -->
        </div>
    </div>
</div>
@endsection