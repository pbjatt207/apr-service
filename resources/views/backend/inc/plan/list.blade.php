@extends('backend.layout.master')
@section('title','Plan')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="mt-4 titl-fo">View Plan</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Plan</li>
                    </ol>
                </div>
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{ route('admin.plan.create') }}" class="btn btn-primary" type="button"><i class='bx bx-plus'>Add</i></a>
                    </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                    <!-- <div class="search-top-table">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                </div>
                                <h4>View plan</h4>
                            </div>
                            <div class="col-lg-6">
                                <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="table-responsiveima pt-4">
                    <input class="form-control mb-3" style="width:25%" id="myInput" type="text" placeholder="Search..">
                        <table>
                            <thead class="the-colo-table">
                                <tr>
                                    <!-- <th><input type="checkbox" name="sr.no">Sr.No</th> -->
                                    <th>S. No.</th>
                                    <th>Name</th>
                                    <th>Min Price</th>
                                    <th>Max Price</th>
                                    <th>Price (per year)</th>
                                    <th>Price (per month)</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $sn = 1;
                                @endphp
                                @foreach($lists as $list)
                                <tr>
                                    <td>{{ $sn++ }}</td>
                                    <td>
                                        <a href="{{ route('admin.plan.edit',$list->id) }}"><i class=" bx bxs-edit"></i> {{ $list->name }}</a>
                                    </td>
                                    <td>₹{{ $list->minprice }} </td>
                                    <td>₹{{ $list->maxprice }} </td>
                                    <td>₹{{ $list->price }} </td>
                                    <td>₹{{ $list->per_month_price }} </td>
                                    <td>
                                        <!-- <button class="btn tblActnBtn p-0">
                                            <a href="{{ route('admin.plan.edit',$list->id) }}" style="color: black;"><i class="bx bxs-edit"></i></a>
                                        </button> -->
                                        {{ Form::open(array('url' => route('admin.plan.destroy',$list->id), 'class' => 'btn tblActnBtn','style'=> 'padding:0px;')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        <button class="btn tblActnBtn">
                                            <a style="color: black;"><i class="bx bxs-trash"></i></a>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                    <td>
                                        <button class="btn tblActnBtn p-0">
                                            <a href="{{ route('admin.planrule.index',$list->id) }}"><i class="bx bxs-eye"></i> View Rules</a>
                                        </button>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <div class="p-3 text-right">{{ $lists->links() }}</div>
                </div>
            </section>
        </div>
    </main>
</div>
@endsection