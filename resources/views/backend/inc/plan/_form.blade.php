@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('name', 'Enter name'), ['class' => 'active']}}
      {{Form::text('record[name]', '', ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('minprice', 'Enter min price'), ['class' => 'active']}}
      {{Form::text('record[minprice]', '', ['class' => 'form-control', 'placeholder'=>'Enter min price'])}}
    </div>
  </div>
  <div class="col-lg-4">
    <div class="form-group">
      {{Form::label('maxprice', 'Enter max price'), ['class' => 'active']}}
      {{Form::text('record[maxprice]', '', ['class' => 'form-control', 'placeholder'=>'Enter max price'])}}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('price', 'Enter price (per year)'), ['class' => 'active']}}
      {{Form::text('record[price]', '', ['class' => 'form-control', 'placeholder'=>'Enter price'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('per_month_price', 'Enter price (per month)'), ['class' => 'active']}}
      {{Form::text('record[per_month_price]', '', ['class' => 'form-control', 'placeholder'=>'Enter price'])}}
    </div>
  </div>
</div>