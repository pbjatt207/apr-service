@extends('backend.layout.master')
@section('title','Change Password')
@section('content')
<div class="main-content box">
	<main class="main-i">
		<div class="container-fluid">
			<h3 class="mt-4 titl-fo">Change Password</h3>
			<ol class="breadcrumb mb-4">
				<li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
				<li class="breadcrumb-item active">Change Password</li>
			</ol>
			<div class="main-addpage-main">
				<div class="main-form-deco">
					{{ Form::open(['url' => route('admin.user.update'), 'method'=>'POST','files' => true, 'class' => 'user']) }}
					@if($message = Session::get('error'))
					<div class="alert alert-danger alert-block">
						<button type="button" class="close" data-dismiss="alert">x</button>
						{{$message}}
					</div>

					@endif
					@if(count($errors->all()))
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)

							<li>{{$error}}</li>

							@endforeach
						</ul>
					</div>
					@endif
					<div class="row">
						<!-- <div class="col-lg-12">
							<div class="form-group">
								{{Form::label('current_password', 'Enter current password'), ['class' => 'active']}}
								{{Form::text('current_password', '', ['class' => 'form-control', 'placeholder'=>'Enter current password','required'=>'required'])}}
							</div>
						</div> -->
						<div class="col-lg-12">
							<div class="form-group">
								{{Form::label('new_password', 'Enter new password'), ['class' => 'active']}}
								{{Form::text('new_password', '', ['class' => 'form-control', 'placeholder'=>'Enter new password','required'=>'required'])}}
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								{{Form::label('confirm_password', 'Confirm new password'), ['class' => 'active']}}
								{{Form::text('confirm_password', '', ['class' => 'form-control', 'placeholder'=>'Confirm new password','required'=>'required'])}}
							</div>
						</div>
					</div>

					<div class="text-right">
						<input type="submit" class="btn btn-primary" name="edit_page" value="Submit" />
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</main>
</div>
@endsection