@extends('backend.layout.master')
@section('title','Smart Phone Edit')
@section('content')
<div class="main-content box">
    <main class="main-i">
        <div class="container-fluid">
            <h3 class="mt-4 titl-fo">Edit Smart Phone</h3>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active"><a href="{{ route('admin.home') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.smartphone.index') }}">Smart Phone</a></li>
                <li class="breadcrumb-item active">Edit Smart Phone</li>
            </ol>
            <div class="main-addpage-main">
                <div class="main-form-deco">
                    {{ Form::open(['url' => route('admin.smartphone.update',$smartphone->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                    @include('backend.inc.smartphone._form')
                    <div class="text-right">
                        <input type="submit" class="btn btn-primary" name="edit_smartphone" value="Update" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </main>
</div>
@endsection