<!DOCTYPE html>
<html>

<head>
	<base href="{{ url('/') }}">
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#000000" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') | Admin Panel</title>
	{{Html::style('admin/fonts/font-awesome.min.css')}}
	{{Html::style('https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css')}}
	{{Html::style('admin/css/bootstrap.min.css')}}
	{{Html::style('https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css')}}
	{{Html::style('admin/css/style.css')}}
	{{Html::style('admin/css/responsive.css')}}
	{{Html::style('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js')}}
</head>

<body>
	<div class="main-des-u">
		<div class="main-contan-i">
			<div class="">
				@include('backend.common.header')
				@include('backend.common.sidebar')
				@yield('content')
			</div>
		</div>
	</div>
	{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js')}}
	{{ Html::script('admin/js/bootstrap.bundle.min.js')}}
	{{ Html::script('admin/js/jquery.min.js')}}
	{{ Html::script('admin/js/togglebtn.js')}}
	{{ Html::script('admin/js/accordion.js')}}
	{{ Html::script('admin/js/script.js')}}
	{{ Html::script('admin/js/validation.js')}}
	{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.8.2/tinymce.min.js')}}
	{{ Html::script('https://checkout.razorpay.com/v1/checkout.js')}}

	<script type="text/javascript">
		$('.select').selectpicker();

	</script>
	<script>
		$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
		});
		</script>
	<script type="text/javascript">
            // Editor TinyMCE
            tinymce.init({
                selector: '.editor',
                plugins: "code, link, image, textcolor, emoticons, hr, lists, charmap, table, tiny_mce_wiris, fullscreen",
                fontsizeselect: true,
                browser_spellcheck: true,
                menubar: false,
                toolbar: 'bold italic underline strikethrough |formatselect h1 h2 h3 h4 link | table hr superscript subscript | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | uploadImageButton | code | fullscreen',

                // forced_root_block : 'div',
                branding: false,
                protect: [
                    /\<\/?(if|endif)\>/g, // Protect <if> & </endif>
                    /\<xsl\:[^>]+\>/g, // Protect <xsl:...>
                    /\<script\:[^>]+\>/g, // Protect <xsl:...>
                    /<\?php.*?\?>/g // Protect php code
                ],
                images_upload_credentials: true,
                file_browser_callback_types: 'image',
                image_dimensions: true,
                automatic_uploads: true,

                relative_urls: false,
                remove_script_host: false,
                // font_format: "devlys 010normal=devlys 010normal",
                setup: function(editor) {
                    // editor.ui.registry.addButton('uploadImageButton', {
                    // icon: 'image',
                    // onAction: function(_) {
                    // $('#mediaModal').modal('show');
                    // }
                    // });
                }
            });
        </script>
	@yield('footer-script')
</body>

</html>