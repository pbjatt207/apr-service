<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta name="theme-color" content="#000000" />
	<title>Login | Admin Panel</title>

	{{Html::style('admin/fonts/font-awesome.min.css')}}
	{{Html::style('https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css')}}
	{{Html::style('admin/css/bootstrap.min.css')}}
	{{Html::style('https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css')}}
	{{Html::style('admin/css/style.css')}}
	{{Html::style('admin/css/responsive.css')}}

</head>

<body class="body-login-pagr">

	@yield('content')

	{{ Html::script('admin/js/bootstrap.bundle.min.js')}}
	{{ Html::script('admin/js/jquery.min.js')}}
	{{ Html::script('admin/js/togglebtn.js')}}
	{{ Html::script('admin/js/accordion.js')}}
	{{ Html::script('admin/js/script.js')}}

</body>

</html>