<section class="footer-bg-color">
	<div class="container">
		<div class="footer-control-pd">
			<hr style="background: url({{ url('images/setting/logo/'.$setting->logo) }}) no-repeat top center; background-size: contain;">
			<div class="social-icon-footer">
				<ul>
					@if($setting->facebook)
					<li><a href="{{ $setting->facebook }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
					@endif
					@if($setting->twitter)
					<li><a href="{{ $setting->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
					@endif
					@if($setting->youtube)
					<li><a href="{{$setting->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a></li>
					@endif
					@if($setting->pinterest)
					<li><a href="{{$setting->pinterest}}" target="_blank"><i class="fa fa-pinterest"></i></a></li>
					@endif
					@if($setting->instagram)
					<li><a href="{{$setting->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
					@endif
				</ul>
			</div>
			<div class="footer-menu">
				<ul>
					@foreach($pages as $item)
					@if($item->slug != 'about-us' && $item->slug != 'plan-benefits')
					<li><a href="{{url('page/'.$item->slug)}}">{{$item->name}}</a></li>
					@endif
					@endforeach
				</ul>
			</div>
			<h4>Copyright © All rights reserved.</h4>
		</div>
	</div>
</section>
<div id="back-top">
	<a title="go to top" href="#">
		<i class="fa fa-arrow-up" aria-hidden="true"></i>
	</a>
</div>