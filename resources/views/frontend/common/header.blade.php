<div class="overly-main"></div>
<header>
	<div class="header-top-i h-bg-c-t header  " id="myHeader">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-6 col-md-6 col-sm-6">
					<div class="logo-top-left">
						<a href="{{ url('/')}}">
							<img src="{{ url('images/setting/logo/'.$setting->logo) }}" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="col-lg-3 display-teble-view col-md-5">
					<div class="top-button-header">
						<ul>
							@if(empty(auth()->user()))
							<li class="btn"><a href="{{ route('admin.login') }}">Shop</a></li>
							@endif
							@if(auth()->user())
							<li class="btn"><a href="{{ url('/profile') }}">
									<i class="fa fa-user"></i>  Hi {{ ucwords(strtolower(auth()->user()->fname)) }}
								</a>


							</li>
							@else
							<li class="btn"><a href="{{ url('login')}}">Customer</a></li>
							@endif
						</ul>
					</div>
				</div>
				<div class="col-lg-7 col-6 col-md-1 col-sm-6">
					<div class="menu-top-header">
						<ul>
							<li>
								<a href="{{ url('') }}" class="aactivee">
									Home
									<hr>
								</a>
							</li>
							<li>
								<a href="{{ url('page/about-us') }}" class="aactivee">
									About us
									<hr>
								</a>
							</li>
							<!-- <li>
								<a href="{{ url('/service') }}" class="aactivee">
									Services
									<hr>
								</a>
							</li> -->
							<li>
								<a href="{{ url('page/plan-benefits') }}" class="aactivee">
								Plan Benefits
									<hr>
								</a>
							</li>
							<li>
								<a href="{{ url('/faq') }}" class="aactivee">
									FAQ
									<hr>
								</a>
							</li>
							<li>
								<a href="{{ url('/contact') }}" class="aactivee">
									Contact Us
									<hr>
								</a>
							</li>
						</ul>
					</div>
					<div class="sidebar-btn">
						<div class=" mob-s-y " id="button-enlarge">
							<div class="bars bar1"></div>
							<div class="bars bar2"></div>
							<div class="bars bar3"></div>
						</div>
					</div>
					<!-- sidebar -->
					<div class="sidebar mobileview">
						<div class="menu-mobil-vew">
							<ul>
								<div class="display-show-mobile">
									<ul>
									@if(empty(auth()->user()))
										<li class="btn"><a href="{{ route('admin.login') }}">Shop</a></li>
										@endif
										@if(auth()->user())
										<li class="btn"><a href="{{ url('/profile') }}">
												<i class="fa fa-user"></i> Hi {{ ucwords(strtolower(auth()->user()->fname)) }}
											</a>

										</li>
										@else
										<li class="btn"><a href="{{ url('login')}}">Customer</a></li>
										@endif
									</ul>
								</div>
								<li><a href="{{ url('') }}" class="aactivee">Home</a></li>

								<li><a href="{{ url('page/about-us') }}" class="aactivee">About us</a></li>

								<li><a href="{{ url('/service') }}" class="aactivee">Services</a></li>
								<li><a href="{{ url('/faq') }}" class="aactivee">FAQ</a></li>
								<li><a href="{{ url('/contact') }}" class="aactivee">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<!-- sidbar -->
				</div>
				<div class="col-lg-3 display-n-mobile col-md-4">
					<div class="top-button-header">
						<ul>
							@if(empty(auth()->user()))
							<li class="btn"><a href="{{ route('admin.login') }}">Shop</a></li>
							
							@endif
							@if(auth()->user())
							<li class="btn" ><a href="{{ url('/profile') }}">
									<i class="fa fa-user"></i> Hi {{ ucwords(strtolower(auth()->user()->fname)) }}
								</a>
								<!-- <div style="position:absolute;z-index:2;background:#fff;border:1px solid #aaa;border-radius:8px;top:90%">
										<ul style="background:#fff;margin:10px;list-style:none" class="header_child_menu">
											<li style="background:#fff;border-color:#fff;text-align:left">Profile</li>
											<li style="background:#fff;border-color:#fff;text-align:left">Change Password</li>
											<li style="background:#fff;border-color:#fff;text-align:left">Logout</li>
										</ul>
									</div> -->
							</li>
							@else
							<li class="btn"><a href="{{ url('login')}}">Customer</a></li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			{{ Form::open(['url' => url('inquery'), 'method'=>'POST']) }}
			<div class="modal-body">
				<div class="po-enqur-form">
					<div class="inquri">
						<h2>Airoshot</h2>
					</div>
					<input type="text" name="record[name]" placeholder="Your Name" required>
					<input type="Email" name="record[email]" placeholder="Your Email" required>
					<input type="text" name="record[mobile]" placeholder="Mobile Number" required>
					<textarea type="text-area" name="record[message]" placeholder="Inquery"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary">Send Message</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div> -->