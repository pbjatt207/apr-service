@php
$setting = App\Model\Setting::find(1);
$pages = App\Model\Page::get();
@endphp
<!DOCTYPE html>
<html>

<head>
	<base href="{{ url('/') }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="theme-color" content="#4c60b1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="@yield('keyword')">
	<meta name="url" content="@yield('url')">
	<meta name="author" content="Pankaj Choudhary">

	{{Html::style('https://use.fontawesome.com/releases/v5.8.1/css/solid.css')}}
	{{Html::style('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}
	{{Html::style('https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css')}}
	{{Html::style('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css')}}
	{{Html::style('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css')}}
	{{Html::style('assets/css/bootstrap.min.css')}}
	{{Html::style('assets/css/style.css')}}
	{{Html::style('assets/css/responsive.css')}}

</head>

<body>

	@include('frontend.common.header')
	@yield('contant')
	@include('frontend.common.footer')

	{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js')}}
	{{ Html::script('assets/js/jquery-3.5.1.js')}}
	{{ Html::script('assets/js/bootstrap.min.js')}}
	{{ Html::script('assets/js/jquery.js')}}
	{{ Html::script('admin/js/validation.js')}}
	{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js')}}
	{{ Html::script('https://checkout.razorpay.com/v1/checkout.js')}}

	<script>
		$('.slider').slick({
			dots: false,
			infinite: true,
			speed: 1000,
			autoplay: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false
					}
				}

			]
		});
	</script>
	<script>
		$(document).ready(function() {
			$(".edit").click(function() {
				$(".input-disable").removeAttr('disabled');
				$(".edit_btn").show();
			});
		});
	</script>
	<script>
		//silck slider
		$('.logoslider').slick({
			dots: false,
			infinite: true,
			speed: 1000,
			autoplay: true,
			slidesToShow: 5,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: false
					}
				}

			]
		});
	</script>
	<script type="text/javascript">

		var dtToday = new Date();
    
		var month = dtToday.getMonth() + 1;
		var day = dtToday.getDate();
		var year = dtToday.getFullYear();
		if(month < 10)
			month = '0' + month.toString();
		if(day < 10)
			day = '0' + day.toString();
		
		var maxDate= year + '-' + month + '-' + day ;

		dtToday.setDate(dtToday.getDate()-6);

		month = dtToday.getMonth() + 1;
		day = dtToday.getDate();
		year = dtToday.getFullYear();
		if(month < 10)
			month = '0' + month.toString();
		if(day < 10)
			day = '0' + day.toString();

		var minDate= year + '-' + month + '-' + day;
		
		$('#txtDate').attr('min', minDate);
		$('#txtDate').attr('max', maxDate);
	</script>
	@yield('footer-script')
</body>

</html>