@extends('frontend.layout.master')
@section('title', 'OTP Verify')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="costmer-login-page-my">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="login-costmer-content">
                    <div class="lotgin-costmer-h2">
                        <h2>Sign in to</h2>
                        <h2>REGISTERED</h2>
                    </div>
                    <br>
                    <p>if you have an account</p>
                    <p>You can <span><a href="{{ url('/login') }}">LOGIN NOW</a></span></p>
                </div>
                <div class="img-banner-costmer">
                    <img src="assets/imgs/71-Su4Wr0HL._SY741_.jpg">
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::open(['url' => route('otp-verify'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-3" id="successMessage">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('danger'))
                <div class="alert alert-danger alert-block mt-3" id="successMessage">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if(count($errors->all()))
                <div class="alert alert-danger mt-3" id="successMessage">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li style="list-style: none;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="login-costmer-right-form">
                    <input type="text" name="otp_code" class="form-controllrt-m" placeholder="Enter OTP" required>
                    <a href="{{ url('register/resend_otp') }}">Resend Otp?</a>
                    <button type="submit" class="btn btn-primary signup-costmer-button">OTP Verify</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>


@stop