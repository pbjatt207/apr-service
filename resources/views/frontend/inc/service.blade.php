@extends('frontend.layout.master')
@section('title', 'Service')
@section('keyword', '')
@section('description', '')
@section('contant')
<section class="about-page">
	<div class="backcolo-about">
		<h1>Services</h1>
		<p><a href="{{ url('/')}}">Home</a>&nbsp; /&nbsp;&nbsp;<span>Services</span></p>
	</div>
	<div class="container">
        <div class="top-seling-brand my-5">
			<div class="row">
				@foreach($lists as $item)
				<div class="col-lg-4 p-0">
					<div class="main-phonsideimg">
						<div class="img-phonbradleft">
							@if($item->image!='')
							<img src="{{ url('images/service/'.$item->image) }}" alt="{{$item->name}}">
							@else
							<img src="{{ url('images/default.png') }}" alt="{{$item->name}}" width="100%">
							@endif
						</div>
						<div class="img-contents">
							<h4>{{$item->name}}</h4>
							<p class="service-box">{!! $item->excerpt !!}</p>
							<!-- <span>{{ $item->excerpt }}</span> -->
							<!-- <p>From ₹1,194/month</p> -->
							<!-- <span>Zero Down Payment</span> -->
						</div>
					</div>
				</div>
				@endforeach
				
			</div>
		</div>
	</div>
</section>
@endsection