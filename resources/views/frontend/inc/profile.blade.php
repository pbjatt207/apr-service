@extends('frontend.layout.master')
@section('title', 'Profile')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="about-page">
    <div class="backcolo-about">
        <h1 style="text-tranform: capitialize">Profile</h1>
        <p><a href="{{ url('/')}}">Home</a>&nbsp; /&nbsp;&nbsp;<span>Profile</span></p>
    </div>
    <div class="container">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block mt-3" id="successMessage">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block mt-3" id="successMessage">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if(count($errors->all()))
        <div class="alert alert-danger mt-3" id="successMessage">
            <ul>
                @foreach($errors->all() as $error)
                <li style="list-style: none;">{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row my-5" style="border: 1px solid #dfdfdf;">
            <div class="col-2 nav flex-column nav-pills pr-0" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="border-right: 1px solid #dfdfdf;">
                <a class="tab-pill nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Profile</a>
                <a class="tab-pill nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Change Password</a>
                <a class="tab-pill nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-insurance" role="tab" aria-controls="v-pills-insurance" aria-selected="false">Repairing Services</a>
                <a class="tab-pill nav-link" href="{{url('/logout')}}">Logout</a>
            </div>
            <div class=" col-10 tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <div class="main">
                        <h2 class="mb-3 text-center">Information</h2>
                        {{ Form::open(['url' => route('user.update'), 'method'=>'POST','files' => true, 'class' => 'user']) }}
                        <div class="card p-0" style="border: 0;">
                            <div class="">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="fname" value="{{auth()->user()->fname}}" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="email" value="{{auth()->user()->email}}" class="form-control" placeholder="Enter Email">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mobile</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" value="{{auth()->user()->mobile}}" class="form-control input-disable" disabled>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>:</td>
                                            <td>
                                                <textarea name="address" class="form-control" value="" placeholder="Enter Address" cols="30" rows="10">{{auth()->user()->address}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="city" value="{{auth()->user()->city}}" class="form-control" placeholder="Enter City">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="state" value="{{auth()->user()->state}}" class="form-control" placeholder="Enter State" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pincode</td>
                                            <td>:</td>
                                            <td>
                                            <input type="number" name="pincode" value="{{auth()->user()->pincode}}" class="form-control" placeholder="Enter Pincode" max="6">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="text-right mt-4">
                                                    <input type="submit" class="btn btn-primary" value="Update" />
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                    <div class="main">
                        <h2 class="mb-3 text-center">Change Password</h2>
                        {{ Form::open(['url' => route('user.changepassword'), 'method'=>'POST','files' => true, 'class' => 'user']) }}
                        <div class="card p-0" style="border: 0;">
                            <div class="">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>New Password</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="new_password" placeholder="New Password" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Confirm Password</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="confirm_password" placeholder="Confirm Password" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="text-right mt-4">
                                                    <input type="submit" class="btn btn-primary" value="Update" />
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-insurance" role="tabpanel" aria-labelledby="v-pills-insurance-tab">
                    <div class="main" style="margin:10px">
                    @foreach($repair_services as $item)
                        <table class="service_history">
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">Txn_id :</span>
                                        <span>#{{$item->txn_id}}</span>
                                    </div>
                                </td>
                                <td style="width: 60%" colspan = "2">
                                    <div>
                                        <span class="service_history_title">Date :</span>
                                        <span>{{ $item->created_at }}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">Name :</span>
                                        <span>{{ $item->name }}</span>
                                    </div>
                                </td>
                                <td style="width: 60%" colspan = "2">
                                    <div>
                                        <span class="service_history_title">Email :</span>
                                        <span>{{ $item->email }}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">Mobile :</span>
                                        <span>{{$item->mobile}}</span>
                                    </div>
                                </td>
                                <td style="width: 60%" colspan = "2">
                                    <div>
                                        <span class="service_history_title">Aadhar No :</span>
                                        <span>{{$item->aadharcard}}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">Plan brand :</span>
                                        <span>{{$item->plan_brand}}</span>
                                    </div>
                                </td>
                                <td style="width: 60%" colspan = "2">
                                    <div>
                                        <span class="service_history_title">Plan price :</span>
                                        <span>₹{{ $item->plan_price }}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">IMEI number :</span>
                                        <span>{{$item->imei_number1}}</span>
                                    </div>
                                </td>
                                <td style="width: 60%" colspan = "2">
                                    <div>
                                        <span class="service_history_title">IMEI number :</span>
                                        <span>{{$item->imei_number2}}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%">
                                    <div>
                                        <span class="service_history_title">Payment :</span>
                                        <span>₹{{$item->payment}}</span>
                                    </div>
                                </td>
                                <td style="width:30%">
                                    <div>
                                        <span class="service_history_title">Payment Mode :</span>
                                        <span>{{$item->payment_mode}}</span>
                                    </div>
                                </td>
                                <td style="width:30%">
                                    <div>
                                        <span class="service_history_title">Payment Status :</span>
                                        <span>{{$item->payment_status}}</span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        @endforeach
                    </div>
                </div>
                <!-- <div class="tab-pane fade" id="v-pills-logout" role="tabpanel" aria-labelledby="v-pills-logout-tab">
                    <div class="main">
                        <h2 class="mb-3 text-center">Change Password</h2>
                        {{ Form::open(['url' => route('user.changepassword'), 'method'=>'POST','files' => true, 'class' => 'user']) }}
                        <div class="card p-0" style="border: 0;">
                            <div class="">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>New Password</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="new_password" placeholder="New Password" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Confirm Password</td>
                                            <td>:</td>
                                            <td>
                                                <input type="text" name="confirm_password" placeholder="Confirm Password" class="form-control" required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="text-right mt-4">
                                                    <input type="submit" class="btn btn-primary" value="Update" />
                                                </div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div> -->
            </div>
            <!-- <div class="col-9">
                
            </div> -->

            <!-- End -->

            <!-- Main -->

        </div>
    </div>
</section>

@endsection