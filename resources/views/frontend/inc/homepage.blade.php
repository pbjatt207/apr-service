@extends('frontend.layout.master')
@section('title', 'Home Page')
@section('keyword', '')
@section('description', '')
@section('contant')

<!-- Slider -->
@if(!$sliders->isEmpty())
<section>
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block mt-3" id="successMessage">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('danger'))
<div class="alert alert-danger alert-block mt-3" id="successMessage">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger mt-3" id="successMessage">
	<ul>
		@foreach($errors->all() as $error)
		<li style="list-style: none;">{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif
	<div class="slider-baner">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				@foreach($sliders as $key => $item)
				<li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
				@endforeach
			</ol>
			<div class="carousel-inner">
				<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
					@foreach($sliders as $key => $item)

					<div class="carousel-item {{$key == 0 ? 'active' : '' }}">
						<img class="d-block w-100" src="{{ url('images/slider/'.$item->image) }}" alt="{{$item->name}}">
					</div>
					@endforeach
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="left display-n" aria-hidden="true"><i class="fa fa-angle-left"></i></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="right display-n" aria-hidden="true"><i class="fa fa-angle-right"></i></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</section>
@endif

<!-- Search Plans -->
<section class="form-mobildetalis">
	<div class="container">
		<div class="devic-form">
			<div class="card-header-f">
				
				<h2>Enter device details</h2>
			</div>
			{{ Form::open(['url' => route('plan'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
			<label><small>When did you purchase your device (DD/MM/YYYY)?</small></label>
			<input type="date" name="date" class="form-controllrt "  id="txtDate" value="{{ date('Y-m-d') }}"  placeholder="Date"  required>

			<label><small>How much did you buy it for?</small></label>
			<input type="text" name="price" class="form-controllrt" placeholder="How much did you buy it for?" required>

			<label><small>Select your mobile brand</small></label>
			<input type="text" name="brand" class="form-controllrt" list="List" placeholder="Search Brand" required>
			<datalist id="List" class="maindetalist">
				@foreach($brands as $item)
				<option value="{{$item->name}}"></option>
				@endforeach
			</datalist>

			<div class="button-main-plan">
				<!-- <a href="{{url('/plan')}}"> -->
				<button type="submit" class="btn btn-primary btn-lg btn-block">See Best Plans</button>

				<!-- </a> -->
				{{ Form::close() }}
			</div>
		</div>
</section>

<!-- Mobiles -->
<section class="top-se-bg">
	<div class="container my-con-topselling">
		<div class="top-selling">
			<h2>Top Selling Smartphones</h2>
		</div>
		<div class="top-seling-brand">
			<div class="row">
				@foreach($smartphones as $item)
				<div class="col-lg-4 p-0">
					<div class="main-phonsideimg">
						<div class="img-phonbradleft">
							@if($item->image!='')
							<img src="{{ url('images/smartphone/'.$item->image) }}" alt="{{$item->name}}">
							@else
							<img src="{{ url('images/default.png') }}" alt="{{$item->name}}" width="100%">
							@endif
						</div>
						<div class="img-content">
							<h4>{{$item->name}}</h4>
							<p>₹{{ $item->price }}</p>
							<span>{{ $item->model }}</span>
							<!-- <p>From ₹1,194/month</p> -->
							<!-- <span>Zero Down Payment</span> -->
						</div>
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
</section>

<!-- How It Work -->
<section class="howdusitwork">
	<div class="container">
		<div class="hedding-how">
			<h2>How does it work?</h2>
		</div>
		<div class="howdoseitworkimg">
			<div class="row">
				@foreach($howitwork as $item)
				<div class="col-lg-3 text-center">
					<div class="iconlimg">
						<img src="{{url('images/howitwork/'.$item->image)}}">
					</div>
					<h5>{{ $item->name }}</h5>
					<p>{!! $item->excerpt !!}</p>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>

<!-- Faqs -->
<section class="faq-main-color-bg">
	<div class="container">
		<section class="faq-in">
			<div class="hedding-faq">
				<h2>FAQs</h2>
			</div>
			<div id="wrapper">
				<div class="accordion">

					@foreach($faqs as $key => $item)

					<div class="accordion-box  {{ $key == 0 ? 'shown' : ''  }}">
						<div class="acordi-in">
							<div class="d-flex">
								<p>{{$item->title}}</p>
								<span><i class='bx bxs-chevron-right'></i></span>
							</div>
						</div>
						<div class="accordion-content " style="{{ $key == 0 ?  'display: block;' : 'display: none;'}}">
							"{!! $item->description !!}"
						</div>
					</div>

					@endforeach

				</div>
			</div>
		</section>
	</div>
</section>

<!-- Testimonial -->
<section class="testimonial">
	<div class="container">
		<div class="hedding-testimo">
			<h2>Testimonial</h2>
		</div>
		<div class="row slider">
			@foreach($testimonials as $key => $item)
			<div class="card mb-3 cardtestim">
				<div class="row no-gutters">
					<div class="col-md-4">
						<div class="left-side-img">
							<img src="{{ url('images/testimonial/'.$item->image) }}" class="card-img" alt="{{ $item->name }}">
						</div>
					</div>
					<div class="col-md-8">
						<div class="card-body">
							<p class="card-text my-cardtext ">" {!! $item->excerpt !!}"</p>
							<h5 class="card-title mytitle">{{ $item->name }}</h5>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

<!-- Brands -->
<section class="container">
	<div class="allbarand">
		<div class="hedding-allbred">
			<h2>All your favorite brands available</h2>
		</div>
		<div class="row logoslider">
			@foreach($brands as $item)
			<div class="prodecticon">
				<img src="{{url('images/brand/'.$item->image)}}">
			</div>
			@endforeach
		</div>
	</div>
</section>

@stop