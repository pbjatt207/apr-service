@extends('frontend.layout.master')
@section('title', 'Profile')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="about-page">
    <div class="backcolo-about">
        <h1 style="text-tranform: capitialize">Enter Your Device Details</h1>
        <p><a href="{{ url('/')}}">Home</a>&nbsp; /&nbsp;&nbsp;<span>Enter Your Device Details</span></p>
    </div>
    <div class="container">
        <div class="mt-5 mb-5">
            

            @if(!@$record->id)
            <div id="iamgeUpload">
                {{ Form::open(['url'=>route('insurance.store',$plan->id), 'method' => 'POST' ,'id'=> 'uploadImage', 'files' => true, 'class' => 'user']) }}
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-3" id="successMessage">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('danger'))
                <div class="alert alert-danger alert-block mt-3" id="successMessage">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if(count($errors->all()))
                <div class="alert alert-danger mt-3" id="successMessage">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li style="list-style: none;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @php
                $user = auth()->user();
                @endphp
                <h4 class="mb-4">Repair Your Mobile :</h4>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('name', 'Enter name'), ['class' => 'active']}}
                            {{Form::text('record[name]', $user->fname, ['class' => 'form-control', 'placeholder'=>'Enter name','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('father', 'Enter father name'), ['class' => 'active']}}
                            {{Form::text('record[father]', '', ['class' => 'form-control', 'placeholder'=>'Enter father name','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('mobile', 'Enter mobile number'), ['class' => 'active']}}
                            {{Form::tel('record[mobile]', $user->mobile, ['class' => 'form-control', 'placeholder'=>'Enter mobile number','required'=>'required','disabled'])}}
                            <input type="hidden" name="record[mobile]" value="{{$user->mobile}}" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{Form::label('email', 'Enter email'), ['class' => 'active']}}
                            {{Form::email('record[email]', $user->email, ['class' => 'form-control', 'placeholder'=>'Enter email','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{Form::label('aadharcard', 'Enter aadhar card'), ['class' => 'active']}}
                            {{Form::number('record[aadharcard]', $user->aadharcard, ['class' => 'form-control', 'placeholder'=>'Enter aadhar card','required'=>'required'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            {{ Form::label('address', 'Enter address'), ['class' => 'active'] }}
                            {{ Form::textarea('record[address]',$user->address, ['class'=>'form-control editor', 'placeholder'=>'Enter address', 'rows' => '4', 'cols' => '10','required'=>'required']) }}
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            {{Form::label('payment', 'Enter Price (₹)'), ['class' => 'active']}}
                            {{Form::number('record[payment]', $plan->price, ['class' => 'form-control', 'placeholder'=>'Enter aadhar card','required'=>'required','disabled'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('photo', 'Choose photo'), ['class' => 'active']}}
                            {{Form::file('photo',['class'=>'form-control','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('aadharcard1', 'Choose aadhar card (front)'), ['class' => 'active']}}
                            {{Form::file('aadharcard1',['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('aadharcard2', 'Choose aadhar card (back)'), ['class' => 'active']}}
                            {{Form::file('aadharcard2',['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('mobile1', 'Choose mobile image (first)'), ['class' => 'active']}}
                            {{Form::file('mobile1',['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('mobile2', 'Choose mobile image (second)'), ['class' => 'active']}}
                            {{Form::file('mobile2',['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('invoice', 'Choose invoice image'), ['class' => 'active']}}
                            {{Form::file('invoice',['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('imei_number1', 'Enter IMEI number (first)'), ['class' => 'active']}}
                            {{Form::text('record[imei_number1]', '', ['class' => 'form-control', 'placeholder'=>'Enter IMEI number','required'=>'required'])}}
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            {{Form::label('imei_number2', 'Enter IMEI number (second)'), ['class' => 'active']}}
                            {{Form::text('record[imei_number2]', '', ['class' => 'form-control', 'placeholder'=>'Enter IMEI number'])}}
                        </div>
                    </div>
                </div>
                <input type="hidden" name="record[plan_date]" value="{{$plan_arr['date']}}" />
                <input type="hidden" name="record[plan_price]" value="{{$plan_arr['price']}}" />
                <input type="hidden" name="record[plan_brand]" value="{{$plan_arr['brand']}}" />
                <input type="hidden" name="record[plan_id]" value="{{$plan_arr['plan_id']}}" />
                <div class="text-right mt-3">
                    <input type="submit" class="btn btn-primary" name="save" value="Next" />
                </div>
                {{ Form::close() }}
            </div>
            @endif
            @if(@$record->id)
            <div>
                {{ Form::open(['id'=> 'checkoutForm', 'files' => true, 'class' => 'user']) }}
                <h5>Payment Confirmation</h5>
                <div class="mt-3">
                    Net Payment : {{ number_format($plan->price) }} ₹
                </div>
                <!-- <div class="mt-3">
                    Payment Mode :
                    {{ Form::radio('record[payment_mode]', 'cash' , false,['class' => 'paymentMethod']) }}
                    {{Form::label('cash', 'Cash'), ['class' => 'active']}}
                    {{ Form::radio('record[payment_mode]', 'online' , true,['class' => 'paymentMethod']) }}
                    {{Form::label('online', 'RazorPay'), ['class' => 'active']}}

                </div> -->
                <input type="hidden" name="record[payment_mode]" value="online">
                <div class="row mt-5">
                    <div class="col-lg-12">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                {{Form::text('record[txn_id]', '', ['id' => 'paymentID', 'hidden'])}}
                {{Form::text('record[id]', $record->id, ['id' => 'paymentID', 'hidden'])}}
                <div class="text-right mt-3">
                    <input type="submit" class="btn btn-primary" name="save" value="Processed to pay" />
                </div>
                {{ Form::close() }}

            </div>
            @endif
        </div>
</section>

@endsection
@section('footer-script')
<script>
    $('#rzp-footer-form').submit(function(e) {
        var button = $(this).find('button');
        var parent = $(this);
        button.attr('disabled', 'true').html('Please Wait...');
        $.ajax({
            method: 'get',
            url: this.action,
            data: $(this).serialize(),
            complete: function(r) {
                console.log('complete');
                //console.log(r);
            }
        })
        return false;
    })

    function padStart(str) {
        return ('0' + str).slice(-2)
    }

    function demoSuccessHandler(transaction) {
        let form = $('#checkoutForm');
        // var photo = $('.photo')[0].files[0];
        // console.log(photo);
        // var formdata = new FormData();
        // formdata.append("photo", photo);
        // console.log(formdata);
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        if (transaction) {
            $('#paymentID').val(transaction.razorpay_payment_id);
        }

        $.ajax({
            method: 'post',
            url: "{!! route('insurance.store',$plan->id) !!}",
            data: form.serialize(),
            // enctype: 'multipart/form-data',
            //contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(r) {
                window.location = "{{ url('/profile') }}";
            }
        })
    }

    var options = {
        key: "{{ env('RAZORPAY_KEY') }}",
        amount: "{{ $plan->price * 100 }}",
        name: 'Arp Service',
        description: '',
        image: '{{ url("imgs/logo.png") }}',
        prefill: {
            "email": "{{ Auth::user()->email }}",
            "contact": "{{ Auth::user()->mobile }}"
        },
        // amount: '200',
        handler: demoSuccessHandler
    }

    window.r = new Razorpay(options);
    $(document).on('submit', '#checkoutForm', function(e) {
        e.preventDefault();
        if (is_valid) {
            // let payMethod = $('.paymentMethod:checked').val();
            // alert(payMethod);
            // if (payMethod == "online") {
                r.open();
            // } else {
                // demoSuccessHandler(false);
            // }
        }
    });
</script>
@endsection