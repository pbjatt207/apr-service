<div class="inquery-form mb-5">
  <div class="firstdiv">
    <div class="form-header py-2 mb-3 text-center">
        <h3>Enquiry</h3>
    </div>
    {{ Form::open(['url' => url('inquery'), 'method'=>'POST', 'class' => 'mx-3 mt-2']) }}
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="record[name]" class="form-control" id="name" placeholder="jhon doe" required="required">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" name="record[email]" class="form-control" id="email" placeholder="name@example.com" required="required">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="text" name="record[mobile]" class="form-control" id="mobile" placeholder="9876543210">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="subject">Subject</label>
                    <input type="text" name="record[subject]" class="form-control" id="subject" placeholder="subject">
                </div> 
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" name="record[message]" id="message" rows="5" required="required"></textarea>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <button class="btn btn-primary" id="contact_form">Submit</button>
                </div>
            </div>
        </div>    
    {{ Form::close() }}
  </div>
</div>