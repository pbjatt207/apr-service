@extends('frontend.layout.master')
@section('title', $slug->name)
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="about-page">
	<div class="backcolo-about">
		<h1 style="text-tranform: capitialize">{{$slug->name}}</h1>
		<p><a href="{{ url('/')}}">Home</a>&nbsp; /&nbsp;&nbsp;<span>{{$slug->name}}</span></p>
	</div>
	<div class="container">
		<div class="about-page-con">
			<h2>{{ $slug->name }}</h2>

			<p>{!! $slug->description !!}</p>
		</div>
	</div>
</section>

@stop