@extends('frontend.layout.master')
@section('title', 'Login')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="costmer-login-page-my">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="login-costmer-content">
                    <div class="lotgin-costmer-h2">
                        <h2>Sign in to</h2>
                        <h2>Recharge Direct</h2>
                    </div>
                    <br>
                    <p>if you don't have an account</p>
                    <p>You can <span><a href="{{ url('/register') }}">REGISTER HERE</a></span></p>
                </div>
                <div class="img-banner-costmer">
                    <img src="assets/imgs/71-Su4Wr0HL._SY741_.jpg">
                </div>
            </div>
            <div class="col-lg-4">
                {{ Form::open(['url' => route('login'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                @if ($message = Session::get('success'))
						<div class="alert alert-success alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if ($message = Session::get('danger'))
						<div class="alert alert-danger alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if(count($errors->all()))
						<div class="alert alert-danger mt-3" id="successMessage">
							<ul>
								@foreach($errors->all() as $error)
								<li style="list-style: none;">{{$error}}</li>
								@endforeach
							</ul>
						</div>
						@endif
                <div class="login-costmer-right-form">
                    <input type="text" name="mobile" class="form-controllrt-m" placeholder="Enter Mobile">
                    <input type="Password" name="password" class="form-controllrt-m2" placeholder="Password">
                    <label class="lable-login-costmer"><small><a href="{{ url('/forgot_password_step-1') }}">Forgot password ?</a></small></label>
                    <button type="submit" class="btn btn-primary login-costmer-button">Log in</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>

@stop