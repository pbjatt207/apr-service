@extends('frontend.layout.master')
@section('title', 'Contact Us')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="contact">
	<div class="backcolo-about">
		<h1>Contact us</h1>
		<p><a href="{{ url('/') }}">Home</a>&nbsp; /&nbsp;&nbsp;<span>Contact us</span></p>
	</div>
	<div class="container">
		<div class="form-contact">
			<div class="row">
				<div class="col-lg-8">
					<div class="form-innar-con">
						<h3>Get in Touch</h3>
						{{ Form::open(['url' => route('inquiry'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if ($message = Session::get('danger'))
						<div class="alert alert-danger alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if(count($errors->all()))
						<div class="alert alert-danger mt-3" id="successMessage">
							<ul>
								@foreach($errors->all() as $error)
								<li style="list-style: none;">{{$error}}</li>
								@endforeach
							</ul>
						</div>
						@endif
						<input class="form-input " name="name" type="text" placeholder="Enter your name" required>
						<input class="form-input-r " name="mobile" type="number" placeholder="Mobile No" required>
						<input class="form-control-form-sub " name="email" type="text" placeholder="Email" required>
						<input class="form-control-form-sub " name="subject" placeholder="Enter Subject" required>
						<textarea class=" w-100" name="message" placeholder="Enter Message" required></textarea>
						<div>
							<button type="submit" class="button  btn btn-primary btn-lg button-contactForm boxed-btn">Send</button>
						</div>
						{{ Form::close() }}
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card-contect">
						<ul>
							<li>
								<i class='bx bx-home'></i>
								<span>
								{{ $setting->address }}
								</span>
								<p></p> 
							</li>
							<li>								
								<i class='bx bx-phone-call'></i><span><a href="tel:{{$setting->mobile}}">{{$setting->mobile}}</a></span>
								<p>
									<!-- Mon to Fri 9am to 6pm -->
								</p>
								<i class='bx bx-envelope'></i><span><a href="mailto:{{$setting->email}}">{{$setting->email}}</a></span>
								<p>Send us your query anytime!</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="map-contact-m" id="map">
		<section class="container">
			<div class="map-google">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28605.32736391467!2d73.15891070000004!3d26.337315250000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3941f26989ff7009%3A0xb9fa8a9adf6b77f0!2sBanar%2C+Rajasthan+342027!5e0!3m2!1sen!2sin!4v1440583416119" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen=""></iframe>
			</div>
		</section>
	</div>
</section>

@stop