@extends('frontend.layout.master')
@section('title', 'Faqs')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="faq-page">
	<div class="backcolo-about faq-hedding">
		<div class="container">
			<div class="slider-main-inpu-fa">
				<h3>How can we help you?</h3>
				{!! Form::open(['method' => 'GET']) !!}
					<input type="text" name="title" placeholder="type keywords to find answers">
				{!! Form::close() !!}
				<p class="color-input-ti">you can also browse the topics below to find what you are looking for</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="wrapper">
			<div class="accordion">
			@foreach($lists as $key => $item)
				<div class="accordion-box {{ $key == 0 ? 'shown' : '' }}">
					<div class="acordi-in">
						<div class="d-flex">
							<p>{{$item->title}}</p>
							<span><i class='bx bxs-chevron-right'></i></span>
						</div>
					</div>
					<div class="accordion-content " style="{{ $key == 0 ? 'display: block;' : 'display: none;' }}">
						"{!! $item->description !!}"
					</div>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</section>

@stop