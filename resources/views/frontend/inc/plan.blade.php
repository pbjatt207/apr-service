@extends('frontend.layout.master')
@section('title', 'Plan')
@section('keyword', '')
@section('description', '')
@section('contant')

<div class="backcolo-plan">
	<div class="backcolo-about">
		<h1>Plans</h1>
		<p><a href="{{url('/')}}">Home</a>&nbsp; /&nbsp;&nbsp;<span>plans</span></p>
	</div>
</div>
<section class="plan-bg-color">
	<div class="container">
		{!! Form::open(['url' => route('insurance_re'), 'method' => 'post']) !!}
		
		<input type="hidden" name="date" value="{{ $plan_filter_arr['date'] }}">
		<input type="hidden" name="price" value="{{ $plan_filter_arr['price'] }}">
		<input type="hidden" name="brand" value="{{ $plan_filter_arr['brand'] }}">

		<div class="row">
			@foreach($lists as $key => $item)
			<div class="col-lg-4 col-md-6">
				<input name="plan_id" type="radio" value="{{ $item->id }}" id="select_plan_{{ $item->id }}" @if($key === 0) checked @endif class="d-none" />
				<div class="card mycard-plan d-block">
					<div class="card-body card-my-plan">
						<h5 class="card-title">{{$item->name}}</h5>
						<p class="card-text"><span>₹ {{number_format($item->price)}}</span> only/year</p>
						<p class="card-text">inclusive of all taxes</p>
						<p class="card-text">(an affordable ₹{{ round($item->price/12) }}/month)</p>
					</div>
					<ul class="list-group list-group-flush my-be-li">
						@foreach($item->rule as $rule)
						<li>
							<span>
								<i class='bx bx-check'></i>
								<h4>{{$rule->name}}</h4>
							</span>
						</li>
						@endforeach
					</ul>

					<div class="card-body maybut-bay">
						@if(auth()->user())
							<label for="select_plan_{{ $item->id }}" class="btn btn-primary btn-block">Choose</label>
						@else
							<a href="{{ url('/login') }}" class="btn btn-primary">Choose</a>
						@endif
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="mt-5 text-right">
			<button type="submit" class="btn btn-primary">Continoue&hellip; &raquo;</button>
		</div>
		{!! Form::close() !!}
	</div>
</section>

@stop