<div class="inquery-form">
	<div class="close-div" onclick="inquery()">
		<i class="fa fa-close"></i>
	</div>
  <div class="firstdiv">
    <div class="form-header text-center py-2">
        <p class="p-0 m-0">Enquiry</p>
    </div>
    {{ Form::open(['url' => url('inquery'), 'method'=>'POST', 'class' => 'mx-3 mt-2']) }}
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">                    
                    <input type="text" name="record[name]" class="form-control" id="name" placeholder="jhon doe" required="required">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">                      
                      <input type="email" name="record[email]" class="form-control" id="email" placeholder="name@example.com" required="required">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">                
                    <input type="text" name="record[mobile]" class="form-control" id="mobile" placeholder="9876543210">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">               
                    <input type="text" name="record[subject]" class="form-control" id="subject" placeholder="subject">
                </div> 
            </div>
            <div class="col-lg-12">
                <div class="form-group">                                   
                    <textarea class="form-control" name="record[message]" placeholder="Message" id="message" rows="3" required="required"></textarea>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group text-right">
                    <button class="btn btn-warning btn-sm" id="contact_form">Submit</button>
                </div>
            </div>
        </div>    
    {{ Form::close() }}
  </div>
</div>