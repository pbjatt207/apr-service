@extends('frontend.layout.master')
@section('title', 'Forgot Password')
@section('keyword', '')
@section('description', '')
@section('contant')

<section class="contact">
	<div class="backcolo-about">
		<h1>Forgot Password</h1>
		<p><a href="{{ url('/') }}">Home</a>&nbsp; /&nbsp;&nbsp;<span>Forgot Password</span></p>
	</div>
	<div class="container">
		<div class="form-contact">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-innar-con">
						<h3>Forgot Password</h3>
						{{ Form::open(['url' => route('forgot.step-2'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if ($message = Session::get('danger'))
						<div class="alert alert-danger alert-block mt-3" id="successMessage">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
						@if(count($errors->all()))
						<div class="alert alert-danger mt-3" id="successMessage">
							<ul>
								@foreach($errors->all() as $error)
								<li style="list-style: none;">{{$error}}</li>
								@endforeach
							</ul>
						</div>
						@endif
                        <div class="row">
                            <div class="col-lg-9">
                                <input class="form-input" style="width:100%;" name="otp_code" type="number" placeholder="Enter Otp" required>
                                <input class="form-input" style="width:100%;" name="mobile" type="hidden" value="{{$request->mobile}}" placeholder="Enter Otp" required>
                                <input class="form-input" style="width:100%;" name="password" type="text" placeholder="Enter new password" required>
                            
                                <button type="submit"  class="button  btn btn-primary btn-lg button-contactForm boxed-btn">CHANGE</button>
                            </div>
                        </div>
						
						<div>
							
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop