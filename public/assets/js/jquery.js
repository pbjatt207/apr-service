

$(document).ready(function () {
  $("#successMessage").delay(5000).slideUp(300);
});

//active class menu

$(".aactivee").click(function () {
  // If this isn't already active
  if (!$(this).hasClass("active")) {
    // Remove the class from anything that is active
    $(".aactivee.active").removeClass("active");
    // And make this active
    $(this).addClass("active");
  }
});

//acordion

$(function () {
  $(document).on("click", ".accordion-box>.acordi-in", function () {
    $(this).closest(".accordion-box").children(".accordion-content").slideToggle();
    $(this).closest(".accordion-box").toggleClass('shown');

    $(this).closest(".accordion-box").siblings().children(".accordion-content").slideUp();
    $(this).closest(".accordion-box").siblings().removeClass('shown');;
  });
});


$('.more-drop').on('click', function () {
  $(this).siblings('.hidden-more').toggleClass('show-moreblo');
  $(this).closest('.arrow-dis').toggleClass('arro-active');

});

//menu button toggle

$('.overly-main').on('click', function () {
  $('.mobileview').toggleClass('mobileview-e');
  $('.overly-main').toggleClass('overly-main-d');
  $('.change').toggleClass('overly-colo-c');
});

$('#button-enlarge').on('click', function () {
  $('.mobileview').toggleClass('mobileview-e');
  $('.overly-main').toggleClass('overly-main-d');
});

$('.overly-main').on('click', function () {

  $('#button-enlarge').removeClass("show-nav");
});

$('#button-enlarge').on('click', function () {

  $(this).toggleClass("show-nav");
});

//backtop

var mybutton = document.getElementById("back-top");


window.onscroll = function () { scrollFunction() };

function scrollFunction() {
  if (document.body.scrollTop > 480 || document.documentElement.scrollTop > 480) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
//nav

$(function () {
  var shrinkHeader = 700;
  $(window).scroll(function () {
    var scroll = getCurrentScroll();
    if (scroll >= shrinkHeader) {
      $('#myHeader').addClass('sticky');
    }
    else {
      $('#myHeader').removeClass('sticky');
    }
  });
  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }
});